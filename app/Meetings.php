<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Projects;

/**
 * @property integer $id
 * @property string $meeting_note_id
 * @property integer $crm_userid
 * @property integer $delta
 * @property string $created_by
 * @property string $company_id
 * @property integer $meeting_date
 * @property integer $created_date
 * @property string $last_updated_by
 * @property integer $last_updated_date
 * @property boolean $contract_signed
 * @property boolean $signed_on_the_spot
 * @property string $priority
 * @property integer $project_year
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 * @property boolean $completed
 * @property string $status
 * @property string $reason
 * @property string $research_status
 * @property string $parent_meeting_note_id
 */
class Meetings extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'meeting_notes';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['meeting_note_id', 'crm_userid', 'delta', 'created_by', 'company_id', 'meeting_date', 'created_date', 'last_updated_by', 'last_updated_date', 'contract_signed', 'signed_on_the_spot', 'priority', 'project_year', 'removed', 'created_at', 'updated_at', 'completed', 'status', 'reason', 'research_status', 'parent_meeting_note_id', 'email_uid', 'email_sequence', 'driver_email', 'driver_invite', 'company_attendees_invite', 'company_attendees_subject_invite', 'meeting_location', 'scheduled_by', 'timezone'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";

    /**
     * Outlook files for company
     * 
     * @param  companyId
     * @param  year
     * 
     * @return [object] files
     */
    public function findOutlookFiles($companyId)
    {
        $meetings = $this
                    // ->leftJoin('tblMeetingsNotesContents', 'tblMeetingsNotesContents.tblMeetings_fldMeetingId', '=', 'tblMeetings.fldMeetingId')
                    // ->leftJoin('tblMeetingsNotesHeadings', 'tblMeetingsNotesHeadings.fldMeetingNoteHeadingId', '=', 'tblMeetingsNotesContents.tblMeetingsNotesHeadings_fldMeetingNoteHeadingId')
                    // ->leftJoin('tblMeetingsDeltas', 'tblMeetingsDeltas.fldMeetingDeltaId', '=', 'tblMeetings.tblMeetingsDeltas_fldMeetingDeltaId')
                    // ->leftJoin('tblYears', 'tblYears.fldYearId', '=', 'tblMeetings.tblYears_fldYearId')
                    ->where('meeting_notes.removed', false)
                    // ->where('tblMeetingsNotesContents.fldMeetingNoteContentDeleted', false)
                    // ->where('tblMeetingsNotesHeadings.fldMeetingNoteHeadingDeleted', false)
                    // ->where('tblMeetingsNotesHeadings.fldMeetingNoteHeadingActive', true)
                    ->where('meeting_notes.company_id', $companyId)
                    ->select('meeting_notes.id AS id', 'meeting_notes.meeting_note_id AS hash','meeting_notes.delta AS delta', 'meeting_notes.meeting_date AS date', 'meeting_notes.project_year AS year')
                    ->orderBy('meeting_notes.project_year', 'DESC')
                    ->orderBy('meeting_notes.meeting_date', 'DESC');
                    // ->select('tblMeetingsDeltas.fldMeetingDeltaTitle AS delta', 'tblMeetings.fldMeetingDate AS date', 'tblYears.fldYearText AS year', 'tblMeetingsNotesContents.fldMeetingNoteContentText AS content', 'tblMeetingsNotesHeadings.fldMeetingNoteHeadingName AS heading');

        $response = $meetings->get()->toArray();

        return $response;
    }

    /**
     * Outlook files for project
     * 
     * @param  companyId
     * @param  year
     * 
     * @return [object] files
     */
    public function findProjectOutlookFiles($companyId, $projects)
    {
        $year = Projects::whereIn('id', $projects)
                ->select('project_year AS text', 'project_year_id AS id')
                ->first();

        $meetings = $this
                    ->where('meeting_notes.removed', false)
                    ->where('meeting_notes.company_id', $companyId)
                    ->where('meeting_notes.project_year', $year->text)
                    ->select('meeting_notes.id AS id', 'meeting_notes.meeting_note_id AS hash','meeting_notes.delta AS delta', 'meeting_notes.meeting_date AS date', 'meeting_notes.project_year AS year')
                    ->whereNull('meeting_notes.parent_meeting_note_id')
                    ->orderBy('meeting_notes.meeting_date', 'DESC');

        $response = $meetings->get()->toArray();

        return $response;
    }

    /**
     * List of meetings
     * 
     * @param  projects
     * @param  start
     * @param  end
     * 
     * @return [object] response
     */
    public function findMeetings($projects, $start, $end)
    {
        $year = Projects::whereIn('id', $projects)
                ->select('project_year AS text', 'project_year_id AS id')
                ->first();

        $country = Projects::whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $unixStart = strtotime($start);
        $unixEnd = strtotime($end);
        $meetings = $this
                    // ->leftJoin('meeting_note_attendance', 'meeting_note_attendance.meeting_note_id', '=', 'meeting_notes.meeting_note_id')
                    ->leftJoin('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->leftJoin('company_advisory_potential', 'company_advisory_potential.company_id', '=', 'companies.company_id')
                    ->where('meeting_notes.removed', false)
                    // ->where('meeting_note_attendance.contact_id', $employeeId)
                    // ->where('meeting_note_attendance.is_obg_employee', true)
                    ->where('meeting_notes.meeting_date', '>=', $unixStart)
                    ->where('meeting_notes.meeting_date', '<=', $unixEnd)
                    ->where('meeting_notes.project_year', $year->text)
                    ->where('companies.country_id', $country->id)
                    ->select('meeting_notes.id AS id', 'meeting_notes.meeting_note_id AS hash','meeting_notes.delta AS delta', 'meeting_notes.meeting_date AS date', 'meeting_notes.project_year AS year', 'companies.name AS name', 'companies.company_id AS company_hash', 'companies.advisory_potential AS potential', 'company_advisory_potential.advisory_potential AS potential_reason')
                    ->whereNull('meeting_notes.parent_meeting_note_id')
                    ->orderBy('meeting_notes.meeting_date', 'DESC');
        $response = [];
        
        $agendas = $meetings->get()->toArray();
        foreach ($agendas as $agenda) {
            $push = [];
            $delta = $agenda['delta'];
            $color = $this->getColor($delta);
            $date = $agenda['date'];
            $name = $agenda['name'];
            $hash = $agenda['hash'];

            // $push['allDay'] = true;
            $push['backgroundColor'] = $color;
            $push['borderColor'] = $color;
            $push['textColor'] = '#fff';
            $push['delta'] = $delta;
            $push['start'] = date('Y-m-d H:i', $date);
            $push['title'] = $name;
            $push['hash'] = $hash;
            $push['overlap'] = true;
            array_push($response, $push);
        }

        return $response;
    }

    /**
     * Count meetings of user by status
     * 
     * @param  delta
     * @param  title
     * @param  status
     * @param  employeeId
     * @param  sessions
     * 
     * @return [object] count
     */
    public function countUserMeetingsByStatus($delta, $title, $status, $employeeId, $department, $projects)
    {
        $year = Projects::whereIn('id', $projects)
                ->select('project_year AS text', 'project_year_id AS id')
                ->first();

        $country = Projects::whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $count = 0;
        switch ($title) {
            case 'country director':
                $query = $this->leftJoin('meeting_note_attendance', 'meeting_note_attendance.meeting_note_id', '=', 'meeting_notes.meeting_note_id')
                    ->leftJoin('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('meeting_notes.status', $status)
                    ->where('meeting_note_attendance.removed', false)
                    ->where('meeting_notes.meeting_date', '<=', time())
                    ->where('companies.country_id', $country->id)
                    ->whereIn('meeting_notes.delta', $delta)
                    ->where('meeting_notes.removed', false)
                    ->where('meeting_notes.project_year', $year->text)
                    ->distinct('meeting_notes.meeting_note_id');

                if (!in_array($department, ['administrators', 'field operations', 'managements'])) {
                    $query->where('meeting_note_attendance.contact_id', $employeeId);
                }

                $count = $query->count('meeting_notes.meeting_note_id');
                break;
            case 'project manager':
            case 'editorial manager':
            case 'editorial manager trainee':
                    $query = $this->leftJoin('meeting_note_attendance', 'meeting_note_attendance.meeting_note_id', '=', 'meeting_notes.meeting_note_id')
                        ->leftJoin('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                        ->where('meeting_notes.status', $status)
                        // ->where('meeting')
                        ->where('meeting_note_attendance.removed', false)
                        ->where('meeting_notes.meeting_date', '<=', time())
                        ->where('companies.country_id', $country->id)
                        ->whereIn('meeting_notes.delta', $delta)
                        ->where('meeting_notes.removed', false)
                        ->where('meeting_notes.project_year', $year->text)
                        ->distinct('meeting_notes.meeting_note_id')
                        ->select('meeting_notes.delta AS delta', 'meeting_notes.meeting_note_id AS hash');

                    if (!in_array($department, ['administrators', 'field operations', 'managements'])) {
                        $query->where('meeting_note_attendance.contact_id', $employeeId);
                    }

                    $meetings = $query->get();

                    $parents = $this->getParents($title);

                    foreach ($meetings as $meeting) {
                        $parent = $parents;
                        $hash = $meeting->hash;
                        $delta = $meeting->delta;
                        if ($title == 'editorial manager' && $meeting->delta == 2) {
                            $count++;
                            continue;
                        }

                        if ($title == 'editorial manager trainee' && $meeting->delta == 2) {
                            $parent = ['Editorial Manager'];
                        }

                        $exist = MeetingsAttendances::leftJoin('contacts', 'contacts.contact_id', '=', 'meeting_note_attendance.contact_id')
                                ->where('meeting_note_attendance.meeting_note_id', $hash)
                                ->whereIn('contacts.job_title', $parent)
                                ->where('meeting_note_attendance.removed', false)
                                ->count();

                        if ($exist == 0) {
                            $count++;
                        }
                    }

                    // $count = $query->count('meeting_notes.meeting_note_id');
                break;
            default:
                $query = $this->leftJoin('meeting_note_attendance', 'meeting_note_attendance.meeting_note_id', '=', 'meeting_notes.meeting_note_id')
                    ->leftJoin('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('meeting_notes.status', $status)
                    ->where('meeting_note_attendance.removed', false)
                    ->where('meeting_notes.meeting_date', '<=', time())
                    ->where('companies.country_id', $country->id)
                    ->whereIn('meeting_notes.delta', $delta)
                    ->where('meeting_notes.removed', false)
                    ->where('meeting_notes.project_year', $year->text)
                    ->distinct('meeting_notes.meeting_note_id');

                /*if (!in_array($department, ['administrators', 'field operations', 'managements'])) {
                    $query->where('meeting_note_attendance.contact_id', $employeeId);
                }*/

                $count = $query->count('meeting_notes.meeting_note_id');
                break;
        }
     

        return $count;
    }

    /**
     * Find all meetings for user
     * 
     * @param  department
     * @param  parameters
     * @param  projects
     * @param  employeeId
     * 
     * @return [object] $meetings
     */
    public function findUserMeetings($department, $delta, $title, $parameters, $projects, $employeeId)
    {
        $year = Projects::whereIn('id', $projects)
                ->select('project_year AS text', 'project_year_id AS id')
                ->first();

        $country = Projects::whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $query = $this->leftJoin('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                ->where('meeting_notes.removed', false)
                ->where('meeting_notes.meeting_date', '<=', time())
                ->where('meeting_notes.delta', '!=', 6)
                ->where('companies.country_id', $country->id)
                ->where('meeting_notes.project_year', $year->text)
                ->orderBy('meeting_notes.meeting_date', 'DESC')
                ->distinct('meeting_notes.meeting_note_id')
                ->select('meeting_notes.meeting_note_id AS hash', 'companies.company_id AS company_hash', 'companies.name AS company_name', 'companies.sector AS sector', 'meeting_notes.status AS status', 'meeting_notes.delta AS delta');

        if (!in_array($department, ['administrators', 'field operations', 'managements'])) {
            $query->leftJoin('meeting_note_attendance', 'meeting_note_attendance.meeting_note_id', '=', 'meeting_notes.meeting_note_id')
                ->where('meeting_note_attendance.contact_id', $employeeId)
                ->where('meeting_note_attendance.removed', false);
        }

        $table = $this->buildQuery($parameters, $query, $department, $country->id, $year->text);

        $meetings = [];
        if ($this->validate($parameters, 'owner')) {
            switch ($title) {
                case 'country director':
                    $table->where('meeting_notes.meeting_date', '<=', time())
                        ->whereIn('meeting_notes.delta', $delta);
                    break;
                case 'project manager':
                case 'editorial manager':
                case 'editorial manager trainee':
                        $table->where('meeting_notes.meeting_date', '<=', time())
                            ->whereIn('meeting_notes.delta', $delta);
    
                        $items = $table->get()->toArray();
    
                        $parents = $this->getParents($title);
    
                        foreach ($items as $item) {
                            $parent = $parents;
                            $hash = $item['hash'];
                            $delta = $item['delta'];
                            if ($title == 'editorial manager' && $item['delta'] == 2) {
                                array_push($meetings, $item);
                                continue;
                            }
    
                            if ($title == 'editorial manager trainee' && $item['delta'] == 2) {
                                $parent = ['Editorial Manager'];
                            }
    
                            $exist = MeetingsAttendances::leftJoin('contacts', 'contacts.contact_id', '=', 'meeting_note_attendance.contact_id')
                                    ->where('meeting_note_attendance.meeting_note_id', $hash)
                                    ->whereIn('contacts.job_title', $parent)
                                    ->where('meeting_note_attendance.removed', false)
                                    ->count();
    
                            if ($exist == 0) {
                                array_push($meetings, $item);
                            }
                        }
    
                        // $count = $query->count('meeting_notes.meeting_note_id');
                    break;
            }
        } else {
            $meetings = $table->get()->toArray();
        }

        // $meetings = $table->get()->toArray();

        if ($this->validate($parameters, 'contacts')) {
            $response = [];
            foreach ($meetings as $meeting) {
                $hash = $meeting['hash'];
                $exist = MeetingsAttendances::where('meeting_note_id', $hash)->whereIn('contact_id', $parameters['contacts'])->where('is_obg_employee', false)->first();
                if ($exist) {
                    array_push($response, $meeting);
                }
            }
            return $response;
        }

        return $meetings;
    }

    /**
     * Get color of delta
     * 
     * @param delta
     * 
     * @return $color
     */
    public function getColor($delta) {
        $color = '';
        $colors = [
            0 => '#FE8383',
            1 => '#FF9DD8',
            2 => '#FFBB79',
            3 => '#83C3FE',
            4 => '#6FCF97',
            5 => '#b466d6',
            6 => '#c6b858',
            7 => '#FED100',
            8 => '#FF5733'
        ];

        $color = $colors[$delta];
        return $color;
    }

    /**
     * Get parents of role
     * 
     * @param title
     * 
     * @return $parents
     */
    public function getParents($title) {
        $parents = [];

        if (strtolower($title) == "project manager") {
            $parents = ["Country Director"];
        }  else if (strtolower($title) == "editorial manager") {
            $parents = ["Country Director", "Project Manager"];
        } else if (strtolower($title) == "editorial manager trainee") {
            $parents = ["Country Director", "Project Manager", "Editorial Manager"];
        }
        return $parents;
    }

    /**
     * Build a query with parameters/filters
     * 
     * @param [array] parameters
     * @param [object] query
     * 
     * @return [object] query
     */
    private function buildQuery($parameters, $query, $department, $country, $year) 
    {
        if($this->validate($parameters, 'name')) {
            $query->where('companies.name', 'LIKE', '%'.$parameters['name'].'%');
        }

        if($this->validate($parameters, 'status')) {
            $query->whereIn('meeting_notes.status', $parameters['status']);
        }

        // if($this->validate($parameters, 'employees')) {
        //     if (!in_array($department, ['administrators', 'field operations', 'managements'])) {
        //         $query->where(function($subquery) use ($parameters){
        //             $subquery->whereIn('meeting_note_attendance.contact_id', $parameters['employees'])
        //             ->where('meeting_note_attendance.is_obg_employee', true);
        //         });
        //     } else {
        //         $query->join('meeting_note_attendance', 'meeting_note_attendance.meeting_note_id', '='. 'meeting_notes.meeting_note_id')
        //             ->where(function($subquery) use ($parameters){
        //                 $subquery->whereIn('meeting_note_attendance.contact_id', $parameters['employees'])
        //                 ->where('meeting_note_attendance.is_obg_employee', true);
        //             });
        //     }
            
        // }

        // if($this->validate($parameters, 'contacts')) {
        //     if ($this->validate($parameters, 'employees')) {
        //         $query->where(function($subquery) use ($parameters){
        //             $subquery->whereIn('meeting_note_attendance.contact_id', $parameters['contacts'])
        //             ->where('meeting_note_attendance.is_obg_employee', false);
        //         });
        //     } else {
        //         if (!in_array($department, ['administrators', 'field operations', 'managements'])) {
        //             $query->where(function($subquery) use ($parameters){
        //                 $subquery->whereIn('meeting_note_attendance.contact_id', $parameters['contacts'])
        //                 ->where('meeting_note_attendance.is_obg_employee', false);
        //             });
        //         } else {
        //             $query->join('meeting_note_attendance', 'meeting_note_attendance.meeting_note_id', '='. 'meeting_notes.meeting_note_id')
        //             ->where(function($subquery) use ($parameters){
        //                 $subquery->whereIn('meeting_note_attendance.contact_id', $parameters['contacts'])
        //                 ->where('meeting_note_attendance.is_obg_employee', false);
        //             });
        //         }
        //     }
        // }

        return $query;
    }

    /**
     * Check if parameter exists and variable is not empty
     * 
     * @param [array] parameters
     * @param [string] index
     * 
     * @return [bool] exist
     */
    function validate($parameters, $index) {
        if(isset($parameters[$index])) {
            return true;
        }

        return false;
    }

    /**
     * Generate array of delta for role
     * @param [string] title
     * 
     * @return delta
     */
    function generateDeltas($title) {
        $delta = [];
        switch(strtolower($title)) {
            case 'country director':
                $delta = [0,1,3,4,5];
                break;
            case 'project manager':
                $delta = [0,1,3,4,5];
                break;
            case 'editorial manager':
                $delta = [0,2,5];
                break;
            case 'editorial manager trainee':
                $delta = [0,2,5];
                break;
            default:
                $delta = [0,1,2,3,4,5];
                break;
        }

        return $delta;
    }
}
