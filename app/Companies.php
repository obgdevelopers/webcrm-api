<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Projects;
use App\Reminders;
use App\CompaniesNotes;
use App\Histories;

/**
 * @property integer $id
 * @property string $company_id
 * @property integer $crm_userid
 * @property string $name
 * @property string $parent_company_type
 * @property string $sector
 * @property string $office_phone
 * @property string $office_fax
 * @property string $office_address_line_1
 * @property string $office_address_line_2
 * @property string $office_address_town_city
 * @property string $office_address_region
 * @property string $office_postal_code
 * @property string $office_directions
 * @property string $billing_address_line_1
 * @property string $billing_address_line_2
 * @property string $billing_address_town_city
 * @property string $billing_address_region
 * @property string $billing_postal_code
 * @property integer $country_id
 * @property string $key_contact
 * @property string $financial_information
 * @property string $rating
 * @property string $company_notes
 * @property string $previous_client
 * @property string $web_address
 * @property boolean $multiregion
 * @property integer $advertised_in_competitor_report
 * @property string $country_custom
 * @property string $assigned_user
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 * @property string $main_contact
 * @property string $last_contacted
 * @property string $last_met
 * @property string $turnover
 * @property string $company_size
 * @property string $source
 * @property int $status
 */
class Companies extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['company_id', 'crm_userid', 'name', 'parent_company_type', 'sector', 'office_phone', 'office_fax', 'office_address_line_1', 'office_address_line_2', 'office_address_town_city', 'office_address_region', 'office_postal_code', 'office_directions', 'billing_address_line_1', 'billing_address_line_2', 'billing_address_town_city', 'billing_address_region', 'billing_postal_code', 'country_id', 'key_contact', 'financial_information', 'rating', 'company_notes', 'previous_client', 'web_address', 'multiregion', 'advertised_in_competitor_report', 'country_custom', 'assigned_user', 'removed', 'created_at', 'updated_at', 'main_contact', 'last_contacted', 'last_met', 'turnover', 'company_size', 'source', 'status', 'advisory_potential', 'flagged', 'color_flag', 'company_email', 'advisory_assignee', 'advisory_assignee_new'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";

    /**
     * Index page table data
     * 
     * @param  [string] where
     * @param  [string] bind
     * @param  [int] limit
     * @param  [int] offset
     * 
     * @return [object] table
     */
    public function buildTable($parameters, $limit = 50, $offset, $projects)
    {
        // $projects = $parameters['projects'];
        // $projects = explode(',', $projects);

        $year = Projects::whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = Projects::whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $query = $this->leftJoin('contacts', 'contacts.contact_id', '=', 'companies.assigned_user')
                    ->leftJoin('companies_project_status', function($join) use ($projects) {
                        $join->on('companies_project_status.company_hash_id', '=', 'companies.company_id')
                            ->whereIn('companies_project_status.project_id', $projects);
                    })
                    ->leftJoin('company_status', 'company_status.id', '=', 'companies_project_status.company_status_id')
                    ->where('companies.removed', false)
                    ->select('companies.id AS id', 'companies.company_id AS hash', 'companies.name AS name', 'contacts.id AS assignee_id', 'contacts.first_name AS assignee_firstname', 'contacts.last_name AS assignee_lastname', 'companies.sector as sector', 'company_status.name AS status', 'companies.office_phone AS landline', 'companies.office_address_line_1 AS address_line1', 'companies.office_address_line_2 AS address_line2', 'companies.office_address_town_city AS town')
                    // ->selectRaw('(SELECT COUNT(company_notes_tbl.company_notes_id) FROM company_notes_tbl WHERE company_notes_tbl.company_notes_companyid = companies.company_id) AS notes_count')
                    // ->selectRaw('(SELECT COUNT(reminder_tbl.reminder_id) FROM reminder_tbl WHERE reminder_tbl.reminder_companyid = companies.company_id AND reminder_tbl.reminder_active = 1) AS reminders_count')
                    // ->selectRaw('(SELECT COUNT(histories.id) FROM histories WHERE histories.company_id = companies.company_id AND histories.project_year = \''.$year.'\' AND removed = 0 LIMIT 1) AS logs_count')
                    ->distinct('id');
        
        
                    
        $query->where('companies.country_id', $country->id);
        $table = $this->buildQuery($parameters, $query, $country->id, $year->text);
        $count = $table->count();

        $companies = $table->get();

        $results = [];
        foreach($companies as $company) {
            $id = $company['hash'];
            $reminder = Reminders::where('reminder_companyid', $id)->where('reminder_active', true)->select('reminder_date as date')->first();
            $notes = CompaniesNotes::where('company_notes_companyid', $id)->exists();
            $histories = Histories::where('company_id', $id)->where('project_year', $year->text)->where('removed', false)->exists();

            $company['reminder'] = $reminder;
            $company['notes'] = $notes;
            $company['logs'] = $histories;
            
            array_push($results, $company);
        }
        // $table
        // ->skip($offset);
        // ->take($limit);
                    
        $response = [
            'table' => $results,
            'count' => $count
        ];

        return $response;
    }

    /**
     * All companies for the project
     * 
     * @param  [string] where
     * @param  [string] bind
     * @param  [int] limit
     * @param  [int] offset
     * 
     * @return [object] table
     */
    public function buildRaw($projects)
    {
        // $projects = $parameters['projects'];
        // $projects = explode(',', $projects);

        $year = Projects::whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = Projects::whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $query = $this->where('companies.removed', false)
                    ->distinct('id');
                    
        $query->where('companies.country_id', $country->id);
        $count = $query->count();

        $companies = $query->get();

        $response = [
            'table' => $companies,
            'count' => $count
        ];

        return $response;
    }

    /**
     * Build a query with parameters/filters
     * 
     * @param [array] parameters
     * @param [object] query
     * 
     * @return [object] query
     */
    private function buildQuery($parameters, $query, $country, $year) 
    {
        if($this->validate($parameters, 'name')) {
            $query->where('companies.name', 'LIKE', '%'.$parameters['name'].'%');
        }

        if($this->validate($parameters, 'sector')) {
            $query->whereIn('companies.sector', $parameters['sector']);
        }

        if($this->validate($parameters, 'address')) {
            $query->where(function($subquery) use ($parameters){
                $subquery->where('companies.office_address_line_1', 'LIKE', '%'.$parameters['address'].'%')
                  ->orWhere('companies.office_address_line_2', 'LIKE', '%'.$parameters['address'].'%');
            });
        }

        if($this->validate($parameters, 'status')) {
            $query->where('company_status.id', $parameters['status']);
        }

        if($this->validate($parameters, 'assignee')) {
            $query->whereIn('contacts.contact_id', $parameters['assignee']);
        }

        if($this->validate($parameters, 'rating')) {
            $query->whereIn('companies.rating', $parameters['rating']);
        }

        if($this->validate($parameters, 'previous_status')) {
            $query->whereRaw('companies.company_id IN 
                (SELECT companies_project_status.company_hash_id FROM companies_project_status WHERE companies_project_status.project_id = 
                    (SELECT id FROM project WHERE project_year < '.$year.' AND country_id = '.$country.' ORDER BY project_year DESC LIMIT 1) 
                AND companies_project_status.company_status_id IN ('.join(',', $parameters['previous_status']).'))');
        }

        if($this->validate($parameters, 'turnover')) {
            $query->where('companies.turnover', 'LIKE', '%'.$parameters['turnover'].'%');
        }

        if($this->validate($parameters, 'size')) {
            $query->where('companies.company_size', 'LIKE', '%'.$parameters['size'].'%');
        }

        if($this->validate($parameters, 'source')) {
            $query->where('companies.source', 'LIKE', '%'.$parameters['source'].'%');
        }

        if($this->validate($parameters, 'history')) {
            $history = $parameters['history'];
            $query->leftJoin('adverts', 'adverts.company_id', '=', 'companies.company_id');
            switch($history) {
                case 1: // 1 => 'Never seen'
                    $query->leftJoin('meeting_notes', 'meeting_notes.company_id', '=', 'companies.company_id')
                        ->whereNull('adverts.company_id')
                        ->whereNull('meeting_notes.company_id');
                    break;
                case 2: // 2 => 'Previously seen'
                    $query->join('meeting_notes', 'meeting_notes.company_id', '=', 'companies.company_id')
                        ->whereNull('adverts.company_id')
                        ->where('meeting_notes.project_year', '<', $year)
                        ->where('meeting_notes.meeting_date', '<', time())
                        ->where('meeting_notes.removed', false);
                    break;
                case 3: // 3 => 'Previous advertiser'
                    $query->join('project', 'project.project_year', '=', 'adverts.project_year')
                        ->where('adverts.project_year', '<', $year)
                        ->where('project.closed', true);
                    break;
            }
        }

        if($this->validate($parameters, 'tags')) {
            $query->join('entity_project_tag', 'entity_project_tag.entity_id', '=', 'companies.company_id')
                    ->where('entity_project_tag.entity_type', 'company')
                    ->where('entity_project_tag.term_id', $parameters['tags']);
        }

        if($this->validate($parameters, 'met_date')) {
            $metDate = $parameters['met_date'];
            $start = strtotime($metDate['start']);
            $end = strtotime($metDate['end']);

            if($start != null) {
                $query->where('companies.last_met', '>=', $start);
            }

            if($end != null) {
                $query->where('companies.last_met', '<=', $end);
            }
        }

        if($this->validate($parameters, 'contacted_date')) {
            $contactedDate = $parameters['contacted_date'];
            $start = strtotime($contactedDate['start']);
            $end = strtotime($contactedDate['end']);

            if($start != null) {
                $query->where('companies.last_contacted', '>=', $start);
            }

            if($end != null) {
                $query->where('companies.last_contacted', '<=', $end);
            }
        }

        if($this->validate($parameters, 'complete')) {
            $complete = $parameters['complete'];
            switch ($complete){
                case 1:  //filter = "Yes"
                    $query->where(function($subquery){
                        $subquery->whereNotNull('companies.office_phone')
                        ->whereNotNull('companies.sector')
                        ->whereNotNull('companies.office_address_line_1')
                        ->whereNotNull('companies.rating');
                    });
                    $query->where(function($subquery){
                        $subquery->where('companies.office_phone', '!=', '""')
                        ->where('companies.sector', '!=', '""')
                        ->where('companies.office_address_line_1', '!=', '""')
                        ->where('companies.rating', '!=', '""');
                    });
                    break;
                case 0: //filter = "No"
                    $query->where(function($subquery){
                        $subquery->whereNull('companies.office_phone')
                        ->orWhere('companies.office_phone', '=', '""')
                        ->orWhereNull('companies.sector')
                        ->orWhere('companies.sector', '=', '""')
                        ->orWhereNull('companies.office_address_line_1')
                        ->orWhere('companies.office_address_line_1', '=', '""')
                        ->orWhereNull('companies.rating')
                        ->orWhere('companies.rating', '=', '""');
                    });
                    break;
            }
        }

        if($this->validate($parameters, 'advertised')) {
            $query->where('companies.advertised_in_competitor_report', $parameters['advertised']);
        }

        if($this->validate($parameters, 'seen')) {
            $seen = $parameters['seen'];
            $query->leftJoin('meeting_notes', 'meeting_notes.company_id', '=', 'companies.company_id');
            switch ($seen){
                case 1:  //filter = "Yes"
                    $query->where('meeting_notes.removed', false)
                    ->where('meeting_notes.meeting_date', '<', time())
                    ->where('meeting_notes.project_year', $year);
                    break;
                case 0: //filter = "No"
                    $query->whereNull('meeting_notes.company_id');
                    break;
            }
        }

        if($this->validate($parameters, 'previous')) {
            $previous = $parameters['previous'];
            $query->leftJoin('adverts', 'adverts.company_id', '=', 'companies.company_id');
            switch ($previous){
                case 1:  //filter = "Yes"
                    $query->whereNotNull('adverts.company_id');
                    break;
                case 0: //filter = "No"
                    $query->whereNull('adverts.company_id');
                    break;
            }
        }

        if($this->validate($parameters, 'reminder')) {
            $reminder = $parameters['reminder'];
            
            switch ($reminder){
                case 1:  //filter = "Yes"
                    $query->leftJoin('reminder_tbl', 'reminder_tbl.reminder_companyid', '=', 'companies.company_id');
                    $query->whereNotNull('reminder_tbl.reminder_companyid')
                        ->where('reminder_tbl.reminder_active', true);
                    break;
                case 0: //filter = "No"
                    /* Needs improvement */
                    $query->whereRaw('(SELECT COUNT(reminder_tbl.reminder_id) FROM reminder_tbl WHERE reminder_tbl.reminder_companyid = companies.company_id AND reminder_tbl.reminder_active = 1) = 0');
                    break;
            }
        }

        if($this->validate($parameters, 'action')) {
            $action = $parameters['action'];
            $previousYear = (int)$year - 1;
            switch ($action){
                case 1:  //filter = "See" 
                    $query->whereRaw('(SELECT mnany.action FROM meeting_notes_action_next_year mnany WHERE mnany.company_id = companies.company_id AND mnany.project_year = ' . $previousYear . ') = 1');
                    break;
                case 2: //filter = "Dont see"
                    $query->whereRaw('(SELECT mnany.action FROM meeting_notes_action_next_year mnany WHERE mnany.company_id = companies.company_id AND mnany.project_year = ' . $previousYear . ') = 2');
                    break;
            }
        }

        return $query;
    }

    /**
     * Check if parameter exists and variable is not empty
     * 
     * @param [array] parameters
     * @param [string] index
     * 
     * @return [bool] exist
     */
    function validate($parameters, $index) {
        if(isset($parameters[$index])) {
            return true;
        }

        return false;
    }
}
