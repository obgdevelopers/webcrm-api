<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property string $name
 * @property string $options_group
 */
class Options extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'options';

    /**
     * @var array
     */
    protected $fillable = ['name', 'options_group'];

     /**
     * Get Online DB Ver
     *
     * @return [object] OnlineVersion
     */
    public function getOnlineDBVer()
    {
        $query = $this->whereRaw('option_name LIKE "database_version" OR option_name LIKE "system_version"');

        $results = $query->get();

        return $results;
    }

    /**
     * Get Online tables
     *
     * @return [object] tables
     */
    public function getTables()
    {
        $tables = [];
        $results = DB::select(DB::raw("SHOW TABLES"));
        foreach ($results as $t) {
            $tables[] = $t->Tables_in_obg_crm;
        }
        return $tables;
    }

    /**
     * Get Online table schema
     *
     * @param  [string] tableName
     * @return [object] tableSchema
     */
    public function getTableSchema($tableName)
    {
        $results = DB::select( DB::raw("SHOW CREATE TABLE $tableName"));
        return $results;
    }
}
