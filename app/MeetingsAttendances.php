<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $attendance_id
 * @property string $contact_id
 * @property string $meeting_note_id
 * @property boolean $is_obg_employee
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 * @property boolean $send_invite
 */
class MeetingsAttendances extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'meeting_note_attendance';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['attendance_id', 'contact_id', 'meeting_note_id', 'is_obg_employee', 'removed', 'created_at', 'updated_at', 'send_invite'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";

    /**
     * Find attendees in meetings
     * 
     * @param  companyId
     * @param  year
     * 
     * @return [object] files
     */
    public function findAttendees($meetings)
    {
        $pushEmployees = [];
        foreach($meetings as $meeting) {
            $hash = $meeting['hash'];
            $employees = $this->join('contacts', 'contacts.contact_id', '=', 'meeting_note_attendance.contact_id')
                    ->where('meeting_note_attendance.meeting_note_id', $hash)
                    ->where('meeting_note_attendance.is_obg_employee', true)
                    ->where('meeting_note_attendance.removed', false)
                    ->select('contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'contacts.job_title AS job_title', 'contacts.office_phone AS landline', 'contacts.mobile_phone AS mobile', 'contacts.email AS email', 'contacts.contact_id AS hash', 'contacts.office_fax AS fax', 'meeting_note_attendance.send_invite AS contact_invite');

            $meeting['employees'] = $employees->get()->toArray();

            $pushEmployees[] = $meeting;
        }
        $push = [];
        foreach($pushEmployees as $meeting) {
            $hash = $meeting['hash'];
            $contacts = $this->join('contacts', 'contacts.contact_id', '=', 'meeting_note_attendance.contact_id')
                    ->where('meeting_note_attendance.meeting_note_id', $hash)
                    ->where('meeting_note_attendance.is_obg_employee', false)
                    ->where('meeting_note_attendance.removed', false)
                    ->select('contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'contacts.job_title AS job_title', 'contacts.office_phone AS landline', 'contacts.mobile_phone AS mobile', 'contacts.email AS email', 'contacts.contact_id AS hash', 'meeting_note_attendance.send_invite AS contact_invite');

            $meeting['contacts'] = $contacts->get()->toArray();

            $push[] = $meeting;
        }
        

        $response = $push;

        return $response;
    }

}
