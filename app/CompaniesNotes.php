<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $company_notes_id
 * @property string $company_notes_content
 * @property string $company_notes_companyid
 * @property string $company_notes_contactid
 * @property string $company_notes_created
 * @property string $company_notes_updated
 * @property boolean $company_notes_private
 */
class CompaniesNotes extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'company_notes_tbl';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'company_notes_id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['company_notes_content', 'company_notes_companyid', 'company_notes_contactid', 'company_notes_created', 'company_notes_updated', 'company_notes_private'];

    /**
     * No timestamps
     */
    public $timestamps = false;
}
