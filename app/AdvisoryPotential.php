<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $company_id
 * @property string $advisory_potential
 * @property string $created_at
 * @property string $updated_at
 * @property string $last_updated_by
 */
class AdvisoryPotential extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'company_advisory_potential';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['company_id', 'advisory_potential', 'created_at', 'updated_at', 'last_updated_by'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";
}
