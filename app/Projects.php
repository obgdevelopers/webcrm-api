<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $project_year_id
 * @property integer $country_id
 * @property integer $project_year
 * @property boolean $closed
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 * @property string $updated_by
 * @property int $target_sales
 * @property ProjectYear $projectYear
 * @property Country $country
 */
class Projects extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'project';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['project_year_id', 'country_id', 'project_year', 'closed', 'created_at', 'updated_at', 'status', 'updated_by', 'target_sales'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function projectYear()
    {
        return $this->belongsTo('App\ProjectYear');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";
}
