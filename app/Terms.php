<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property int $tags_vocabulary_id
 */
class Terms extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tags_terms';

    /**
     * @var array
     */
    protected $fillable = ['name', 'tags_vocabulary_id'];

    /**
     * Index page term data
     * 
     * @param  [string] type

     * @return [object] terms
     */
    public function findByType($type, $projects)
    {
        $terms = $this->leftJoin('tags_vocabulary', 'tags_vocabulary.id', '=', 'tags_terms.tags_vocabulary_id')
                    ->leftJoin('entity_types', 'entity_types.id', '=', 'tags_vocabulary.entity_type_id')
                    ->where('entity_types.entity_name', $type)
                    ->select('tags_terms.id AS id', 'tags_terms.name AS text');

        $countries = [];
        $years = [];           
        foreach($projects as $project) {
            array_push($countries, $project['country_id']);
            array_push($years, $project['year']);
        }
        $terms->whereIn('tags_vocabulary.country', $countries);
                    
        return $terms->get();
    }
}
