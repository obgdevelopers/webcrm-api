<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $history_id
 * @property integer $crm_userid
 * @property int $project_year
 * @property string $contact_id
 * @property string $company_id
 * @property string $notes
 * @property string $history_type
 * @property string $history_stored_type
 * @property integer $date_of_history
 * @property string $created_by
 * @property integer $created_date
 * @property integer $last_updated_date
 * @property string $last_updated_by
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 * @property integer $last_contacted
 */
class Histories extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['history_id', 'crm_userid', 'project_year', 'contact_id', 'company_id', 'notes', 'history_type', 'history_stored_type', 'date_of_history', 'created_by', 'created_date', 'last_updated_date', 'last_updated_by', 'removed', 'created_at', 'updated_at', 'last_contacted'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";

    /**
     * All histories for the project
     * 
     * @param  [string] where
     * @param  [string] bind
     * @param  [int] limit
     * @param  [int] offset
     * 
     * @return [object] table
     */
    public function buildRaw($projects)
    {
        // $projects = $parameters['projects'];
        // $projects = explode(',', $projects);
        $year = Projects::whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = Projects::whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $query = $this->join('companies', 'companies.company_id', '=', 'histories.company_id')
                    ->where('companies.removed', false)
                    ->where('histories.removed', false)
                    ->where('companies.country_id', $country->id)
                    ->select('histories.*')
                    ->distinct('histories.id');
        // $query = $this->where('companies.removed', false)
        //             ->select('companies.company_id AS id')
        //             ->distinct('id');
                    
        // $query->where('companies.country_id', $country->id);
        $count = $query->count();

        $histories = $query->get();

        $response = [
            'table' => $histories,
            'count' => $count
        ];

        return $response;
    }
}
