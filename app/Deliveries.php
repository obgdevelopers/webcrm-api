<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $delivery_id
 * @property integer $crm_userid
 * @property string $contact_id
 * @property string $delivery_type
 * @property integer $quantity
 * @property string $special_print_conditions
 * @property string $other_copies
 * @property int $project_year
 * @property integer $last_updated
 * @property string $last_updated_by
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 */
class Deliveries extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['delivery_id', 'crm_userid', 'contact_id', 'delivery_type', 'quantity', 'special_print_conditions', 'other_copies', 'project_year', 'last_updated', 'last_updated_by', 'removed', 'created_at', 'updated_at'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";
}
