<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $mn_content_id
 * @property string $meeting_note_id
 * @property integer $heading_id
 * @property string $heading_text
 * @property string $content
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 */
class MeetingsNotesContents extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'meeting_note_content';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['mn_content_id', 'meeting_note_id', 'heading_id', 'heading_text', 'content', 'removed', 'created_at', 'updated_at'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";

    /**
     * Find contents for outlook files
     * 
     * @param  companyId
     * @param  year
     * 
     * @return [object] files
     */
    public function findContents($meetings)
    {
        $push = [];
        foreach($meetings as $meeting) {
            $hash = $meeting['hash'];
            $contents = $this->leftJoin('meeting_notes_headings', 'meeting_notes_headings.id', '=', 'meeting_note_content.heading_id')
                    ->leftJoin('meeting_notes', 'meeting_notes.meeting_note_id', '=', 'meeting_note_content.meeting_note_id')
                    ->where('meeting_note_content.removed', false)
                    ->where('meeting_notes_headings.removed', false)
                    ->where('meeting_notes_headings.active', true)
                    ->where('meeting_note_content.meeting_note_id', $hash)
                    ->select('meeting_notes_headings.heading_name AS heading', 'meeting_note_content.content AS content', 'meeting_notes.company_id', 'meeting_notes.project_year');

            $meeting['files'] = $contents->get()->toArray();

            $push[] = $meeting;
        }
        

        $response = $push;

        return $response;
    }
}
