<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $fldContactTagId
 * @property string $fldContactTagCreated
 * @property string $fldContactTagModified
 * @property boolean $fldContactTagDeleted
 * @property int $tblContacts_fldContactId
 * @property string $tblContacts_fldContactHashId
 * @property int $tblProjects_fldProjectId
 * @property int $tblTerms_fldTermId
 */
class ContactsTags extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tblContactsTags';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'fldContactTagId';

    /**
     * @var array
     */
    protected $fillable = ['fldContactTagCreated', 'fldContactTagModified', 'fldContactTagDeleted', 'tblContacts_fldContactId', 'tblContacts_fldContactHashId', 'tblProjects_fldProjectId', 'tblTerms_fldTermId'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "fldContactTagCreated";
    const UPDATED_AT = "fldContactTagModified";
}
