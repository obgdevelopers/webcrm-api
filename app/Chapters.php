<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $chapter_id
 * @property string $chapter_name
 * @property integer $weight
 * @property string $country_id
 * @property int $project_year
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 */
class Chapters extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'advert_chapter';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['chapter_id', 'chapter_name', 'weight', 'country_id', 'project_year', 'removed', 'created_at', 'updated_at'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";
}
