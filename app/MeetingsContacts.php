<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $fldMeetingContactId
 * @property string $fldMeetingContactHashId
 * @property string $fldMeetingContactCreated
 * @property string $fldMeetingContactModified
 * @property boolean $fldMeetingContactDeleted
 * @property int $tblMeetings_fldMeetingId
 * @property string $tblMeetings_fldMeetingHashId
 * @property int $tblContacts_fldContactId
 * @property string $tblContacts_fldContactHashId
 */
class MeetingsContacts extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tblMeetingsContacts';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'fldMeetingContactId';

    /**
     * @var array
     */
    protected $fillable = ['fldMeetingContactHashId', 'fldMeetingContactCreated', 'fldMeetingContactModified', 'fldMeetingContactDeleted', 'tblMeetings_fldMeetingId', 'tblMeetings_fldMeetingHashId', 'tblContacts_fldContactId', 'tblContacts_fldContactHashId'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "fldMeetingContactCreated";
    const UPDATED_AT = "fldMeetingContactModified";
}
