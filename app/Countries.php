<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $region_id
 * @property string $name
 * @property string $code
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 * @property boolean $dormant
 * @property boolean $timezone
 * @property Project[] $projects
 */
class Countries extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['region_id', 'name', 'code', 'removed', 'created_at', 'updated_at', 'dormant', 'timezone'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";
}
