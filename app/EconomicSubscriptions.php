<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $contacts_countries_id
 * @property string $contact_id
 * @property string $country_id
 * @property integer $date_subscribed
 * @property string $created_at
 * @property string $updated_at
 * @property string $removed
 */
class EconomicSubscriptions extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'contacts_economic_updates_countries';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['contacts_countries_id', 'contact_id', 'country_id', 'date_subscribed', 'created_at', 'updated_at', 'removed'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";
}
