<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $fldCommunicationLogId
 * @property string $fldCommunicationLogHashId
 * @property string $fldCommunicationLogNotes
 * @property string $fldCommunicationLogDate
 * @property string $fldCommunicationLogCreated
 * @property string $fldCommunicationLogModified
 * @property boolean $fldCommunicationLogDeleted
 * @property int $tblYears_fldYearId
 * @property int $tblCompanies_fldCompanyId
 * @property string $tblCompanies_fldCompanyHashId
 * @property int $tblContacts_fldContactId
 * @property string $tblContacts_fldContactHashId
 * @property int $tblEmployees_fldEmployeeId
 * @property string $tblEmployees_fldEmployeeHashId
 * @property int $tblCommunicationLogsType_fldCommunicationLogTypeId
 */
class CommunicationLogs extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tblCommunicationLogs';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'fldCommunicationLogId';

    /**
     * @var array
     */
    protected $fillable = ['fldCommunicationLogHashId', 'fldCommunicationLogNotes', 'fldCommunicationLogDate', 'fldCommunicationLogCreated', 'fldCommunicationLogModified', 'fldCommunicationLogDeleted', 'tblYears_fldYearId', 'tblCompanies_fldCompanyId', 'tblCompanies_fldCompanyHashId', 'tblContacts_fldContactId', 'tblContacts_fldContactHashId', 'tblEmployees_fldEmployeeId', 'tblEmployees_fldEmployeeHashId', 'tblCommunicationLogsType_fldCommunicationLogTypeId'];

     /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "fldCommunicationLogCreated";
    const UPDATED_AT = "fldCommunicationLogModified";
}
