<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

/**
 * @property int $id
 * @property string $username
 * @property string $username_canonical
 * @property string $email
 * @property string $email_canonical
 * @property boolean $enabled
 * @property string $salt
 * @property string $password
 * @property string $last_login
 * @property string $confirmation_token
 * @property string $password_requested_at
 * @property array $roles
 * @property string $contact_id
 * @property string $first_name
 * @property string $last_name
 * @property int $login_count
 * @property int $sf_user_departmentid
 * @property int $send_meeting_invite
 */
class Users extends Authenticatable
{
    use Notifiable, HasApiTokens;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'sf_user';

    /**
     * @var array
     */
    protected $fillable = ['username', 'username_canonical', 'email', 'email_canonical', 'enabled', 'salt', 'password', 'last_login', 'confirmation_token', 'password_requested_at', 'roles', 'contact_id', 'first_name', 'last_name', 'login_count', 'sf_user_departmentid', 'send_meeting_invite'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'confirmation_token',
    ];

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }
}
