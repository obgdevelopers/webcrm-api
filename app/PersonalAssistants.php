<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $pa_id
 * @property string $salutation
 * @property string $first_name
 * @property string $last_name
 * @property string $office_phone
 * @property string $office_fax
 * @property string $mobile_phone
 * @property string $email
 * @property string $company_id
 * @property string $pa_notes
 * @property string $created_by
 * @property integer $created_date
 * @property string $last_updated_by
 * @property integer $last_updated_date
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 */
class PersonalAssistants extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['pa_id', 'salutation', 'first_name', 'last_name', 'office_phone', 'office_fax', 'mobile_phone', 'email', 'company_id', 'pa_notes', 'created_by', 'created_date', 'last_updated_by', 'last_updated_date', 'removed', 'created_at', 'updated_at'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";

    /**
     * All contacts for the project
     * 
     * @param  [string] where
     * @param  [string] bind
     * @param  [int] limit
     * @param  [int] offset
     * 
     * @return [object] table
     */
    public function buildRaw($projects)
    {
        // $projects = $parameters['projects'];
        // $projects = explode(',', $projects);
        $year = Projects::whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = Projects::whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $query = $this->join('companies', 'companies.company_id', '=', 'personal_assistants.company_id')
                    ->where('companies.removed', false)
                    ->where('personal_assistants.removed', false)
                    ->where('companies.country_id', $country->id)
                    ->select('personal_assistants.*')
                    ->distinct('personal_assistants.id');
        // $query = $this->where('companies.removed', false)
        //             ->select('companies.company_id AS id')
        //             ->distinct('id');
                    
        // $query->where('companies.country_id', $country->id);
        $count = $query->count();

        $assistants = $query->get();

        $response = [
            'table' => $assistants,
            'count' => $count
        ];

        return $response;
    }
}
