<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $advert_id
 * @property integer $crm_userid
 * @property int $project_year
 * @property string $quick_description
 * @property string $company_id
 * @property integer $contract_date
 * @property string $signatory
 * @property string $obg_signatory
 * @property string $accounts_contact
 * @property string $secondary_accounts_contact
 * @property string $artwork_contact
 * @property string $invoice_recipient_contact
 * @property string $ad_type
 * @property string $regions_included
 * @property string $chapter_name
 * @property string $placement
 * @property string $size
 * @property string $volume_of_spaces
 * @property string $special_conditions
 * @property string $caveats
 * @property string $other_countries
 * @property integer $subscriptions_to_site
 * @property string $price_offered
 * @property string $signed_fee
 * @property string $signed_fee_deals_in
 * @property string $signed_fee_by_cd
 * @property string $signed_fee_total
 * @property string $type_of_barter
 * @property string $barter_value
 * @property string $barter_details
 * @property string $share_of_regional_deal
 * @property string $address_type
 * @property string $billing_address_line_1
 * @property string $billing_address_line_2
 * @property string $billing_address_town_city
 * @property string $billing_address_region
 * @property string $billing_postal_code
 * @property string $created_by
 * @property integer $created_date
 * @property integer $last_updated_date
 * @property string $last_updated_by
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 * @property string $chapter_id
 * @property integer $chapter_weight
 * @property string $cancellation_reason
 * @property boolean $saved_as_draft
 */
class Contracts extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'adverts';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['advert_id', 'crm_userid', 'project_year', 'quick_description', 'company_id', 'contract_date', 'signatory', 'obg_signatory', 'accounts_contact', 'secondary_accounts_contact', 'artwork_contact', 'invoice_recipient_contact', 'ad_type', 'regions_included', 'chapter_name', 'placement', 'size', 'volume_of_spaces', 'special_conditions', 'caveats', 'other_countries', 'subscriptions_to_site', 'price_offered', 'signed_fee', 'signed_fee_deals_in', 'signed_fee_by_cd', 'signed_fee_total', 'type_of_barter', 'barter_value', 'barter_details', 'share_of_regional_deal', 'address_type', 'billing_address_line_1', 'billing_address_line_2', 'billing_address_town_city', 'billing_address_region', 'billing_postal_code', 'created_by', 'created_date', 'last_updated_date', 'last_updated_by', 'removed', 'created_at', 'updated_at', 'chapter_id', 'chapter_weight', 'cancellation_reason', 'saved_as_draft', 'customer_care_contact', 'contract_received', 'online_banner_days', 'ipad_banner_days', 'report_copies', 'online_banner_type', 'advisory_assignee', 'video_advert_days'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";

}
