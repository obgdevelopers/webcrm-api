<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompaniesController extends Controller
{
    /**
     * Base page for companies
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] count
     */
    public function index(Request $request)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        
        $companiesCount = $this->companies->where('removed', false)->count();

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'All queries successful!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'count' => [
                'result' => $companiesCount,
                'total' => $companiesCount,
                'name' => 'companies'
            ]
        ];

        return response()->json($response);
    }

    /**
     * All companies for project
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $result = $this->companies->buildRaw($projects);
        $table = $result['table'];
        $count = $result['count'];

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Companies were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $table,
            'count' => [
                [
                    'result' => count($table),
                    'total' => $count
                ]
            ]
        ];

        return response()->json($response);
    }

    /**
     * Fetch updates for companies
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/updates');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();

        $datetime = $input['datetime'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $companies = $this->companies
                    ->where('companies.updated_at', '>', $datetime)
                    ->where('companies.country_id', $country->id)
                    ->distinct('id')
                    ->get();

        if(count($companies) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Companies were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $companies,
            'count' => [
                'result' => count($companies),
                'total' => count($companies),
                'name' => 'companies'
            ]
        ];

        return response()->json($response, $code);
    }
    
    /**
     * Fetch updates for actions
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function actionupdates(Request $request)
    {
        $baseUrl = url('/api/updates');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();

        $datetime = $input['datetime'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $actions = $this->companiesActionNextYear
                    ->where('updated_date', '>', $datetime)
                    //->where('project_year', $year->text)
                    ->distinct('id')
                    ->get();

        if(count($actions) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Companies action were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $actions,
            'count' => [
                'result' => count($actions),
                'total' => count($actions),
                'name' => 'actions'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Table for companies page
     *
     * @param  [string] company_name
     * @param  [array] sectors
     * @param  [string] address
     * @param  [array] status
     * @param  [array] assignee
     * @param  [array] rating
     * @param  [array] status_previous
     * @param  [string] turnover
     * @param  [string] company_size
     * @param  [string] company_source
     * @param  [int] history
     * @param  [array] tags
     * @param  [string] met_from
     * @param  [string] met_before
     * @param  [string] contacted_from
     * @param  [string] contacted_before
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function table(Request $request)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $parameters = $request->all();
        $page = 0;
        if(isset($parameters['page'])) {
            $page = $parameters['page'];
        }

        $actionToBeTaken = $this->projects->whereIn('id', $projects)
                    ->select('action_to_be_taken AS action')
                    ->first();

        $result = $this->companies->buildTable($parameters, 50, 50 * $page, $projects);
        $table = $result['table'];
        $count = $result['count'];

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Table query successful!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $table,
            'actionToBeTaken' => isset($actionToBeTaken) ? $actionToBeTaken->action : 0,
            'count' => [
                [
                    'result' => count($table),
                    'total' => $count
                ]
            ]
        ];

        return response()->json($response);
    }

    /**
     * List of companies
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function list(Request $request)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $companies = $this->companies
                        ->leftJoin('contacts', function($join) {
                            $join->on('contacts.company_id', '=', 'companies.company_id')
                                ->where(function($subquery){
                                    $subquery->where('contacts.removed', false)
                                    ->where('contacts.contact_type', 'Main');
                                });
                        })
                        ->where('companies.country_id', $country->id)
                        ->where('companies.removed', false)
                        // ->where('contacts.removed', false)
                        // ->where('contacts.contact_type', 'Main')
                        ->select('companies.company_id AS hash', 'companies.name AS name', 'contacts.contact_id AS contact_hash')
                        ->distinct()
                        ->get();



        if($companies) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Companies were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Companies were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $companies,
            'count' => [
                'result' => count($companies),
                'total' => count($companies),
                'name' => 'companies'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * View details of company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] records
     */
    public function view(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        // ->select('companies.id AS id', 'companies.company_id AS hash', 'companies.name AS name', 'contacts.id AS assignee_id', 'contacts.first_name AS assignee_firstname', 'contacts.last_name AS assignee_lastname', 'companies.sector as sector', 'company_status.name AS status', 'companies.office_phone AS landline', 'companies.office_address_line_1 AS address_line1', 'companies.office_address_line_2 AS address_line2', 'companies.office_address_town_city AS town')

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $query = $this->companies
                        ->leftJoin('contacts', 'contacts.contact_id', '=', 'companies.assigned_user')
                        ->leftJoin('companies_project_status', function($join) use ($projects) {
                            $join->on('companies_project_status.company_hash_id', '=', 'companies.company_id')
                                ->whereIn('companies_project_status.project_id', $projects);
                        })
                        ->leftJoin('company_status', 'company_status.id', '=', 'companies_project_status.company_status_id')
                        ->leftJoin('company_advisory_potential', 'company_advisory_potential.company_id', '=', 'companies.company_id')
                        ->leftJoin('countries', 'countries.id', '=', 'companies.country_id')
                        ->where('companies.removed', false)
                        ->where('companies.company_id', $companyId)
                        ->select('companies.id AS id', 'companies.company_id AS hash', 'companies.name AS name', 'contacts.id AS assignee_id', 'contacts.contact_id AS assignee_hash', 'contacts.first_name AS assignee_firstname', 'contacts.last_name AS assignee_lastname', 'company_status.name AS status', 'companies.web_address as website', 'companies.sector as sector', 'companies.parent_company_type AS parent_type', 'companies.country_custom AS country_custom', 'companies.financial_information AS financial_information', 'companies.advertised_in_competitor_report AS advertised_in_competitor', 'companies.previous_client AS previous_client', 'companies.company_notes AS company_notes', 'companies.source AS source', 'companies.company_size AS size', 'companies.turnover AS turnover', 'companies.rating AS rating', 'companies.office_phone AS landline', 'companies.office_fax AS fax', 'companies.office_address_line_1 AS address_line1', 'companies.office_address_line_2 AS address_line2', 'companies.office_address_town_city AS town', 'companies.office_address_region AS region', 'companies.office_postal_code AS postal_code', 'companies.office_directions AS directions', 'companies.billing_address_line_1 AS billing_address_line1', 'companies.billing_address_line_2 AS billing_address_line2', 'companies.billing_address_town_city AS billing_town', 'companies.billing_address_region AS billing_region', 'companies.billing_postal_code AS billing_postal_code', 'companies.advisory_potential AS potential', 'company_advisory_potential.advisory_potential AS reason', 'countries.timezone');
                        
        // foreach($projects as $project) {
        //     $query->where('tblCompaniesProjectStatus.tblProjects_fldProjectId', '=', $project);
        // }

        $company = $query->first();
        $hash = $company['hash'];

        $reminder = $this->reminders->where('reminder_companyid', $hash)->where('reminder_active', true)->select('reminder_date as date')->first();
        $company['reminder'] = $reminder;
        $notes = $this->companiesNotes->where('company_notes_companyid', $hash)->exists();
        $histories = $this->histories->where('company_id', $hash)->where('project_year', $year->text)->where('removed', false)->exists();

        $company['notes'] = $notes;
        $company['logs'] = $histories;
        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Company was found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $company
        ];

        return response()->json($response);
    }

    /**
     * Delete company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function delete(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $input = $request->all();

        $contacts = $this->contacts->where('company_id', $companyId)->where('removed', false)->first();
        $meetings = $this->meetings->where('company_id', $companyId)->where('removed', false)->first();
        $contracts = $this->contracts->where('company_id', $companyId)->where('removed', false)->first();

        $data = [];

        if (!$contacts && !$meetings && !$contracts) {
            $company = $this->companies->where('company_id', $companyId)->update(['removed' => 1]);
            if($company) {
                $code = 200;
                $status = [
                    'type' => 'success',
                    'code' => 200,
                    'message' => 'Company was deleted successfully!',
                    'error' => false
                ];
                $data = $company;
            } else {
                $code = 404;
                $status = [
                    'type' => 'success',
                    'code' => 404,
                    'message' => 'Company was not deleted!',
                    'error' => false
                ];
            }
        } else {
            $code = 403;
            $status = [
                'type' => 'success',
                'code' => 403,
                'message' => 'Company has relevant records!',
                'error' => false
            ];
        }
        

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'companies'
            ]
        ];

        return response()->json($response, $code);

    }

    /**
     * Add a company
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function add(Request $request)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $countries = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $input = $request->all();
        $name = $input['name'];
        $website = $input['website'];
        $sector = $input['sector'];
        $country = $input['country'];
        $parent = $input['parent'];
        $assignee = $input['assignee'];
        $source = $input['source'];
        $size = $input['size'];
        $turnover = $input['turnover'];
        $rating = $input['rating'];
        $financial = $input['financial'];
        $notes = $input['notes'];
        $advertised = $input['advertised'];
        $address1 = $input['address1'];
        $address2 = $input['address2'];
        $town = $input['town'];
        $region = $input['region'];
        $postal = $input['postal'];
        $directions = $input['directions'];
        $landline = $input['landline'];
        $fax = $input['fax'];
        $billingAddress1 = $input['billingAddress1'];
        $billingAddress2 = $input['billingAddress2'];
        $billingTown = $input['billingTown'];
        $billingRegion = $input['billingRegion'];
        $billingPostal = $input['billingPostal'];

        $company = $this->companies
                    ->create([
                        'company_id' => $this->generateHash('companies'),
                        'name' => $name,
                        'crm_userid' => 1,
                        'parent_company_type' => $parent,
                        'sector' => $sector,
                        'office_phone' => $landline,
                        'office_fax' => $fax,
                        'office_address_line_1' => $address1,
                        'office_address_line_2' => $address2,
                        'office_address_town_city' => $town,
                        'office_address_region' => $region,
                        'office_postal_code' => $postal,
                        'office_directions' => $directions,
                        'billing_address_line_1' => $billingAddress1,
                        'billing_address_line_2' => $billingAddress2,
                        'billing_address_town_city' => $billingTown,
                        'billing_address_region' => $billingRegion,
                        'billing_postal_code' => $billingPostal,
                        'country_id' => $countries['id'],
                        'financial_information' => $financial,
                        'rating' => $rating,
                        'company_notes' => $notes,
                        'previous_client' => 0,
                        'web_address' => $website,
                        'advertised_in_competitor_report' => $advertised,
                        'country_custom' => $country,
                        'assigned_user' => $assignee != null ? $assignee : '',
                        'removed' => false,
                        'turnover' => $turnover,
                        'company_size' => $size,
                        'source' => $source
                    ]);

        if (isset($input['tags'])) {
            $tags = $input['tags'];
            foreach ($tags as $tag) {
                $this->entitiesTags
                    ->create([
                        'entity_id' => $company->company_id,
                        'entity_type' => 'company',
                        'project_id' => $projects[0],
                        'term_id' => $tag
                    ]);
            }
        }
        $data = [];
        if($company) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Company was created successfully!',
                'error' => false
            ];
            $data = $company->company_id;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Company was not created!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'companies'
            ]
        ];

        return response()->json($response, $code);

    }

    /**
     * Update a company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function update(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $name = $input['name'];
        $website = $input['website'];
        $sector = $input['sector'];
        $country = $input['country'];
        $parent = $input['parent'];
        $assignee = $input['assignee'];
        $source = $input['source'];
        $size = $input['size'];
        $turnover = $input['turnover'];
        $rating = $input['rating'];
        $financial = $input['financial'];
        $notes = $input['notes'];
        $advertised = $input['advertised'];
        $address1 = $input['address1'];
        $address2 = $input['address2'];
        $town = $input['town'];
        $region = $input['region'];
        $postal = $input['postal'];
        $directions = $input['directions'];
        $landline = $input['landline'];
        $fax = $input['fax'];
        $billingAddress1 = $input['billingAddress1'];
        $billingAddress2 = $input['billingAddress2'];
        $billingTown = $input['billingTown'];
        $billingRegion = $input['billingRegion'];
        $billingPostal = $input['billingPostal'];

        $company = $this->companies
                    ->where('company_id', $companyId)
                    ->update([
                        'name' => $name,
                        'parent_company_type' => $parent,
                        'sector' => $sector,
                        'office_phone' => $landline,
                        'office_fax' => $fax,
                        'office_address_line_1' => $address1,
                        'office_address_line_2' => $address2,
                        'office_address_town_city' => $town,
                        'office_address_region' => $region,
                        'office_postal_code' => $postal,
                        'office_directions' => $directions,
                        'billing_address_line_1' => $billingAddress1,
                        'billing_address_line_2' => $billingAddress2,
                        'billing_address_town_city' => $billingTown,
                        'billing_address_region' => $billingRegion,
                        'billing_postal_code' => $billingPostal,
                        'financial_information' => $financial,
                        'rating' => $rating,
                        'company_notes' => $notes,
                        'web_address' => $website,
                        'advertised_in_competitor_report' => $advertised,
                        'country_custom' => $country,
                        'assigned_user' => $assignee != null ? $assignee : '',
                        'turnover' => $turnover,
                        'company_size' => $size,
                        'source' => $source
                    ]);

        if (isset($input['tags'])) {
            $tags = $input['tags'];

            $this->entitiesTags
                ->where(['entity_id' => $companyId, 'entity_type' => 'company', 'project_id' => $projects[0]])
                ->whereNotIn('term_id', $tags)
                ->delete();

            foreach ($tags as $tag) {
                $this->entitiesTags
                    ->firstOrCreate(
                        ['entity_id' => $companyId, 'entity_type' => 'company', 'project_id' => $projects[0], 'term_id' => $tag]
                    );
            }
        }

        if(isset($input['contact'])) {
            $contact = $input['contact'];
            $this->contacts
                ->where('contact_id', '!=', $contact)
                ->where('company_id', $companyId)
                ->where('removed', false)
                ->update(['contact_type' => 'Other']);

            $this->contacts
                ->where('contact_id', $contact)
                ->where('company_id', $companyId)
                ->update(['contact_type' => 'Main']);
        }

        $data = [];
        if($company) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Company was updated successfully!',
                'error' => false
            ];
            $data = $companyId;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Company was not created!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'companies'
            ]
        ];

        return response()->json($response, $code);

    }

    /**
     * All companies actions next year
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function actions(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $result = $this->companiesActionNextYear
                ->join('companies', 'companies.company_id', '=', 'meeting_notes_action_next_year.company_id')
                ->where('companies.country_id', $country->id)
                ->select('meeting_notes_action_next_year.id', 'meeting_notes_action_next_year.meeting_action_id', 'meeting_notes_action_next_year.company_id', 'meeting_notes_action_next_year.project_year', 'meeting_notes_action_next_year.action', 'meeting_notes_action_next_year.note', 'meeting_notes_action_next_year.created_date', 'meeting_notes_action_next_year.updated_date', 'meeting_notes_action_next_year.updated_by')
                ->get();     

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Actions were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $result,
            'count' => count($result)
        ];

        return response()->json($response);
    }
}