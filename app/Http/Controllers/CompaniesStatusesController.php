<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompaniesStatusesController extends Controller
{
    /**
     * Find statuses for companies in projects
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function projects(Request $request)
    {
        $baseUrl = url('/api/projects');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $statuses = $this->companiesProjectsStatuses
                ->whereIn('project_id', $projects)
                ->get();

        if(count($statuses) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Companies\' project statuses were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Companies\' project statuses were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $statuses,
            'count' => [
                'result' => count($statuses),
                'total' => count($statuses),
                'name' => 'companies_projects_statuses'
            ]
        ];

        return response()->json($response, $code);
    }

}