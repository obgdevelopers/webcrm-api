<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MeetingsAttendancesController extends Controller
{
    /**
     * All meeting attendees
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        
        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $meetings = $this->meetings
                    ->join('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('companies.country_id', $country->id)
                    ->where('meeting_notes.removed', false)
                    ->where('companies.removed', false)
                    ->select('meeting_notes.meeting_note_id')
                    ->distinct('meeting_notes.id')
                    ->get();     
        
        $ids = [];
        foreach ($meetings as $meeting) {
            array_push($ids, $meeting->meeting_note_id);
        }

        $files = $this->meetingsAttendances
                    ->whereIn('meeting_note_attendance.meeting_note_id', $ids)
                    ->where('meeting_note_attendance.removed', false)
                    ->select('meeting_note_attendance.*')
                    ->distinct('meeting_note_attendance.id')
                    // ->take(20)
                    ->get();


        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Files were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $files,
            'count' => count($files)
        ];

        return response()->json($response);
    }

    /**
     * Fetch updates for attendees
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/updates');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        
        $datetime = $input['datetime'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $meetings = $this->meetings
                    ->join('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('companies.country_id', $country->id)
                    ->select('meeting_notes.meeting_note_id')
                    ->distinct('meeting_notes.id')
                    ->get();     
        
        $ids = [];
        foreach ($meetings as $meeting) {
            array_push($ids, $meeting->meeting_note_id);
        }

        $attendances = $this->meetingsAttendances
                    ->whereIn('meeting_note_attendance.meeting_note_id', $ids)
                    ->where('meeting_note_attendance.removed', false)
                    ->where('meeting_note_attendance.updated_at', '>', $datetime)
                    ->select('meeting_note_attendance.*')
                    ->distinct('meeting_note_attendance.id')
                    // ->take(20)
                    ->get();


        if(count($attendances) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Attendances were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $attendances,
            'count' => [
                'result' => count($attendances),
                'total' => count($attendances),
                'name' => 'attendances'
            ]
        ];

        return response()->json($response, $code);
    }
}
