<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PersonalAssistantsController extends Controller
{
    /**
     * View assistant details of contact
     * @param contactId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function contact(Request $request, $contactId)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();

        $assistant = $this->contacts
                            ->leftJoin('personal_assistants', 'personal_assistants.pa_id', '=', 'contacts.pa_id')
                            ->where('contacts.contact_id', $contactId)
                            ->where('personal_assistants.removed', false)
                            ->select('personal_assistants.id AS id', 'personal_assistants.pa_id AS hash', 'personal_assistants.salutation AS salutation', 'personal_assistants.first_name AS firstname', 'personal_assistants.last_name AS lastname', 'personal_assistants.office_phone AS landline', 'personal_assistants.office_fax AS fax', 'personal_assistants.mobile_phone AS mobile', 'personal_assistants.email AS email', 'personal_assistants.pa_notes AS notes')
                            ->first();

        if(count($assistant) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Personal assistant was found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Personal assistant was not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $assistant,
            'count' => [
                'result' => count($assistant),
                'total' => count($assistant),
                'name' => 'assistants'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * List of assistants for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function companies(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $assistants = $this->personalAssistants
                            ->where('personal_assistants.company_id', $companyId)
                            ->where('personal_assistants.removed', false)
                            ->select('personal_assistants.id AS id', 'personal_assistants.pa_id AS hash', 'personal_assistants.salutation AS salutation', 'personal_assistants.first_name AS firstname', 'personal_assistants.last_name AS lastname', 'personal_assistants.office_phone AS landline', 'personal_assistants.office_fax AS fax', 'personal_assistants.mobile_phone AS mobile', 'personal_assistants.email AS email', 'personal_assistants.pa_notes AS notes')
                            ->get();

        if($assistants) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Personal assistants were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Personal assistants were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $assistants,
            'count' => [
                'result' => count($assistants),
                'total' => count($assistants),
                'name' => 'assistants'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * All assistants for project
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        // $iteration = $input['iteration'];

        $result = $this->personalAssistants->buildRaw($projects);
        $assistants = $result['table'];

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Personal assistants were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $assistants,
            'count' => count($assistants)
        ];

        return response()->json($response);
    }

    /**
     * Fetch updates for assistants
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $datetime = $input['datetime'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $contacts = $this->personalAssistants
                    ->join('companies', 'companies.company_id', '=', 'personal_assistants.company_id')
                    ->where('personal_assistants.updated_at', '>', $datetime)
                    ->where('companies.country_id', $country->id)
                    ->select('personal_assistants.*')
                    ->distinct('personal_assistants.id')
                    ->get();

        if(count($contacts) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contacts were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $contacts,
            'count' => [
                'result' => count($contacts),
                'total' => count($contacts),
                'name' => 'contacts'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * View company details
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function view(Request $request, $assistantId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $assistant = $this->personalAssistants
                            ->where('personal_assistants.pa_id', $assistantId)
                            ->where('personal_assistants.removed', false)
                            ->select('personal_assistants.id AS id', 'personal_assistants.pa_id AS hash', 'personal_assistants.salutation AS salutation', 'personal_assistants.first_name AS firstname', 'personal_assistants.last_name AS lastname', 'personal_assistants.office_phone AS landline', 'personal_assistants.office_fax AS fax', 'personal_assistants.mobile_phone AS mobile', 'personal_assistants.email AS email', 'personal_assistants.pa_notes AS notes')
                            ->first();

        if($assistant) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Personal assistant was found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Personal assistant was not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $assistant,
            'count' => [
                'result' => count($assistant),
                'total' => count($assistant),
                'name' => 'assistant'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Add a company
     * @param contactId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function add(Request $request, $contactId)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();
        $input = $request->all();

        $hash = $input['hash'];
        $salutation = $input['salutation'];
        $firstname = $input['firstname'];
        $lastname = $input['lastname'];
        $landline = $input['landline'];
        $fax = $input['fax'];
        $mobile = $input['mobile'];
        $notes = $input['notes'];
        $email = $input['email'];
        $user = $input['user'];

        $assistant = null;

        if ($hash == 'new') {
            $contact = $this->contacts->where('contact_id', $contactId)->first();
            $companyId = $contact['company_id'];
            $assistant = $this->personalAssistants
                ->create(
                    [
                    'pa_id' => $this->generateHash('personal_assistants'),
                    'salutation' => $salutation, 
                    'first_name' => $firstname, 
                    'last_name' => $lastname,
                    'office_phone' => $landline,
                    'office_fax' => $fax,
                    'mobile_phone' => $mobile,
                    'email' => $email,
                    'company_id' => $companyId,
                    'pa_notes' => $notes,
                    'created_by' => $user,
                    'created_date' => time(),
                    'removed' => 0,
                    'last_updated_by' => $user,
                    'last_updated_date' => time()
                    ]);

            if($assistant) {
                $this->contacts->where('contact_id', $contactId)->update(['pa_id' => $assistant->pa_id]);
                $code = 200;
                $status = [
                    'type' => 'success',
                    'code' => 200,
                    'message' => 'Personal assistant was created!',
                    'error' => false
                ];
            } else {
                $code = 404;
                $status = [
                    'type' => 'success',
                    'code' => 404,
                    'message' => 'Personal assistant was not created!',
                    'error' => false
                ];
            }
        } else {
            if ($hash != '') {
                $assistant = $this->personalAssistants
                ->where('pa_id', $hash)
                ->update(
                    ['salutation' => $salutation, 
                    'first_name' => $firstname, 
                    'last_name' => $lastname,
                    'office_phone' => $landline,
                    'office_fax' => $fax,
                    'mobile_phone' => $mobile,
                    'email' => $email,
                    'pa_notes' => $notes,
                    'last_updated_by' => $user,
                    'last_updated_date' => time()
                    ]);

                if($assistant) {
                    $this->contacts->where('contact_id', $contactId)->update(['pa_id' => $hash]);
                    $code = 200;
                    $status = [
                        'type' => 'success',
                        'code' => 200,
                        'message' => 'Personal assistant was updated!',
                        'error' => false
                    ];
                } else {
                    $code = 404;
                    $status = [
                        'type' => 'success',
                        'code' => 404,
                        'message' => 'Personal assistant was not updated!',
                        'error' => false
                    ];
                }
            } else {
                $code = 200;
                $status = [
                    'type' => 'success',
                    'code' => 200,
                    'message' => 'Personal assistant was updated!',
                    'error' => false
                ];
                $this->contacts->where('contact_id', $contactId)->update(['pa_id' => '']);
            }
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $assistant,
            'count' => [
                'result' => count($assistant),
                'total' => count($assistant),
                'name' => 'assistant'
            ]
        ];

        return response()->json($response, $code);
    }
}