<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TermsController extends Controller
{
    /**
     * Terms for current project
     *
     * @param [string] type
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function index(Request $request)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();

        $parameters = $request->all();
        $headers = $request->headers->all();
        $sessions = $headers['session-project'][0];
        $sessions = explode(',', $sessions);

        $request->validate([
            'type' => 'required|string'
        ]);

        $accesses = [];
        foreach($sessions as $session) {
            $push = [];
            $projects = $this->projects
            ->where('id', $session)
            ->select('project_year_id AS year_id', 'country_id AS country_id', 'project_year AS year')
            ->first();
            $push['year_id'] = $projects['year_id'];
            $push['country_id'] = $projects['country_id'];
            $push['year'] = $projects['year'];
            array_push($accesses, $push);
        }

        $parameters = $request->all();
        $type = $parameters['type'];

        $terms = $this->terms->findByType($type, $accesses);
        
        if(count($terms) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Terms were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Terms with this type does not exist!',
                'error' => 'false'
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $terms,
            'count' => [
                'result' => count($terms),
                'total' => count($terms),
                'name' => 'terms'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Find tags of contact for current project
     * @param contactId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function contacts(Request $request, $contactId)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $tags = $this->entitiesTags
                    ->leftJoin('tags_terms', 'tags_terms.id', '=', 'entity_project_tag.term_id')
                    ->where('entity_project_tag.entity_id', '=', $contactId)
                    ->where('entity_project_tag.entity_type', 'contact')
                    ->whereIn('entity_project_tag.project_id', $projects)
                    ->select('tags_terms.id AS id', 'tags_terms.name AS text')
                    ->get();

        if(count($tags) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Tags were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Tags were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $tags,
            'count' => [
                'result' => count($tags),
                'total' => count($tags),
                'name' => 'tags'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Find tags of companies for current project
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function companies(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $tags = $this->entitiesTags
                    ->leftJoin('tags_terms', 'tags_terms.id', '=', 'entity_project_tag.term_id')
                    ->where('entity_project_tag.entity_id', '=', $companyId)
                    ->where('entity_project_tag.entity_type', 'company')
                    ->whereIn('entity_project_tag.project_id', $projects)
                    ->select('tags_terms.id AS id', 'tags_terms.name AS text')
                    ->get();

        if(count($tags) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Tags were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Tags were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $tags,
            'count' => [
                'result' => count($tags),
                'total' => count($tags),
                'name' => 'tags'
            ]
        ];

        return response()->json($response, $code);
    }
}