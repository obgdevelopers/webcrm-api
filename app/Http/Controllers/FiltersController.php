<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FiltersController extends Controller
{
    /**
     * Terms for current project
     *
     * @param [string] type
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function index(Request $request)
    {
        $baseUrl = url('/api/filters');
        $selfUrl = url()->full();

        $parameters = $request->all();
        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $request->validate([
            'type' => 'required|string'
        ]);

        // $accesses = [];
        // foreach($sessions as $session) {
        //     $push = [];
        //     $projects = $this->projects
        //     ->where('id', $session)
        //     ->select('project_year_id AS year_id', 'country_id AS country_id', 'project_year AS year')
        //     ->first();
        //     $push['year_id'] = $projects['year_id'];
        //     $push['country_id'] = $projects['country_id'];
        //     $push['year'] = $projects['year'];
        //     array_push($accesses, $push);
        // }

        $filters = $this->savedSearches->findByType($parameters, $projects);
        
        if(count($filters) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Filters were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Filters does not exist!',
                'error' => 'false'
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $filters,
            'count' => [
                'result' => count($filters),
                'total' => count($filters),
                'name' => 'filters'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Save a filter for employee
     *
     * @param employeeId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function user(Request $request, $employeeId)
    {
        $baseUrl = url('/api/employees');
        $selfUrl = url()->full();

        $parameters = $request->all();
        $headers = $request->headers->all();
        $sessions = $headers['session-project'][0];
        $sessions = explode(',', $sessions);

        $name = $parameters['name'];
        $privacy = $parameters['privacy'];
        if ($privacy == "true") {
            /* It's the other way around because the field is "visible_to_all_users" which means it is not private */
            $privacy = 0;
        } else {
            $privacy = 1;
        }

        $type = $parameters['type'];
        $query = $parameters['query'];

        $url = http_build_query($query);

        foreach($sessions as $session) {
            $filter = $this->savedSearches->create([
                'name' => $name,
                'parameters' => '"?'.$url.'"',
                'saved_by' => $employeeId,
                'visible_to_all_users' => $privacy,
                'project_id' => $session,
                'entity_type' => $type
            ]);
        }
        
        
        if($filter) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Filters were created!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Filters were not created!',
                'error' => 'false'
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $filter,
            'count' => [
                'result' => count($filter),
                'total' => count($filter),
                'name' => 'filter'
            ]
        ];
        return response()->json($response, $code);
    }

    /**
     * Delete filter
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function delete(Request $request, $filterId)
    {
        $baseUrl = url('/api/filters');
        $selfUrl = url()->full();

        $input = $request->all();

        $filter = $this->savedSearches->find($filterId)->delete();

        $data = [];
        if($filter) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Saved filter was deleted successfully!',
                'error' => false
            ];
            $data = $filter;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Saved filter was not deleted!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'filters'
            ]
        ];

        return response()->json($response, $code);

    }
}
