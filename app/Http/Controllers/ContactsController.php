<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactsController extends Controller
{
    /**
     * Base page for contacts
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] count
     */
    public function list(Request $request)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();
        
        $parameters = $request->all();
        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $query = $this->contacts
                    ->leftJoin('companies', 'companies.company_id', '=', 'contacts.company_id')
                    ->where('companies.country_id', $country->id)
                    ->where('companies.removed', false)
                    ->select('contacts.id AS id', 'contacts.contact_id AS hash', 'contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'companies.name AS company_name')
                    ->distinct('id');
                    // ->get();

        if(isset($parameters['query'])) {
            $query->where(function($subquery) use ($parameters){
                $subquery->where('contacts.first_name', 'LIKE', '%'.$parameters['query'].'%')
                  ->orWhere('contacts.last_name', 'LIKE', '%'.$parameters['query'].'%');
            });
        }

        $contacts = $query->get();

        if(count($contacts) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contacts were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Contact were not found!',
                'error' => false
            ];
        }
        
        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $contacts,
            'count' => [
                'result' => count($contacts),
                'total' => count($contacts),
                'name' => 'contacts'
            ]
        ];

        return response()->json($response, $code);
    }
  
    /**
     * Table for contacts page
     *
     * @param  [string] first_name
     * @param  [string] last_name
     * @param  [string] job_title
     * @param  [string] company_name
     * @param  [string] phone_number
     * @param  [string] mobile_number
     * @param  [string] email_address
     * @param  [array] tags
     * @param  [array] contacted_date
     * @param  [array] met_date
     * @param  [int] page
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function table(Request $request)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();

        $parameters = $request->all();
        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);
        
        $page = 0;
        if(isset($parameters['page'])) {
            $page = $parameters['page'];
        }

        $result = $this->contacts->buildTable($parameters, 50, 50 * $page, $projects);
        $table = $result['table'];
        $count = $result['count'];

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Table query successful!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $table,
            'count' => [
                'result' => count($table),
                'total' => $count,
                'name' => 'contacts'
            ]
        ];

        return response()->json($response);
    }

    /**
     * Add a contact
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function add(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        
        $type = $input['type'];
        $salutation = $input['salutation'];
        $firstname = $input['firstname'];
        $lastname = $input['lastname'];
        $middlename = $input['middlename'];
        $title = $input['title'];
        $landline = $input['landline'];
        $mobile = $input['mobile'];
        $fax = $input['fax'];
        $email = $input['email'];
        $address1 = $input['address1'];
        $address2 = $input['address2'];
        $address3 = $input['address3'];
        $town = $input['town'];
        $region = $input['region'];
        $postal = $input['postal'];
        $user = $input['user'];

        if ($type == 'Main') {
            $this->contacts
                ->where('contact_type', 'Main')
                ->where('company_id', $companyId)
                ->where('removed', false)
                ->update(['contact_type' => 'Other']);
        }
        
        /* No last contacted and last met yet */
        $contact = $this->contacts
                        ->create([
                            'contact_id' => $this->generateHash('contacts'),
                            'salutation' => $salutation,
                            'first_name' => $firstname,
                            'middle_name' => $middlename,
                            'last_name' => $lastname,
                            'job_title' => $title,
                            'office_phone' => $landline,
                            'office_fax' => $fax,
                            'mobile_phone' => $mobile,
                            'email' => $email,
                            'office_address_line_1' => $address1,
                            'office_address_line_2' => $address2,
                            'office_address_line_3' => $address3,
                            'office_address_town_city' => $town,
                            'office_address_region' => $region,
                            'office_postal_code' => $postal,
                            'contact_type' => $type,
                            'company_id' => $companyId,
                            'created_by' => $user,
                            'created_date' => time(),
                            'last_updated_by' => $user,
                            'last_updated_date' => time(),
                            'economic_updates' => 0,
                            'left_company' => 0,
                            'removed' => 0,
                        ]);

        if (isset($input['tags'])) {
            $tags = $input['tags'];
            foreach ($tags as $tag) {
                $this->entitiesTags
                    ->create([
                        'entity_id' => $contact->contact_id,
                        'entity_type' => 'contact',
                        'project_id' => $projects[0],
                        'term_id' => $tag
                    ]);
            }
        }

        $data = [];
        if($contact) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contact was created successfully!',
                'error' => false
            ];
            $data = $contact->contact_id;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Contact was not created!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'companies'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Update a contact
     * @param contactId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function update(Request $request, $contactId)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        
        $companyId = $input['company'];
        $type = $input['type'];
        $salutation = $input['salutation'];
        $firstname = $input['firstname'];
        $lastname = $input['lastname'];
        $middlename = $input['middlename'];
        $title = $input['title'];
        $landline = $input['landline'];
        $mobile = $input['mobile'];
        $fax = $input['fax'];
        $email = $input['email'];
        $address1 = $input['address1'];
        $address2 = $input['address2'];
        $address3 = $input['address3'];
        $town = $input['town'];
        $region = $input['region'];
        $postal = $input['postal'];
        $left = $input['left'];
        $user = $input['user'];

        if ($type == 'Main') {
            $this->contacts
                ->where('contact_type', 'Main')
                ->where('company_id', $companyId)
                ->where('removed', false)
                ->update(['contact_type' => 'Other']);
        }
        
        /* No last contacted and last met yet */
        $contact = $this->contacts
                        ->where('contact_id', $contactId)
                        ->update([
                            'salutation' => $salutation,
                            'first_name' => $firstname,
                            'middle_name' => $middlename,
                            'last_name' => $lastname,
                            'job_title' => $title,
                            'office_phone' => $landline,
                            'office_fax' => $fax,
                            'mobile_phone' => $mobile,
                            'email' => $email,
                            'office_address_line_1' => $address1,
                            'office_address_line_2' => $address2,
                            'office_address_line_3' => $address3,
                            'office_address_town_city' => $town,
                            'office_address_region' => $region,
                            'office_postal_code' => $postal,
                            'contact_type' => $type,
                            'last_updated_by' => $user,
                            'last_updated_date' => time(),
                            'left_company' => $left
                        ]);

        if (isset($input['tags'])) {
            $tags = $input['tags'];

            $this->entitiesTags
                ->where(['entity_id' => $contactId, 'entity_type' => 'contact', 'project_id' => $projects[0]])
                ->whereNotIn('term_id', $tags)
                ->delete();

            foreach ($tags as $tag) {
                $this->entitiesTags
                    ->firstOrCreate(
                        ['entity_id' => $contactId, 'entity_type' => 'contact', 'project_id' => $projects[0], 'term_id' => $tag]
                    );
            }
        }

        $data = [];
        if($contact) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contact was updated successfully!',
                'error' => false
            ];
            $data = $contactId;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Contact was not updated!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'companies'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * All companies for project
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        // $iteration = $input['iteration'];

        $result = $this->contacts->buildRaw($projects);
        $contacts = $result['table'];

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contacts were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $contacts,
            'count' => count($contacts)
        ];

        return response()->json($response);
    }

    /**
     * Fetch updates for contacts
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $datetime = $input['datetime'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $contacts = $this->contacts
                    ->join('companies', 'companies.company_id', '=', 'contacts.company_id')
                    ->where('contacts.updated_at', '>', $datetime)
                    ->where('companies.country_id', $country->id)
                    ->select('contacts.*')
                    ->distinct('contacts.id')
                    ->get();

        if(count($contacts) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contacts were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $contacts,
            'count' => [
                'result' => count($contacts),
                'total' => count($contacts),
                'name' => 'contacts'
            ]
        ];

        return response()->json($response, $code);
    }
    
    /**
     * View details of contact
     * @param contactId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function view(Request $request, $contactId)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();
        
        /* No last contacted and last met yet */
        $contact = $this->contacts
                        ->leftJoin('companies', 'companies.company_id', '=', 'contacts.company_id')
                        ->where('contacts.contact_id', $contactId)
                        ->where('contacts.removed', false)
                        ->select('contacts.contact_id AS id','contacts.salutation AS salutation', 'contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'contacts.middle_name AS middlename', 'contacts.job_title AS job_title', 'contacts.office_phone AS landline', 'contacts.mobile_phone AS mobile', 'contacts.office_fax AS fax', 'contacts.email AS email', 'contacts.office_address_line_1 AS address_line1', 'contacts.office_address_line_2 AS address_line2', 'contacts.office_address_line_3 AS address_line3', 'contacts.office_address_town_city AS town', 'contacts.office_address_region AS region', 'contacts.office_postal_code AS postal_code', 'contacts.last_met AS last_met', 'companies.company_id AS company_hash', 'companies.name AS company_name', 'companies.office_address_line_1 AS company_address_line1', 'companies.office_address_line_2 AS company_address_line2', 'companies.office_address_town_city AS company_town', 'companies.office_address_region AS company_region', 'companies.office_postal_code AS company_postal_code', 'companies.office_directions AS company_directions', 'contacts.contact_type AS type', 'contacts.left_company AS left')
                        ->first();

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contact was found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $contact,
            'count' => [
                'result' => count($contact),
                'total' => count($contact),
                'name' => 'contacts'
            ]
        ];

        return response()->json($response);
    }

    /**
     * Find contacts for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function companies(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $contacts = $this->contacts
                    ->where('contacts.company_id', $companyId)
                    ->where('contacts.removed', false)
                    ->select('contacts.id AS id', 'contacts.contact_id AS hash', 'contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'contacts.job_title AS job_title', 'contacts.office_phone AS landline', 'contacts.mobile_phone AS mobile', 'contacts.email AS email_address', 'contacts.left_company AS left', 'contacts.contact_type AS type', 'contacts.pa_id AS personal_assistant', 'contacts.updated_at AS updated', 'contacts.created_at AS created')
                    ->orderBy('contacts.left_company')
                    ->orderBy('contacts.first_name')
                    ->get();

        if(count($contacts) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contacts were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Contacts were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $contacts,
            'count' => [
                'result' => count($contacts),
                'total' => count($contacts),
                'name' => 'contacts'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Delete contact
     * @param contactId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function delete(Request $request, $contactId)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();

        $input = $request->all();

        $histories = $this->histories->where('contact_id', $contactId)->where('removed', false)->first();
        $attendance = $this->meetingsAttendances->where('contact_id', $contactId)->where('removed', false)->first();
        $contracts = $this->contracts->where('signatory', $contactId)->where('removed', false)->first();
        $deliveries = $this->deliveries->where('contact_id', $contactId)->where('removed', false)->first();

        $data = [];

        if (!$histories && !$attendance && !$contracts && !$deliveries) {
            $contact = $this->contacts->where('contact_id', $contactId)->update(['removed' => 1]);

            if($contact) {
                $code = 200;
                $status = [
                    'type' => 'success',
                    'code' => 200,
                    'message' => 'Contact was deleted successfully!',
                    'error' => false
                ];
                $data = $contact;
            } else {
                $code = 404;
                $status = [
                    'type' => 'success',
                    'code' => 404,
                    'message' => 'Contact was not deleted!',
                    'error' => false
                ];
            }
        } else {
            $code = 403;
            $status = [
                'type' => 'success',
                'code' => 403,
                'message' => 'Contact has relevant records!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'contacts'
            ]
        ];

        return response()->json($response, $code);

    }
}