<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContractsController extends Controller
{
    /**
     * Find contracts of companies for current project
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function companies(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $contracts = $this->contracts
                    ->leftJoin('advert_chapter', 'advert_chapter.chapter_id', '=', 'adverts.chapter_id')
                    ->where('adverts.company_id', '=', $companyId)
                    ->select('adverts.id AS id', 'adverts.advert_id AS hash', 'adverts.project_year AS year', 'advert_chapter.chapter_name AS chapter', 'adverts.placement AS placement', 'adverts.signed_fee_total AS signed_fee_total', 'adverts.contract_date AS contract_date', 'adverts.barter_value AS barter_value', 'adverts.ad_type AS type', 'adverts.barter_details AS barter_details', 'adverts.removed AS cancelled', 'adverts.cancellation_reason AS reason')
                    ->orderBy('adverts.project_year', 'DESC')
                    ->orderBy('adverts.contract_date', 'DESC')
                    ->get();


        $list = [];
        if(count($contracts) > 0) {
            foreach($contracts as $contract) {
                $type = strtolower($contract->type);
                switch($type) {
                    case 'partnership':
                    case 'cash/partnership':
                            $hash = $contract->hash;
                            $partnership = $this->contractsPartnerships->where('advert_id', $hash)->select('advertpartnerships_value AS partnership_value', 'advertpartnerships_details AS partnership_detail')->first();
                            $contract['partnership_value'] = $partnership->partnership_value;
                            $contract['partnership_detail'] = $partnership->partnership_detail;
                        break;
                }
                array_push($list, $contract);
            }

            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contracts were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Contracts were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $list,
            'count' => [
                'result' => count($list),
                'total' => count($list),
                'name' => 'contracts'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * All contract for project
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $contracts = $this->contracts
                        ->join('companies', 'companies.company_id', '=', 'adverts.company_id')
                        ->where('companies.country_id', $country->id)
                        ->where('adverts.project_year', $year->text)
                        ->where('companies.removed', false)
                        ->select('adverts.*')
                        ->distinct('adverts.id')
                        ->get();

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contracts were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $contracts,
            'count' => count($contracts)
        ];

        return response()->json($response);
    }

    /**
     * Fetch contracts for chapters
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $datetime = $input['datetime'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $contracts = $this->contracts
                        ->join('companies', 'companies.company_id', '=', 'adverts.company_id')
                        ->where('companies.country_id', $country->id)
                        ->where('adverts.project_year', $year->text)
                        ->where('adverts.updated_at', '>', $datetime)
                        ->select('adverts.*')
                        ->distinct('adverts.id')
                        ->get();

        if(count($contracts) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contracts were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Contracts were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $contracts,
            'count' => [
                'result' => count($contracts),
                'total' => count($contracts),
                'name' => 'contracts'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Find a contract
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function view(Request $request, $contractId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $contract = $this->contracts
                    ->leftJoin('advert_chapter', 'advert_chapter.chapter_id', '=', 'adverts.chapter_id')
                    ->leftJoin('companies', 'companies.company_id', '=', 'adverts.company_id')
                    ->leftJoin('contacts', 'contacts.contact_id', '=', 'adverts.signatory')
                    ->where('adverts.advert_id', '=', $contractId)
                    ->select('adverts.id AS id', 'companies.name AS company', 'companies.billing_address_line_1 AS billing_address_line1', 'companies.billing_address_line_2 AS billing_address_line2', 'companies.billing_address_town_city AS billing_town', 'companies.billing_address_region AS billing_region', 'companies.billing_postal_code AS billing_postal_code', 'adverts.price_offered AS price_offered', 'adverts.regions_included AS regions_included', 'adverts.signed_fee AS signed_fee', 'adverts.signed_fee_deals_in AS signed_fee_deals_in', 'adverts.signed_fee_by_cd AS signed_fee_country', 'adverts.signed_fee_total AS signed_fee_total', 'adverts.invoice_recipient_contact AS invoice_recipient_contact', 'adverts.project_year AS project_year', 'adverts.contract_date AS contract_date', 'adverts.placement AS placement', 'adverts.ad_type AS type', 'adverts.subscriptions_to_site AS subscriptions_to_site', 'adverts.barter_value AS barter_value', 'adverts.barter_details AS barter_details', 'adverts.special_conditions AS special_conditions', 'adverts.size AS size', 'adverts.volume_of_spaces AS spaces', 'adverts.caveats AS caveats', 'adverts.type_of_barter AS barter_type', 'advert_chapter.chapter_name AS chapter', 'adverts.cancellation_reason AS reason', 'adverts.removed AS cancelled', 'contacts.first_name AS first_name', 'contacts.last_name AS last_name', 'contacts.job_title AS job_title', 'contacts.office_phone AS landline', 'contacts.mobile_phone AS mobile', 'contacts.email AS email_address', 'adverts.accounts_contact AS accounts', 'adverts.obg_signatory AS employee', 'adverts.secondary_accounts_contact AS secondary', 'adverts.artwork_contact AS artwork', 'adverts.invoice_recipient_contact AS invoice')
                    ->first();


        if(count($contract) > 0) {
            $type = strtolower($contract->type);
            switch($type) {
                case 'partnership':
                case 'cash/partnership':
                        $partnership = $this->contractsPartnerships->where('advert_id', $contractId)->select('advertpartnerships_value AS partnership_value', 'advertpartnerships_details AS partnership_detail')->first();
                        $contract['partnership_value'] = '';
                        $contract['partnership_detail'] = '';
                        if ($partnership) {
                            $contract['partnership_value'] = $partnership->partnership_value;
                            $contract['partnership_detail'] = $partnership->partnership_detail;
                        }
                        
                    break;
            }

            $artwork = $this->artworksContracts
                    ->leftJoin('artwork', 'artwork.artwork_id', '=', 'artwork_advert.artwork_id')
                    ->leftJoin('adverts', 'adverts.advert_id', '=', 'artwork.advert_id')
                    ->where('adverts.advert_id', $contractId)
                    ->select('artwork_advert.artwork_due_date AS artwork_date', 'artwork_advert.artwork_supplied_by AS artwork_supplier')
                    ->first();

            $contract['artwork_date'] = '';
            $contract['artwork_supplier'] = '';
            if ($artwork) {
                $contract['artwork_date'] = $artwork->artwork_date;
                $contract['artwork_supplier'] = $artwork->artwork_supplier;
            }

            $employeeId = $contract->employee;
            $employee = $this->contacts
                        ->where('contacts.contact_id', $employeeId)
                        ->select('contacts.first_name AS firstname', 'contacts.last_name AS lastname')
                        ->first();
            $contract['employee_firstname'] = $employee->firstname;
            $contract['employee_lastname'] = $employee->lastname;

            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Contracts were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Contracts were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $contract,
            'count' => [
                'result' => count($contract),
                'total' => count($contract),
                'name' => 'contracts'
            ]
        ];

        return response()->json($response, $code);
    }
}
