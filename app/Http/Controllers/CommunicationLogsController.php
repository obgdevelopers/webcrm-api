<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommunicationLogsController extends Controller
{
  
    /**
     * All communication logs for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function companies(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $histories = $this->histories
                ->leftJoin('contacts', 'contacts.contact_id', '=', 'histories.created_by')
                ->where('histories.company_id', $companyId)
                ->where('histories.removed', false)
                ->select('histories.id AS id', 'histories.history_id AS hash', 'histories.project_year AS year', 'histories.date_of_history AS date', 'histories.history_type AS type', 'histories.notes AS notes', 'contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'histories.contact_id AS recipient')
                ->orderBy('histories.project_year', 'DESC')
                ->orderBy('histories.date_of_history', 'DESC')
                ->get();
        // $logs = $this->communicationLogs
        //                     ->leftJoin('tblCommunicationLogsType', 'tblCommunicationLogsType.fldCommunicationLogTypeId', '=', 'tblCommunicationLogs.tblCommunicationLogsType_fldCommunicationLogTypeId')
        //                     ->leftJoin('tblYears', 'tblYears.fldYearId', '=', 'tblCommunicationLogs.tblYears_fldYearId')
        //                     ->leftJoin('tblEmployees', 'tblEmployees.fldEmployeeId', '=', 'tblCommunicationLogs.tblEmployees_fldEmployeeId')
        //                     ->where('tblCommunicationLogs.fldCommunicationLogDeleted', false)
        //                     ->where('tblCommunicationLogs.tblCompanies_fldCompanyId', $companyId)
        //                     ->select('tblCommunicationLogs.fldCommunicationLogId AS id', 'tblYears.fldYearText AS year', 'tblCommunicationLogs.fldCommunicationLogDate AS timestamp', 'tblCommunicationLogsType.fldCommunicationLogTypeName AS type', 'tblCommunicationLogs.fldCommunicationLogNotes AS notes', 'tblEmployees.fldEmployeeFirstname AS caller_firstname', 'tblEmployees.fldEmployeeLastname AS caller_lastname')
        //                     ->get();
        $data = [];
        if(count($histories) > 0) {
            foreach($histories as $history) {
                $recipient = $history['recipient'];
                $contact = $this->contacts->where('contact_id', $recipient)->select('first_name', 'last_name')->first();
                $history['client_firstname'] = null;
                $history['client_lastname'] = null;
                if($contact) {
                    $history['client_firstname'] = $contact['first_name'];
                    $history['client_lastname'] = $contact['last_name'];
                }
                array_push($data, $history);
            }
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Communication logs or histories were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Communication logs or histories were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $histories,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'histories'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * All communication logs for contact
     * @param contactId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function contacts(Request $request, $contactId)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();

        $histories = $this->histories
                ->leftJoin('contacts', 'contacts.contact_id', '=', 'histories.created_by')
                ->where('histories.contact_id', $contactId)
                ->where('histories.removed', false)
                ->select('histories.id AS id', 'histories.history_id AS hash', 'histories.project_year AS year', 'histories.date_of_history AS date', 'histories.history_type AS type', 'histories.notes AS notes', 'contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'histories.contact_id AS recipient')
                ->orderBy('histories.project_year', 'DESC')
                ->orderBy('histories.date_of_history', 'DESC')
                ->get();
        // $logs = $this->communicationLogs
        //                     ->leftJoin('tblCommunicationLogsType', 'tblCommunicationLogsType.fldCommunicationLogTypeId', '=', 'tblCommunicationLogs.tblCommunicationLogsType_fldCommunicationLogTypeId')
        //                     ->leftJoin('tblYears', 'tblYears.fldYearId', '=', 'tblCommunicationLogs.tblYears_fldYearId')
        //                     ->leftJoin('tblEmployees', 'tblEmployees.fldEmployeeId', '=', 'tblCommunicationLogs.tblEmployees_fldEmployeeId')
        //                     ->where('tblCommunicationLogs.fldCommunicationLogDeleted', false)
        //                     ->where('tblCommunicationLogs.tblCompanies_fldCompanyId', $companyId)
        //                     ->select('tblCommunicationLogs.fldCommunicationLogId AS id', 'tblYears.fldYearText AS year', 'tblCommunicationLogs.fldCommunicationLogDate AS timestamp', 'tblCommunicationLogsType.fldCommunicationLogTypeName AS type', 'tblCommunicationLogs.fldCommunicationLogNotes AS notes', 'tblEmployees.fldEmployeeFirstname AS caller_firstname', 'tblEmployees.fldEmployeeLastname AS caller_lastname')
        //                     ->get();
        $data = [];
        if(count($histories) > 0) {
            foreach($histories as $history) {
                $recipient = $history['recipient'];
                $contact = $this->contacts->where('contact_id', $recipient)->select('first_name', 'last_name')->first();
                $history['client_firstname'] = null;
                $history['client_lastname'] = null;
                if($contact) {
                    $history['client_firstname'] = $contact['first_name'];
                    $history['client_lastname'] = $contact['last_name'];
                }
                array_push($data, $history);
            }
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Communication logs or histories were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Communication logs or histories were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $histories,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'histories'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * All histories
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        // $iteration = $input['iteration'];

        $result = $this->histories->buildRaw($projects);
        $histories = $result['table'];

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Histories were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $histories,
            'count' => count($histories)
        ];

        return response()->json($response);
    }

    /**
     * Fetch updates for histories
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $datetime = $input['datetime'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $histories = $this->histories
                    ->join('companies', 'companies.company_id', '=', 'histories.company_id')
                    ->where('histories.updated_at', '>', $datetime)                    
                    ->where('companies.country_id', $country->id)
                    ->select('histories.*')
                    ->distinct('histories.id')
                    ->get();

        if(count($histories) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Histories were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $histories,
            'count' => [
                'result' => count($histories),
                'total' => count($histories),
                'name' => 'histories'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Add logs for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function add(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $parameters = $request->all();
        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $hash = $input['hash'];
        $recipient = $input['recipient'];
        $type = $input['type'];
        $date = $input['date'];
        $time = $input['time'];
        $notes = $input['notes'];
        $user = $input['user'];

        $unix = strtotime(date('Y-m-d H:i:s'));

        $year = $this->projects->whereIn('id', $projects)->select('project_year AS text')->first();

        $histories = $this->histories->create([
            'history_id' => $this->generateHash('histories'),
            'crm_userid' => 1,
            'project_year' => $year['text'],
            'contact_id' => $recipient,
            'company_id' => $companyId,
            'notes' => $notes,
            'history_type' => $type,
            'history_stored_type' => 'contact',
            'date_of_history' => strtotime($date. ' ' .$time),
            'created_by' => $user,
            'created_date' => $unix,
            'last_updated_date' => $unix,
            'last_updated_by' => $user,
            'removed' => 0
        ]);
        $data = [];
        if($histories) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Communication log or history was created successfully!',
                'error' => false
            ];
            $data['id'] = $histories->history_id;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'histories'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Add logs for contact
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function addContact(Request $request, $contactId)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();

        $parameters = $request->all();
        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $companyId = $input['hash'];
        $recipient = $contactId;
        $type = $input['type'];
        $date = $input['date'];
        $time = $input['time'];
        $notes = $input['notes'];
        $user = $input['user'];

        $unix = strtotime(date('Y-m-d H:i:s'));

        $year = $this->projects->whereIn('id', $projects)->select('project_year AS text')->first();

        $histories = $this->histories->create([
            'history_id' => $this->generateHash('histories'),
            'crm_userid' => 1,
            'project_year' => $year['text'],
            'contact_id' => $recipient,
            'company_id' => $companyId,
            'notes' => $notes,
            'history_type' => $type,
            'history_stored_type' => 'contact',
            'date_of_history' => strtotime($date. ' ' .$time),
            'created_by' => $user,
            'created_date' => $unix,
            'last_updated_date' => $unix,
            'last_updated_by' => $user,
            'removed' => 0
        ]);
        $data = [];
        if($histories) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Communication log or history was created successfully!',
                'error' => false
            ];
            $data['id'] = $histories->history_id;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'histories'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * View communication log details
     * @param communicationLogId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function view(Request $request, $historyId)
    {
        $baseUrl = url('/api/histories');
        $selfUrl = url()->full();
        
        // $histories = $this->communicationLogs
        //                     ->leftJoin('tblCommunicationLogsType', 'tblCommunicationLogsType.fldCommunicationLogTypeId', '=', 'tblCommunicationLogs.tblCommunicationLogsType_fldCommunicationLogTypeId')
        //                     ->leftJoin('tblYears', 'tblYears.fldYearId', '=', 'tblCommunicationLogs.tblYears_fldYearId')
        //                     ->leftJoin('tblEmployees', 'tblEmployees.fldEmployeeId', '=', 'tblCommunicationLogs.tblEmployees_fldEmployeeId')
        //                     ->leftJoin('tblContacts', 'tblContacts.fldContactId', '=', 'tblCommunicationLogs.tblContacts_fldContactId')
        //                     ->where('tblCommunicationLogs.fldCommunicationLogDeleted', false)
        //                     ->where('tblCommunicationLogs.fldCommunicationLogId', $communicationLogId)
        //                     ->select('tblCommunicationLogs.fldCommunicationLogId AS id', 'tblYears.fldYearText AS year', 'tblCommunicationLogs.fldCommunicationLogDate AS timestamp', 'tblCommunicationLogsType.fldCommunicationLogTypeName AS type', 'tblContacts.fldContactFirstname AS caller_firstname', 'tblContacts.fldContactLastname AS caller_lastname', 'tblEmployees.fldEmployeeFirstname AS receiver_firstname', 'tblEmployees.fldEmployeeLastname AS receiver_lastname', 'tblCommunicationLogs.fldCommunicationLogNotes AS notes')
        //                     ->get();

        $histories = $this->histories
                ->leftJoin('contacts', 'contacts.contact_id', '=', 'histories.created_by')
                ->where('histories.history_id', $historyId)
                ->where('histories.removed', false)
                ->select('histories.id AS id', 'histories.history_id AS hash', 'histories.project_year AS year', 'histories.date_of_history AS date', 'histories.history_type AS type', 'histories.notes AS notes', 'contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'histories.contact_id AS recipient', 'histories.company_id AS company_hash')
                ->first();

        if($histories) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Communication log or history was created successfully!',
                'error' => false
            ];
            
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $histories,
            'count' => [
                'result' => count($histories),
                'total' => count($histories),
                'name' => 'histories'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Delete log
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function delete(Request $request, $historyId)
    {
        $baseUrl = url('/api/histories');
        $selfUrl = url()->full();

        $input = $request->all();

        $history = $this->histories->where('history_id', $historyId)->update(['removed' => 1]);

        $data = [];
        if($history) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'History was deleted successfully!',
                'error' => false
            ];
            $data = $history;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'History was not deleted!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'histories'
            ]
        ];

        return response()->json($response, $code);

    }

    /**
     * Update log
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function update(Request $request, $historyId)
    {
        $baseUrl = url('/api/histories');
        $selfUrl = url()->full();

        $input = $request->all();

        $companyId = $input['hash'];
        $recipient = $input['recipient'];
        $type = $input['type'];
        $date = $input['date'];
        $time = $input['time'];
        $notes = $input['notes'];
        $user = $input['user'];

        $unix = strtotime(date('Y-m-d H:i:s'));

        $history = $this->histories
            ->where('history_id', $historyId)
            ->update([
            'contact_id' => $recipient,
            'notes' => $notes,
            'history_type' => $type,
            'date_of_history' => strtotime($date. ' ' .$time),
            'last_updated_date' => $unix,
            'last_updated_by' => $user
        ]);

        $data = [];
        if($history) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'History was updated successfully!',
                'error' => false
            ];
            $data = $history;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'History was not updated!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'histories'
            ]
        ];

        return response()->json($response, $code);

    }
  
}