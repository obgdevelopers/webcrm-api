<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MeetingsController extends Controller
{
    /**
     * List of agendas
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function agendas(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $sessions = $headers['session-project'][0];
        $sessions = explode(',', $sessions);

        $files = [];

        $meetings = $this->meetings->findProjectOutlookFiles($companyId, $sessions);

        $outlook = [];
        if($meetings) {
            $files = $this->meetingsAttendances->findAttendees($meetings);

            foreach($files as $file) {
                $delta = $file['delta'];
                $title = $this->findDelta($delta);
                $file['title'] = $title;
                $outlook[] = $file;
            }
        }

        if(count($files) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Meetings were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Meetings were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $outlook,
            'count' => [
                'result' => count($outlook),
                'total' => count($outlook),
                'name' => 'files'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * All meetings
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        
        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $meetings = $this->meetings
                    ->join('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('companies.country_id', $country->id)
                    ->where('meeting_notes.removed', false)
                    ->where('companies.removed', false)
                    ->select('meeting_notes.id', 
                            'meeting_notes.meeting_note_id',
                            'meeting_notes.crm_userid',
                            'meeting_notes.delta',
                            'meeting_notes.created_by',
                            'meeting_notes.company_id',
                            'meeting_notes.meeting_date',
                            'meeting_notes.created_date',
                            'meeting_notes.last_updated_by',
                            'meeting_notes.last_updated_date',
                            'meeting_notes.contract_signed',
                            'meeting_notes.signed_on_the_spot',
                            'meeting_notes.priority',
                            'meeting_notes.project_year',
                            'meeting_notes.removed',
                            'meeting_notes.created_at',
                            'meeting_notes.updated_at',
                            'meeting_notes.completed',
                            'meeting_notes.status',
                            'meeting_notes.reason',
                            'meeting_notes.research_status',
                            'meeting_notes.parent_meeting_note_id',
                            'meeting_notes.email_uid',
                            'meeting_notes.email_sequence',
                            'meeting_notes.driver_email',
                            'meeting_notes.driver_invite',
                            'meeting_notes.company_attendees_invite',
                            'meeting_notes.company_attendees_subject_invite',
                            'meeting_notes.meeting_location',
                            'meeting_notes.timezone',
                            'meeting_notes.scheduled_by'
                        )
                    ->distinct('meeting_notes.id')
                    ->get();


        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Meetings were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $meetings,
            'count' => count($meetings)
        ];

        return response()->json($response);
    }

    /**
     * Fetch updates for meetings
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/updates');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();

        $datetime = $input['datetime'];
        
        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();
                    
        $meetings = $this->meetings
                    ->join('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('companies.country_id', $country->id)
                    ->where('meeting_notes.updated_at', '>', $datetime)
                    ->select('meeting_notes.*')
                    ->distinct('meeting_notes.id')
                    ->get();


        if(count($meetings) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Meetings were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $meetings,
            'count' => [
                'result' => count($meetings),
                'total' => count($meetings),
                'name' => 'meetings'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Details of agenda
     * @param meetingId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function view(Request $request, $meetingId)
    {
        $baseUrl = url('/api/meetings');
        $selfUrl = url()->full();

        $files = [];

        $meetings = $this->meetings
                    ->leftJoin('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->leftJoin('company_advisory_potential', 'company_advisory_potential.company_id', '=', 'companies.company_id')
                    ->leftJoin('contacts', 'contacts.contact_id', '=', 'meeting_notes.scheduled_by')
                    ->where('meeting_notes.meeting_note_id', $meetingId)
                    ->select('meeting_notes.id AS id', 'meeting_notes.meeting_note_id AS hash','meeting_notes.delta AS delta',  'meeting_notes.status AS status', 'meeting_notes.meeting_date AS date', 'meeting_notes.project_year AS year','meeting_notes.driver_email AS driver', 'meeting_notes.driver_invite', 'meeting_notes.company_attendees_invite', 'meeting_notes.company_attendees_subject_invite', 'meeting_notes.meeting_location', 'meeting_notes.scheduled_by', 'meeting_notes.timezone', 'companies.name AS name', 'companies.company_id AS company_hash', 'companies.office_phone AS landline', 'companies.office_fax AS fax', 'companies.office_address_line_1 AS address_line1', 'companies.office_address_line_2 AS address_line2', 'companies.office_address_town_city AS town', 'companies.office_address_region AS region', 'companies.office_postal_code AS postal_code', 'companies.office_directions AS directions', 'companies.advisory_potential AS potential', 'company_advisory_potential.advisory_potential AS potential_reason','contacts.first_name AS scheduledByFirstname', 'contacts.last_name AS scheduledByLastname')
                    ->get()
                    ->toArray();

        $outlook = [];
        if($meetings) {
            $files = $this->meetingsAttendances->findAttendees($meetings);

            foreach($files as $file) {
                $delta = $file['delta'];
                $title = $this->findDelta($delta);
                $file['title'] = $title;
                $outlook[] = $file;
            }
        }

        if(count($files) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Meetings were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Meetings were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $outlook[0],
            'count' => [
                'result' => count($outlook),
                'total' => count($outlook),
                'name' => 'files'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * List of agendas of user
     * @param employeeId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function user(Request $request, $employeeId)
    {
        $baseUrl = url('/api/employees');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $sessions = $headers['session-project'][0];
        $sessions = explode(',', $sessions);

        $request->validate([
            'type' => 'required|string'
        ]);

        $parameters = $request->all();
        $type = $parameters['type'];

        switch($type) {
            case 'count':
                $request->validate([
                    'status' => 'required|string'
                ]);
                $status = $parameters['status'];

                $employee = $this->contacts->where('contact_id', $employeeId)->select('job_title AS job_title')->first();
                $title = strtolower($employee->job_title);
                $delta = $this->generateDeltas($title);

                $user = $this->users
                        ->leftJoin('departments_tbl', 'departments_tbl.departments_id', '=', 'sf_user.sf_user_departmentid')
                        ->where('sf_user.contact_id', $employeeId)
                        ->select('departments_tbl.departments_title AS department')
                        ->first();

                $department = strtolower($user->department);

                $meetings = $this->meetings->countUserMeetingsByStatus($delta, $title, $status, $employeeId, $department, $sessions);

                if(count($meetings) > 0) {
                    $code = 200;
                    $status = [
                        'type' => 'success',
                        'code' => 200,
                        'message' => 'Meetings were found!',
                        'error' => false
                    ];
                } else {
                    $code = 404;
                    $status = [
                        'type' => 'success',
                        'code' => 404,
                        'message' => 'Meetings were not found!',
                        'error' => false
                    ];
                }
                break;
            case 'table':
                $meetings = [];
                $user = $this->users
                ->leftJoin('departments_tbl', 'departments_tbl.departments_id', '=', 'sf_user.sf_user_departmentid')
                ->where('sf_user.contact_id', $employeeId)
                ->select('departments_tbl.departments_title AS department')
                ->first();

                $department = strtolower($user->department);
                $employee = $this->contacts->where('contact_id', $employeeId)->select('job_title AS job_title')->first();
                $jobTitle = strtolower($employee->job_title);
                $delta = $this->generateDeltas($jobTitle);

                $files = $this->meetings->findUserMeetings($department, $delta, $jobTitle, $parameters, $sessions, $employeeId);
                $files = $this->meetingsAttendances->findAttendees($files);

                foreach($files as $file) {
                    $delta = $file['delta'];
                    $title = $this->findDelta($delta);
                    $file['title'] = $title;
                    $meetings[] = $file;
                }

                $code = 200;
                $status = [
                    'type' => 'success',
                    'code' => 200,
                    'message' => 'Meetings were found!',
                    'error' => false
                ];
                
                break;
            default:
                break;
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $meetings,
            'count' => [
                'result' => count($meetings),
                'total' => count($meetings),
                'name' => 'meetings'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * List of meetings
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function calendar(Request $request)
    {
        $baseUrl = url('/api/employees');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $sessions = $headers['session-project'][0];
        $sessions = explode(',', $sessions);

        $input = $request->all();
        $start = $input['start'];
        $end = $input['end'];
        $meetings = $this->meetings->findMeetings($sessions, $start, $end);
        
        if(count($meetings) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Meetings were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Meetings were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $meetings,
            'count' => [
                'result' => count($meetings),
                'total' => count($meetings),
                'name' => 'meetings'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Outlook files of meetings
     * @param meetingId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function files(Request $request, $meetingId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);
        
        $actionToBeTaken = $this->projects->whereIn('id', $projects)
                    ->select('action_to_be_taken AS action')
                    ->first();
        // if($meeting) {
        //     $delta = $meeting['delta'];
        //     if ($delta == 6) {
        //         $children = $this->meetings->where('parent_meeting_note_id', $meetingId)->get();
        //         foreach ($children as $child) {
        //             $contents = $this->meetingsNotesContents->where('meeting_note_id', $child->meeting_note_id)->where('removed', false)->get();
        //             foreach ($contents as $content) {
        //                 $subject = $content->content;
        //                 if($subject != '' && $subject != null) {
        //                     $empty = false;
        //                     break;
        //                 }
        //             }

        //             if (!$empty) {
        //                 break;
        //             }
        //         }
        //     } else {
                
        //     }
        // }

        $contents = $this->meetingsNotesContents
                    ->join('meeting_notes', 'meeting_notes.meeting_note_id', '=', 'meeting_note_content.meeting_note_id')
                    ->where('meeting_note_content.meeting_note_id', $meetingId)
                    ->where('meeting_note_content.removed', false)
                    ->select('meeting_note_content.mn_content_id AS hash', 'meeting_note_content.heading_id AS heading', 'meeting_note_content.heading_text AS title', 'meeting_note_content.content AS content', 'meeting_notes.company_id', 'meeting_notes.project_year')
                    ->get();
     
        foreach($contents as &$content) {
            if(isset($actionToBeTaken) && $actionToBeTaken->action==1 && strpos($content->title, 'Action to be taken next year') !== false) {
                $content->title = 'Action to be taken next year';
                $actionNote = $this->companiesActionNextYear
                    ->where('company_id', $content->company_id)
                    ->where('project_year', $content->project_year)
                    ->select('action', 'note')
                    ->first();
                if($actionNote) {
                    //$note = $actionNote->action == 1 ? 'Go back' : "Don't go back";
                    //$note = $actionNote->note;
                    $content->content = $actionNote->note;
                    $content->action = $actionNote->action;
                }
                $content->action_to_be_taken = 1;
            }
        }

        if(count($contents) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Files were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Files were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $contents,
            'count' => [
                'result' => count($contents),
                'total' => count($contents),
                'name' => 'meetings'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Check if meetings is empty
     * @param meetingId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function check(Request $request, $meetingId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $meeting = $this->meetings->where('meeting_note_id', $meetingId)->first();

        $empty = true;

        if($meeting) {
            $delta = $meeting['delta'];
            if ($delta == 6) {
                $children = $this->meetings->where('parent_meeting_note_id', $meetingId)->get();
                foreach ($children as $child) {
                    $contents = $this->meetingsNotesContents->where('meeting_note_id', $child->meeting_note_id)->where('removed', false)->get();
                    foreach ($contents as $content) {
                        $subject = $content->content;
                        if($subject != '' && $subject != null) {
                            $empty = false;
                            break;
                        }
                    }

                    if (!$empty) {
                        break;
                    }
                }
            } else {
                $contents = $this->meetingsNotesContents->where('meeting_note_id', $meetingId)->where('removed', false)->get();
                foreach ($contents as $content) {
                    $subject = $content->content;
                    if($subject != '' && $subject != null) {
                        $empty = false;
                        break;
                    }
                }
            }
        }

        if(!$empty) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Meeting is not empty!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Meetings is empty!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $empty,
            'count' => [
                'result' => 1,
                'total' => 1,
                'name' => 'files'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * List of meetings with its contents
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function list(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();
        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);
        
        $actionToBeTaken = $this->projects->whereIn('id', $projects)
                    ->select('action_to_be_taken AS action')
                    ->first();

        $files = [];

        $meetings = $this->meetings->findOutlookFiles($companyId);

        $outlook = [];
        if($meetings) {
            $files = $this->meetingsNotesContents->findContents($meetings);
            $files = $this->meetingsAttendances->findAttendees($files);

            foreach($files as $file) {
                $delta = $file['delta'];
                $title = $this->findDelta($delta);
                $file['title'] = $title;
                foreach($file['files'] as &$content) {
                    if(isset($actionToBeTaken) && $actionToBeTaken->action==1 && strpos($content['heading'], 'Action to be taken next year') !== false) {
                        $content['heading'] = 'Action to be taken next year';
                        $actionNote = $this->companiesActionNextYear
                            ->where('company_id', $content['company_id'])
                            ->where('project_year', $content['project_year'])
                            ->select('action', 'note')
                            ->first();
                        if($actionNote) {
                            $content['content'] = $actionNote->note;
                            $content['action'] = $actionNote->action;
                        }
                        $content['action_to_be_taken'] = 1;
                    }
                }
                $outlook[] = $file;
            }
        }

        if(count($files) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Meetings were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Meetings were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $outlook,
            'count' => [
                'result' => count($outlook),
                'total' => count($outlook),
                'name' => 'files'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Add meetings for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function add(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $parameters = $request->all();
        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $type = $input['type'];
        $date = $input['date'];
        $time = $input['time'];
        $user = $input['user'];
        $assignees = $input['assignee'];
        $assigneeInvite = isset($input['assigneeInvite']) ? $input['assigneeInvite'] : [];
        $contacts = $input['contact'];
        $contactInvite = isset($input['contactInvite']) ? $input['contactInvite'] : [];
        $contactEmailSubject = isset($input['contactEmailSubject']) ? $input['contactEmailSubject'] : '';
        $driver = isset($input['driver']) ? $input['driver'] : '';
        $driverInvite = isset($input['driverInvite']) ? $input['driverInvite'] : '';
        $meetingLocation = isset($input['meetingLocation']) ? $input['meetingLocation'] : '';
        $scheduledBy = isset($input['scheduledBy']) ? $input['scheduledBy'] : '';
        $timezone = isset($input['timezone']) ? $input['timezone'] : '';

        $unix = strtotime(date('Y-m-d H:i:s'));

        $year = $this->projects->whereIn('id', $projects)->select('project_year AS text')->first();

        $company = $this->companies->where('company_id', $companyId)->select('name')->first();
        
        $country = $this->projects
                    ->leftJoin('countries', 'countries.id', '=', 'project.country_id')
                    ->whereIn('project.id', $projects)
                    ->select('countries.timezone AS timezone')
                    ->first();
        
        $meetingData = [
            'meeting_note_id' => $this->generateHash('meeting_notes'),
            'crm_user_id' => 1,
            'delta' => $type,
            'created_by' => $user,
            'company_id' => $companyId,
            'meeting_date' => strtotime($date.' '.$time),
            'created_date' => $unix,
            'last_updated_by' => $user,
            'last_updated_date' => $unix,
            'contract_signed' => false,
            'signed_on_the_spot' => false,
            'project_year' => $year->text,
            'removed' => false,
            'completed' => false,
            'status' => 'pending',
            'email_uid' => md5(uniqid(mt_rand(), true))."@oxfordbusinessgroup.com",
            'email_sequence' => 0,
            'scheduled_by' => $scheduledBy,
            'timezone' => $timezone
        ];
        if($driver)
            $meetingData['driver_email'] = $driver;
        if($driverInvite)
            $meetingData['driver_invite'] = $driverInvite;
        if($contactInvite)
            $meetingData['company_attendees_invite'] = $contactInvite;
        if($contactEmailSubject)
            $meetingData['company_attendees_subject_invite'] = $contactEmailSubject;
        if($meetingLocation)
            $meetingData['meeting_location'] = $meetingLocation;

        $meeting = $this->meetings->create($meetingData);

        $uid = $meeting->email_uid;

        if ($uid) {
            $meetingId = $meeting->meeting_note_id;
            // $filesystem = new FileSystem();
            $date = $meeting->meeting_date;
            $sequence = $meeting->email_sequence;
            
            if($assigneeInvite) {
                $employees = $this->contacts
                ->leftJoin('sf_user', 'sf_user.contact_id', '=', 'contacts.contact_id')
                ->whereIn('contacts.contact_id', $assigneeInvite)
                ->select('contacts.email AS email', 'contacts.job_title', 'sf_user.agenda_email')->get()->toArray();
            }

            $emails = [];

            if(isset($employees) && $employees) {
                foreach ($employees as $employee) {
                    if($employee['job_title']=='Project Coordinator') 
                        array_push($emails, $employee['agenda_email']);
                    else
                        array_push($emails, $employee['email']);
                }
            }

            /*$timezone = 'Asia/Manila';*/
            if (!$timezone) {
                $timezone = $country->timezone;
            }

            if($emails) {
                $this->generateInvitation('create', $uid, $company->name, $date, $sequence, $emails, $meeting->delta, $timezone, '', $meetingLocation);
            }

            if($driverInvite) {
                $this->generateInvitation('create', $uid, $company->name, $date, $sequence, [$driver], $meeting->delta, $timezone, '', $meetingLocation);
            }

            if($contactInvite) {
                $compContacts = $this->contacts->whereIn('contact_id', $contacts)->select('contacts.email AS email')->get()->toArray();
                
                $contactEmails = [];
                foreach ($compContacts as $contact) {
                    array_push($contactEmails, $contact['email']);
                }

                if($contactEmails) {
                    $this->generateInvitation('create', $uid, $company->name, $date, $sequence, $contactEmails, $meeting->delta, $timezone, $contactEmailSubject, $meetingLocation);
                }
            }
        }

        if ($type == 6) { /* 1st & 2nd pointer */
            $this->fill($assignees, $meeting->meeting_note_id, $contacts, $type, $assigneeInvite);
            $this->fillWithParent($input, $meeting->meeting_note_id, 1, $companyId, $year); /* 1st pointer */
            $this->fillWithParent($input, $meeting->meeting_note_id, 2, $companyId, $year); /* 2nd pointer */
        } else {
            $this->fill($assignees, $meeting->meeting_note_id, $contacts, $type, $assigneeInvite);
        }
        
        if($meeting) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Meeting was created successfully!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $meeting,
            'count' => [
                'result' => count($meeting),
                'total' => count($meeting),
                'name' => 'meetings'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Update meetings
     * @param meetingId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function update(Request $request, $meetingId)
    {
        $baseUrl = url('/api/meetings');
        $selfUrl = url()->full();

        $parameters = $request->all();
        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $year = $this->projects->whereIn('id', $projects)->select('project_year AS text')->first();

        $input = $request->all();
        $type = $input['type'];
        $date = $input['date'];
        $time = $input['time'];
        $user = $input['user'];
        $assignees = $input['assignee'];
        $assigneeInvite = isset($input['assigneeInvite']) ? $input['assigneeInvite'] : '';
        $contacts = $input['contact'];
        $contactInvite = isset($input['contactInvite']) ? $input['contactInvite'] : '';
        $contactEmailSubject = isset($input['contactEmailSubject']) ? $input['contactEmailSubject'] : '';
        $driver = isset($input['driver']) ? $input['driver'] : '';
        $driverInvite = isset($input['driverInvite']) ? $input['driverInvite'] : '';
        $meetingLocation = isset($input['meetingLocation']) ? $input['meetingLocation'] : '';
        $scheduledBy = isset($input['scheduledBy']) ? $input['scheduledBy'] : 'null';
        $timezone = isset($input['timezone']) ? $input['timezone'] : 'null';

        $unix = strtotime(date('Y-m-d H:i:s'));

        $exist = $this->meetings->where('meeting_note_id', $meetingId)->where('removed', false)->where('delta', $type)->count();

        $company = $this->meetings
                    ->leftJoin('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('meeting_note_id', $meetingId)
                    ->select('companies.company_id AS company', 'companies.name AS name')
                    ->first();

        $country = $this->projects
                    ->leftJoin('countries', 'countries.id', '=', 'project.country_id')
                    ->whereIn('project.id', $projects)
                    ->select('countries.timezone AS timezone')
                    ->first();

        $detail = $this->meetings
                    ->where('meeting_note_id', $meetingId)
                    ->first();

        $uid = $detail->email_uid;

        if ($uid) {
            // $filesystem = new FileSystem();
            $filename = 'invite.ics';
            $newDate = strtotime($date);
            $sequence = $detail->email_sequence;
            
            if($assigneeInvite) {
                $employees = $this->contacts
                ->leftJoin('sf_user', 'sf_user.contact_id', '=', 'contacts.contact_id')
                ->whereIn('contacts.contact_id', $assigneeInvite)
                ->select('contacts.email AS email', 'contacts.job_title', 'sf_user.agenda_email')->get()->toArray();
            }
            
            $compContacts = $this->contacts->whereIn('contact_id', $contacts)->select('contacts.email AS email')->get()->toArray();

            $meetingData = [
                'delta' => $type,
                'meeting_date' => strtotime($date.' '.$time),
                'last_updated_by' => $user,
                'last_updated_date' => $unix,
                'email_sequence' => ($sequence + 1),
                'scheduled_by' => $scheduledBy,
                'timezone' => $timezone
            ];
            
            if($driver)
                $meetingData['driver_email'] = $driver;
            if($driverInvite)
                $meetingData['driver_invite'] = $driverInvite;
            if($contactInvite)
                $meetingData['company_attendees_invite'] = $contactInvite;
            if($contactEmailSubject)
                $meetingData['company_attendees_subject_invite'] = $contactEmailSubject;
            if($meetingLocation)
                $meetingData['meeting_location'] = $meetingLocation;

            $meeting = $this->meetings
                    ->where('meeting_note_id', $meetingId)
                    ->update($meetingData);

            $emails = [];
            if(isset($employees) && $employees) {
                foreach ($employees as $employee) {
                    if($employee['job_title']=='Project Coordinator') 
                        array_push($emails, $employee['agenda_email']);
                    else
                        array_push($emails, $employee['email']);
                }
            }

            //$timezone = 'Asia/Manila';
            if (!$timezone) {
                $timezone = $country->timezone;
            }

            if($emails) {
                $this->generateInvitation('update', $uid, $company->name, $newDate, $sequence, $emails, $detail->delta, $timezone, '', $meetingLocation);
            }

            if($driverInvite) {
                $this->generateInvitation('update', $uid, $company->name, $newDate, $sequence, [$driver], $detail->delta, $timezone, '', $meetingLocation);
            }

            if($contactInvite) {
                $contactEmails = [];
                foreach ($compContacts as $contact) {
                    array_push($contactEmails, $contact['email']);
                }

                if($contactEmails) {
                    if($driverInvite)
                    array_push($contactEmails, $driver);
                    $this->generateInvitation('update', $uid, $company->name, $newDate, $sequence, $contactEmails, $detail->delta, $timezone, $contactEmailSubject, $meetingLocation);
                }
            }
        } else {
            $meeting = $this->meetings
                    ->where('meeting_note_id', $meetingId)
                    ->update([
                        'delta' => $type,
                        'meeting_date' => strtotime($date.' '.$time),
                        'last_updated_by' => $user,
                        'last_updated_date' => $unix,
                        'driver_email' => $driver,
                        'driver_invite' => $driverInvite,
                        'company_attendees_invite' => $contactInvite,
                        'company_attendees_subject_invite' => $contactEmailSubject,
                        'meeting_location' => $meetingLocation,
                        'scheduled_by' => $scheduledBy
                    ]);
        }


        $this->meetingsAttendances
            ->whereNotIn('contact_id', $assignees)
            ->where('is_obg_employee', true)
            ->where('meeting_note_id', $meetingId)
            ->where('removed', false)
            ->update(['removed' => true]);

        if ($assigneeInvite) {
            $this->meetingsAttendances
            ->where('contact_id', $assigneeInvite)
            ->where('is_obg_employee', true)
            ->where('meeting_note_id', $meetingId)
            ->where('removed', false)
            ->update(['send_invite' => true]);
        }

        $this->meetingsAttendances
            ->whereNotIn('contact_id', $contacts)
            ->where('meeting_note_id', $meetingId)
            ->where('is_obg_employee', false)
            ->where('removed', false)
            ->update(['removed' => true]);

        if($exist == 0) {
            $this->meetingsNotesContents
                ->where('meeting_note_id', $meetingId)
                ->where('removed', false)
                ->update(['removed' => true]);

            $children = $this->meetings->where('parent_meeting_note_id', $meetingId)->select('meeting_note_id AS hash')->get();
            
            foreach ($children as $child) {
                $childId = $child->hash;
                $this->meetingsNotesContents
                ->where('meeting_note_id', $childId)
                ->where('removed', false)
                ->update(['removed' => true]);
            }
            $children = $this->meetings->where('parent_meeting_note_id', $meetingId)->update(['removed' => true]);

        }

        if ($type == 6) { /* 1st & 2nd pointer */
            $this->fill($assignees, $meetingId, $contacts, $type);
            $this->fillWithParent($input, $meetingId, 1, $company->company, $year); /* 1st pointer */
            $this->fillWithParent($input, $meetingId, 2, $company->company, $year); /* 2nd pointer */
        } else {
            $this->fill($assignees, $meetingId, $contacts, $type);
        }
        
        if($meeting) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Meeting was updated successfully!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $meeting,
            'count' => [
                'result' => count($meeting),
                'total' => count($meeting),
                'name' => 'meetings'
            ]
        ];

        return response()->json($response, $code);
    }


    /**
     * Cancel meetings for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function cancel(Request $request, $meetingId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $input = $request->all();
        $user = $input['user'];

        $meeting = $this->meetings
                    ->leftJoin('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('meeting_note_id', $meetingId)
                    ->select('meeting_notes.*', 'companies.name AS company_name')
                    ->first();
        $unix = strtotime(date('Y-m-d H:i:s'));

        if($meeting) {
            $uid = $meeting->email_uid;

            if ($uid) {
                // $filesystem = new FileSystem();
                $filename = 'invite.ics';
                $date = $meeting->meeting_date;
                $sequence = $meeting->email_sequence;
                $attendees = $this->meetingsAttendances->findAttendees([['hash' => $meetingId]]);
                
                $employees = $attendees[0]['employees'];

                $emails = [];

                foreach ($employees as $employee) {
                    if($employee['contact_invite'])
                    array_push($emails, $employee['email']);
                }

                if($emails)
                $this->generateInvitation('delete', $uid, $meeting->company_name, $date, $sequence, $emails, $meeting->delta, null);
            }

            $delta = $meeting->delta;
            if ($delta == 6) {
                $this->meetings->where('meeting_note_id', $meetingId)
                    ->update([
                        'last_updated_by' => $user,
                        'last_updated_date' => $unix,
                        'removed' => true
                    ]);
                    
                $children = $this->meetings->where('parent_meeting_note_id', $meetingId)
                            ->update([
                                'last_updated_by' => $user,
                                'last_updated_date' => $unix,
                                'removed' => true
                            ]);
            } else {
                $this->meetings->where('meeting_note_id', $meetingId)
                    ->update([
                        'last_updated_by' => $user,
                        'last_updated_date' => $unix,
                        'removed' => true
                    ]);
            }
        }
        
        if($meeting) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Meeting was deleted successfully!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $meeting,
            'count' => [
                'result' => count($meeting),
                'total' => count($meeting),
                'name' => 'meetings'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Reschedule meeting
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function reschedule(Request $request, $meetingId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $parameters = $request->all();
        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $date = $input['date'];
        $user = $input['user'];

        $unix = strtotime(date('Y-m-d H:i:s'));

        $meeting = $this->meetings
                    ->leftJoin('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('meeting_note_id', $meetingId)
                    ->select('meeting_notes.*', 'companies.name AS company_name')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $uid = $meeting->email_uid;

        if ($uid) {
            // $filesystem = new FileSystem();
            $filename = 'invite.ics';
            $newDate = strtotime($date);
            $sequence = $meeting->email_sequence;
            $attendees = $this->meetingsAttendances->findAttendees([['hash' => $meetingId]]);
                
            $employees = $attendees[0]['employees'];

            $this->meetings
            ->where('meeting_note_id', $meetingId)
            ->orWhere('parent_meeting_note_id', $meetingId)
            ->update([
                'meeting_date' => strtotime($date),
                'last_updated_by' => $user,
                'last_updated_date' => $unix,
                'email_sequence' => ($sequence + 1)
            ]);

            $emails = [];

            foreach ($employees as $employee) {
                array_push($emails, $employee['email']);
            }
            $timezone = 'Asia/Manila';
            if ($country->timezone) {
                $timezone = $country->timezone;
            }

            $this->generateInvitation('update', $uid, $meeting->company_name, $newDate, $sequence, $emails, $meeting->delta, $timezone);            
        } else {
            $meeting = $this->meetings
                    ->where('meeting_note_id', $meetingId)
                    ->orWhere('parent_meeting_note_id', $meetingId)
                    ->update([
                        'meeting_date' => strtotime($date),
                        'last_updated_by' => $user,
                        'last_updated_date' => $unix
                    ]);
        }
        
        $data = [];
        if($meeting) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Meeting was updated successfully!',
                'error' => false
            ];
            $data['id'] = $meeting;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'meetings'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Return delta title
     * @param delta
     * 
     * @return [string] title
     */
    public function findDelta($delta) 
    {
        $deltas = [0 => 'PR Meeting', 1 => '1st Pointer', 2 => '2nd Pointer', 3 => '3rd Pointer', 4 => 'Barter Meeting', 5 => 'Follow Up Meeting', 6 => '1st and 2nd Pointer', 8 => 'Analyst Meeting'];
        $title = $deltas[$delta];
        return $title;
    }

    /**
     * Create details for the meeting
     * @param [array] assignees
     * @param [object] meeting
     * @param [array] contacts
     * @param [int] type
     * 
     * @return null
     */
    public function fill($assignees, $meetingId, $contacts, $type, $assigneeInvite = []) 
    {
        foreach ($assignees as $assignee) {
            $sendInvite =  in_array($assignee, $assigneeInvite) ? true : false;
            $attendance = $this->meetingsAttendances->firstOrCreate([
                'contact_id' => $assignee,
                'meeting_note_id' => $meetingId,
                'is_obg_employee' => true,
                'removed' => false,
                'send_invite' => $sendInvite],
                ['attendance_id' => $this->generateHash('meeting_note_attendance')]
            );
        }

        foreach ($contacts as $contact) {
            $attendance = $this->meetingsAttendances->firstOrCreate([
                'contact_id' => $contact,
                'meeting_note_id' => $meetingId,
                'is_obg_employee' => false,
                'removed' => false,
                'send_invite' => false],
            ['attendance_id' => $this->generateHash('meeting_note_attendance')]);
        }

        $headings = $this->meetingsNotesHeadings
                    ->where('meeting_delta', $type)
                    ->where('removed', false)
                    ->where('active', true)
                    ->get();
        
        foreach ($headings as $heading) {
            $content = $this->meetingsNotesContents->firstOrCreate([
                'meeting_note_id' => $meetingId,
                'heading_id' => $heading->id,
                'heading_text' => $heading->heading_name,
                'removed' => false,
            ],
            ['mn_content_id' => $this->generateHash('meeting_note_content'), 'content' => '']);
        }
    }

    /**
     * Create details for the meeting with parent
     * @param [array] parameters
     * @param [object] parent
     * @param [int] type
     * @param [string] companyId
     * @param [object] year
     * 
     * @return null
     */
    public function fillWithParent($parameters, $parentId, $type, $companyId, $year) 
    {
        $date = $parameters['date'];
        $time = $parameters['time'];
        $user = $parameters['user'];
        $assignees = $parameters['assignee'];
        $contacts = $parameters['contact'];
        $unix = strtotime(date('Y-m-d H:i:s'));

        $meeting = $this->meetings->firstOrCreate([
            'delta' => $type,
            'company_id' => $companyId,
            'removed' => false,
            'parent_meeting_note_id' => $parentId
        ],
        [
            'meeting_note_id' => $this->generateHash('meeting_notes'),
            'crm_user_id' => 1,
            'created_by' => $user,
            'meeting_date' => strtotime($date.' '.$time),
            'created_date' => $unix,
            'last_updated_by' => $user,
            'last_updated_date' => $unix,
            'contract_signed' => false,
            'signed_on_the_spot' => false,
            'project_year' => $year->text,
            'completed' => false,
            'status' => 'pending',
        ]);

        $this->meetings
            ->where('meeting_note_id', $meeting->meeting_note_id)
            ->update([
                'meeting_date' => strtotime($date.' '.$time),
                'last_updated_by' => $user,
                'last_updated_date' => $unix,
            ]);

        $meetingId = $meeting->meeting_note_id;

        foreach ($assignees as $assignee) {
            $attendance = $this->meetingsAttendances->firstOrCreate([
                'contact_id' => $assignee,
                'meeting_note_id' => $meetingId,
                'is_obg_employee' => true,
                'removed' => false],
                ['attendance_id' => $this->generateHash('meeting_note_attendance')]
            );
        }

        foreach ($contacts as $contact) {
            $attendance = $this->meetingsAttendances->firstOrCreate([
                'contact_id' => $contact,
                'meeting_note_id' => $meetingId,
                'is_obg_employee' => false,
                'removed' => false],
            ['attendance_id' => $this->generateHash('meeting_note_attendance')]);
        }

        $headings = $this->meetingsNotesHeadings
                    ->where('meeting_delta', $type)
                    ->where('removed', false)
                    ->where('active', true)
                    ->get();
        
        foreach ($headings as $heading) {
            $content = $this->meetingsNotesContents->firstOrCreate([
                'meeting_note_id' => $meetingId,
                'heading_id' => $heading->id,
                'heading_text' => $heading->heading_name,
                'removed' => false,
            ],
            ['mn_content_id' => $this->generateHash('meeting_note_content'), 'content' => '']);
        }
    }

    /**
     * Generate array of delta for role
     * @param [string] title
     * 
     * @return delta
     */
    function generateDeltas($title) {
        $delta = [];
        switch(strtolower($title)) {
            case 'country director':
                $delta = [0,1,3,4,5];
                break;
            case 'project manager':
                $delta = [0,1,3,4,5];
                break;
            case 'editorial manager':
                $delta = [0,2,5];
                break;
            case 'editorial manager trainee':
                $delta = [0,2,5];
                break;
            default:
                $delta = [0,1,2,3,4,5];
                break;
        }

        return $delta;
    }
}