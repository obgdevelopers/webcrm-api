<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeesController extends Controller
{
    /**
     * List of employees with access to project
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function options(Request $request)
    {
        $baseUrl = url('/api/employees');
        $selfUrl = url()->full();

        $parameters = $request->all();
        $headers = $request->headers->all();
        $sessions = $headers['session-project'][0];
        $sessions = explode(',', $sessions);

        $accesses = [];
        foreach($sessions as $session) {
            $push = [];
            $projects = $this->projects
            ->where('id', $session)
            ->select('project_year_id AS year_id', 'country_id AS country_id', 'project_year AS year')
            ->first();
            $push['year_id'] = $projects['year_id'];
            $push['country_id'] = $projects['country_id'];
            $push['year'] = $projects['year'];
            array_push($accesses, $push);
        }

        if (isset($parameters['user'])) {
            $employee = $this->contacts->where('contact_id', $parameters['user'])->select('job_title AS job_title')->first();
            $title = strtolower($employee->job_title);
            $employees = $this->contacts->findByProjects($accesses, $title);
        } else {
            $employees = $this->contacts->findByProjects($accesses, null);
        }

        if(count($employees) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Employees were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Employees were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $employees,
            'count' => [
                'result' => count($employees),
                'total' => count($employees),
                'name' => 'employees'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Verify set-up request and return projects
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function verify(Request $request)
    {
        $baseUrl = url('/api/employees');
        $selfUrl = url()->full();

        $input = $request->all();

        $username = isset($input['username']) ? $input['username'] : null;
        $password = isset($input['password']) ? $input['password'] : null;
        $hash = isset($input['hash']) ? $input['hash'] : null;

        if ($hash) {
            $contactId = $hash;
            $contact = $this->users->where('contact_id', $contactId)->first();
            if (!$contact) {
                return response()->json([
                    'message' => 'Credentials were unauthorized.'
                ], 401);
            }
            $firstname = $contact->first_name;
            $lastname = $contact->last_name;
        } else {
            $credentials = ['email' => $username, 'password' => $password];

            /**
             * Try logging in
             */
            if(!Auth::attempt($credentials)) {
                return response()->json([
                    'message' => 'Credentials were unauthorized.'
                ], 401);
            }

            $user = $request->user();
            $contactId = $user->contact_id;
            $firstname = $user->first_name;
            $lastname = $user->last_name;
        }
        
        $year = date('Y') - 4;
        $projects = $this->projects
                    ->leftJoin('user_year', function ($join) {
                        $join->on('user_year.project_year', '=', 'project.project_year');
                        $join->on('user_year.country_id', '=', 'project.country_id');
                    })
                    ->leftJoin('countries', 'countries.id', '=', 'project.country_id')
                    ->where('user_year.contact_id', $contactId)
                    ->where('project.closed', false)
                    ->where('project.project_year', '>=', $year)
                    ->select('project.project_year_id', 'project.country_id', 'project.project_year', 'project.closed', 'project.status', 'project.target_sales', 'project.id', 'project.action_to_be_taken', 'countries.name')
                    ->get();

        if ($projects) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Projects were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Projects were not found!',
                'error' => false
            ];
        }
        
        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => [
                'projects' => $projects,
                'credentials' => [
                    'hash' => $contactId,
                    'firstname' => $firstname,
                    'lastname' => $lastname
                ]
            ],
            'count' => [
                'result' => count($projects),
                'total' => count($projects),
                'name' => 'projects'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * All employees
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $user = $input['user'];
        $employees = $this->users->where('contact_id', '!=', $user)->whereNotIn('id', [1,2])->get();
        $contacts = $this->users
                    ->join('contacts', 'contacts.contact_id', '=', 'sf_user.contact_id')
                    // ->where('sf_user.contact_id', '!=', $user)
                    ->where('contacts.company_id', 'b625ee22bcc5ec9ad8d50829a5011583c70901b051336075046fa7c63bb03387')
                    ->select('contacts.id',
                            'contacts.contact_id',
                            'contacts.salutation',
                            'contacts.first_name',
                            'contacts.middle_name',
                            'contacts.last_name',
                            'contacts.job_title',
                            'contacts.office_phone',
                            'contacts.office_fax',
                            'contacts.mobile_phone',
                            'contacts.email',
                            'contacts.office_address_line_1',
                            'contacts.office_address_line_2',
                            'contacts.office_address_line_3',
                            'contacts.office_address_town_city',
                            'contacts.office_address_region',
                            'contacts.office_postal_code',
                            'contacts.contact_type',
                            'contacts.company_id',
                            'contacts.pa_id',
                            'contacts.created_by',
                            'contacts.created_date',
                            'contacts.last_updated_by',
                            'contacts.last_updated_date',
                            'contacts.economic_updates',
                            'contacts.left_company',
                            'contacts.removed',
                            'contacts.created_at',
                            'contacts.updated_at',
                            'contacts.last_contacted',
                            'contacts.last_met'
                    )
                    ->get();

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Employees were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => [
                'users' => $employees,
                'contacts' => $contacts
            ],
            'count' => count($employees)
        ];

        return response()->json($response);
    }

    /**
     * List of employees with access to project
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function all(Request $request)
    {
        $baseUrl = url('/api/employees');
        $selfUrl = url()->full();

        $employees = $this->contacts
                    ->leftJoin('sf_user', 'sf_user.contact_id', '=', 'contacts.contact_id')
                    ->where('contacts.removed', false)
                    ->where('sf_user.sf_user_departmentId', 2)
                    ->select('contacts.id AS id', 'contacts.contact_id AS hash', 'contacts.first_name AS firstname', 'contacts.last_name AS lastname')
                    ->distinct()
                    ->get();

        if(count($employees) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Employees were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Employees were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $employees,
            'count' => [
                'result' => count($employees),
                'total' => count($employees),
                'name' => 'employees'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Find assignee of company for current project
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function assignee(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $employee = $this->companies
                    ->leftJoin('tblEmployees', 'tblEmployees.fldEmployeeId', '=', 'tblCompanies.tblEmployees_fldEmployeeId')
                    ->where('tblCompanies.fldCompanyId', $companyId)
                    ->where('tblCompanies.fldCompanyDeleted', false)
                    ->where('tblEmployees.fldEmployeeDeleted', false)
                    ->select('tblEmployees.fldEmployeeId AS id', 'tblEmployees.fldEmployeeFirstname AS firstname', 'tblEmployees.fldEmployeeLastname AS lastname')
                    ->get();

        if(count($employee) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Employee was found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Employee was not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $employee,
            'count' => [
                'result' => count($employee),
                'total' => count($employee),
                'name' => 'employee'
            ]
        ];

        return response()->json($response, $code);
    }
}