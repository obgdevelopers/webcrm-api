<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CountriesController extends Controller
{
    /**
     * Countries per region
     *
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function all(Request $request)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();
        
        $regions = $this->regions->where('removed', false)->whereNotIn('id', [4,6,7])->get();
        $data = [];
        foreach ($regions as $region) {
            $push = [];
            $name = $region->name;
            $id = $region->id;

            $push['id'] = $id;
            $push['name'] = $name;

            $countries = $this->countries
                        ->where('region_id', $id)
                        ->where('removed', false)
                        ->where('dormant', false)
                        ->select('countries.name AS name', 'countries.id AS id', 'countries.code AS code')
                        ->orderBy('countries.name')
                        ->get();
            $push['countries'] = $countries;

            $data[] = $push;
        }

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Countries were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'countries'
            ]
        ];

        return response()->json($response);
    }
}
