<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SectorsController extends Controller
{
    /**
     * List of sectors
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function list(Request $request)
    {
        $baseUrl = url('/api/statuses');
        $selfUrl = url()->full();

        $sectors = [];
        $sectors = $this->sectors->where('removed', false)->select('sector_id AS id', 'name AS text')->get();

        if(count($sectors) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Sectors were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Sectors were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $sectors,
            'count' => [
                'result' => count($sectors),
                'total' => count($sectors),
                'name' => 'sectors'
            ]
        ];

        return response()->json($response, $code);
    }
}