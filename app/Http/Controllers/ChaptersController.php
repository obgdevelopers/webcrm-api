<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChaptersController extends Controller
{
    /**
     * All chapters for project
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $chapters = $this->chapters->where('country_id', $country->id)->where('project_year', $year->text)->where('removed', false)->get();

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Chapters were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $chapters,
            'count' => count($chapters)
        ];

        return response()->json($response);
    }

    /**
     * Fetch updates for chapters
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $datetime = $input['datetime'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $chapters = $this->chapters
                    ->where('country_id', $country->id)
                    ->where('project_year', $year->text)
                    ->where('updated_at', '>', $datetime)
                    ->get();

        if(count($chapters) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Chapters were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Chapters were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $chapters,
            'count' => [
                'result' => count($chapters),
                'total' => count($chapters),
                'name' => 'chapters'
            ]
        ];

        return response()->json($response, $code);
    }
}
