<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;
use App\Mail\MeetingUpdates;
use App\Users;
use App\Contacts;
use App\Terms;
use App\ContactsAssistants;
use App\PersonalAssistants;
use App\CommunicationLogs;
use App\Countries;
use App\EconomicSubscriptions;
use App\Deliveries;
use App\Statuses;
use App\Options;
use App\Sectors;
use App\Employees;
use App\Companies;
use App\ContactsTags;
use App\CompaniesTags;
use App\CompaniesNotes;
use App\Meetings;
use App\MeetingsContacts;
use App\MeetingsEmployees;
use App\MeetingsNotesContents;
use App\MeetingsNotesHeadings;
use App\Projects;
use App\SavedSearches;
use App\Reminders;
use App\Histories;
use App\EntitiesTags;
use App\Contracts;
use App\ContractsPartnerships;
use App\ArtworksContracts;
use App\MeetingsAttendances;
use App\Regions;
use App\CompaniesProjectsStatuses;
use App\Chapters;
use App\Artworks;
use App\AdvisoryPotential;
use App\CompaniesActionNextYear;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $users;
    protected $contacts;
    protected $terms;
    protected $contactsAssistants;
    protected $personalAssistants;
    protected $communicationLogs;
    protected $countries;
    protected $economicSubscriptions;
    protected $deliveries;
    protected $statuses;
    protected $options;
    protected $sectors;
    protected $employees;
    protected $companies;
    protected $contactsTags;
    protected $companiesTags;
    protected $companiesNotes;
    protected $meetings;
    protected $meetingsContacts;
    protected $meetingsEmployees;
    protected $meetingsNotesContents;
    protected $meetingsNotesHeadings;
    protected $projects;
    protected $savedSearches;
    protected $reminders;
    protected $histories;
    protected $entitiesTags;
    protected $contracts;
    protected $contractsPartnerships;
    protected $artworksContracts;
    protected $meetingsAttendances;
    protected $regions;
    protected $companiesProjectsStatuses;
    protected $chapters;
    protected $artworks;
    protected $advisoryPotential;
    protected $companiesActionNextYear;

    protected $headers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(function ($request, $next) {
        //     $this->user = Auth::user();
        //     $this->granted = $this->getPermissions(Auth::user()->tblRoles_fldRoleId);
        //     return $next($request);
        // });;
        // $this->grants = new Grants;
        // $this->permissions = new Permissions;
        // $this->roles = new Roles;
        // $this->syllabi = new Syllabi;
        // $this->syllabiInfo = new SyllabiInfo;
        // $this->syllabiAssignments = new SyllabiAssignments;
        // $this->syllabiScrutinizers = new SyllabiScrutinizers;
        $this->users = new Users;
        $this->contacts = new Contacts;
        $this->terms = new Terms;
        $this->contactsAssistants = new ContactsAssistants;
        $this->personalAssistants = new PersonalAssistants;
        $this->communicationLogs = new CommunicationLogs;
        $this->countries = new Countries;
        $this->economicSubscriptions = new EconomicSubscriptions;
        $this->deliveries = new Deliveries;
        $this->statuses = new Statuses;
        $this->options = new Options;
        $this->sectors = new Sectors;
        $this->employees = new Employees;
        $this->companies = new Companies;
        $this->contactsTags = new ContactsTags;
        $this->companiesTags = new CompaniesTags;
        $this->companiesNotes = new CompaniesNotes;
        $this->meetings = new Meetings;
        $this->meetingsContacts = new MeetingsContacts;
        $this->meetingsEmployees = new MeetingsEmployees;
        $this->meetingsNotesContents = new MeetingsNotesContents;
        $this->meetingsNotesHeadings = new MeetingsNotesHeadings;
        $this->projects = new Projects();
        $this->savedSearches = new SavedSearches();
        $this->reminders = new Reminders();
        $this->histories = new Histories();
        $this->entitiesTags = new EntitiesTags();
        $this->contracts = new Contracts();
        $this->contractsPartnerships = new ContractsPartnerships();
        $this->artworksContracts = new ArtworksContracts();
        $this->meetingsAttendances = new MeetingsAttendances();
        $this->regions = new Regions();
        $this->companiesProjectsStatuses = new CompaniesProjectsStatuses();
        $this->chapters = new Chapters();
        $this->artworks = new Artworks();
        $this->advisoryPotential = new AdvisoryPotential();
        $this->companiesActionNextYear = new CompaniesActionNextYear();
    }

    /**
     * Extract session from header
     *
     * @param [object] request
     *
     * @return [array] projects
     */
    public function parseProject($request) {
        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];

        return $projects;
    }

    /**
     * Generate an encrypted password
     *
     * @param [string] table
     *
     * @return [string] hash
     */
    public function generateHash($table) {
        $hash = hash("sha256", $table.openssl_random_pseudo_bytes(30));
        return $hash;
    }

    /**
     * Generate an ics and send it
     */
    public function generateInvitation($method, $uid, $name, $date, $sequence, $emails, $delta, $timezone, $mailsubject = '', $location = '') {
        $message = [];
        $filename = 'invite.ics';
        $subject = '';

        switch ($method) {
            case 'create':
                $message[0] = 'BEGIN:VCALENDAR';
                $message[1] = 'PRODID:-//Oxford Business Group//OBG Calendar//EN';
                $message[2] = 'VERSION:2.0';
                $message[3] = 'CALSCALE:GREGORIAN';
                $message[4] = 'X-WR-TIMEZONE:'.$timezone;
                $message[5] = 'METHOD:REQUEST';
                $message[6] = 'BEGIN:VEVENT';
                $message[7] = 'DTSTART;TZID='.$timezone.':'.date('Ymd\THis', $date);
                $message[8] = 'DTEND;TZID='.$timezone.':'.date('Ymd\THis', $date);
                $message[9] = 'DTSTAMP:'.date('Ymd\THis', $date);
                $message[10] = 'ORGANIZER;CN=webcrm@oxfordbusinessgroup.com:mailto:webcrm@oxfordbusinessgroup.com';
                $message[11] = 'ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;RSVP=TRUE;CN=webcrm@oxfordbusinessgroup.com;X-NUM-GUESTS=0:mailto:webcrm@oxfordbusinessgroup.com';
                $message[12] = $this->buildRecipients($emails);
                $message[13] = 'UID:'.$uid;
                $message[14] = 'LOCATION:'.$location;
                $message[15] = 'SEQUENCE:0';
                $message[16] = 'STATUS:CONFIRMED';
                $message[17] = 'SUMMARY:'.$name.' - '.$this->findDelta($delta).' - OBG Webcrm';
                $message[18] = 'TRANSP:OPAQUE';
                $message[19] = 'END:VEVENT';
                $message[20] = 'END:VCALENDAR';

                $invitation = join("\r\n", $message);
                header('text/calendar');
                file_put_contents(public_path($filename), $invitation);

                $subject = $mailsubject ? $mailsubject : $name.' - '.$this->findDelta($delta).' - OBG Webcrm';
            break;
            case 'update':
                $message[0] = 'BEGIN:VCALENDAR';
                $message[1] = 'PRODID:-//Oxford Business Group//OBG Calendar//EN';
                $message[2] = 'VERSION:2.0';
                $message[3] = 'CALSCALE:GREGORIAN';
                $message[4] = 'X-WR-TIMEZONE:'.$timezone;
                $message[5] = 'METHOD:REQUEST';
                $message[6] = 'BEGIN:VEVENT';
                $message[7] = 'DTSTART;TZID='.$timezone.':'.date('Ymd\THis', $date);
                $message[8] = 'DTEND;TZID='.$timezone.':'.date('Ymd\THis', $date);
                $message[9] = 'DTSTAMP:'.date('Ymd\THis', $date);
                $message[10] = 'ORGANIZER;CN=webcrm@oxfordbusinessgroup.com:mailto:webcrm@oxfordbusinessgroup.com';
                $message[11] = 'ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;RSVP=TRUE;CN=webcrm@oxfordbusinessgroup.com;X-NUM-GUESTS=0:mailto:webcrm@oxfordbusinessgroup.com';
                $message[12] = $this->buildRecipients($emails);
                $message[13] = 'UID:'.$uid;
                $message[14] = 'LOCATION:'.$location;
                $message[15] = 'SEQUENCE:'.($sequence + 1);
                $message[16] = 'STATUS:CONFIRMED';
                $message[17] = 'SUMMARY:'.$name.' - '.$this->findDelta($delta).' - OBG Webcrm';
                $message[18] = 'TRANSP:OPAQUE';
                $message[19] = 'END:VEVENT';
                $message[20] = 'END:VCALENDAR';

                $invitation = join("\r\n", $message);
                header('text/calendar');
                file_put_contents(public_path($filename), $invitation);

                $subject = $mailsubject ? 'Updated: '.$mailsubject : 'Updated: '.$name.' - '.$this->findDelta($delta).' - OBG Webcrm';
            break;
            case 'delete':
                $message[0] = 'BEGIN:VCALENDAR';
                $message[1] = 'PRODID:-//Oxford Business Group//OBG Calendar//EN';
                $message[2] = 'VERSION:2.0';
                $message[3] = 'CALSCALE:GREGORIAN';
                $message[4] = 'METHOD:CANCEL';
                $message[5] = 'BEGIN:VEVENT';
                $message[6] = 'DTSTART:'.date('Ymd\THis', $date);
                $message[7] = 'DTEND:'.date('Ymd\THis', $date);
                $message[8] = 'DTSTAMP:'.date('Ymd\THis', $date);
                $message[9] = 'ORGANIZER;CN=webcrm@oxfordbusinessgroup.com:mailto:webcrm@oxfordbusinessgroup.com';
                $message[10] = 'ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;RSVP=TRUE;CN=webcrm@oxfordbusinessgroup.com;X-NUM-GUESTS=0:mailto:webcrm@oxfordbusinessgroup.com';
                $message[11] = $this->buildRecipients($emails);
                $message[12] = 'UID:'.$uid;
                $message[13] = 'LOCATION:'.$location;
                $message[14] = 'SEQUENCE:'.($sequence + 1);
                $message[15] = 'STATUS:CANCELLED';
                $message[16] = 'SUMMARY:'.$name.' - '.$this->findDelta($delta).' - OBG Webcrm';
                $message[17] = 'TRANSP:OPAQUE';
                $message[18] = 'END:VEVENT';
                $message[19] = 'END:VCALENDAR';

                $invitation = join("\r\n", $message);
                header('text/calendar');
                file_put_contents(public_path($filename), $invitation);

                $subject = $mailsubject ? 'Cancelled: '.$mailsubject : 'Cancelled: '.$name.' - '.$this->findDelta($delta).' - OBG Webcrm';
            break;
        }

        /*$recipients = [];
        foreach($emails as $email) {
            $active = $this->users->where('email', $email)->where('send_meeting_invite', true)->first();
            if ($active) {
                array_push($recipients, $email);
            }
        }*/

        Mail::to($emails)->send(new MeetingUpdates($subject));

        unlink(public_path($filename));
    }

    /**
     * Build emails recipient
     */
    public function buildRecipients($emails) {
        $build = [];
        foreach($emails as $email) {
            $active = $this->users->where('email', $email)->where('send_meeting_invite', true)->first();
            if ($active) {
                array_push($build, 'ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN='.$email.';X-NUM-GUESTS=0:mailto:'.$email);
            }
        }

        return join("\r\n", $build);
    }
}
