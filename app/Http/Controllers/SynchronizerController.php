<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SynchronizerController extends Controller
{

    /**
     * Push updates from offline
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function push(Request $request)
    {
        $baseUrl = url('/api/updates');
        $selfUrl = url()->full();

        $parameters = $request->all();
        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();

        $items = $input['items'];
        $invites = [];

        try {
            foreach ($items as $item) {
                $table = $item['table'];
                $operation = $item['operation'];
    
                unset($item['table']);
                unset($item['operation']);
                unset($item['id']);
    
                switch ($table) {
                    case 'companies':
                        $hash = $item['company_id'];
                        unset($item['company_id']);

                        $item['assigned_user'] = $item['assigned_user'] != NULL ? $item['assigned_user'] : '';
                        $item['country_custom'] = $item['country_custom'] != NULL ? $item['country_custom'] : '';
                        if ($operation == 'create') {
                            $company = $this->companies->firstOrCreate(['company_id' => $hash], $item);
                        } else if ($operation == 'update') {
                            $company = $this->companies->where('company_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        } else if ($operation == 'delete') {
                            $company = $this->companies->where('company_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        }
                    break;
                    case 'contacts':
                        $hash = $item['contact_id'];
                        unset($item['contact_id']);
                        if ($operation == 'create') {
                            $contact = $this->contacts->firstOrCreate(['contact_id' => $hash], $item);
                        } else if ($operation == 'update') {
                            $contact = $this->contacts->where('contact_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        } else if ($operation == 'delete') {
                            $contact = $this->contacts->where('contact_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        }
                    break;
                    case 'histories':
                        $hash = $item['history_id'];
                        unset($item['history_id']);
                        if ($operation == 'create') {
                            $history = $this->histories->firstOrCreate(['history_id' => $hash], $item);
                        } else if ($operation == 'update') {
                            $history = $this->histories->where('history_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        } else if ($operation == 'delete') {
                            $history = $this->histories->where('history_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        }
                    break;
                    case 'personal_assistants':
                        $hash = $item['pa_id'];
                        unset($item['pa_id']);
                        if ($operation == 'create') {
                            $assistant = $this->personalAssistants->firstOrCreate(['pa_id' => $hash], $item);
                        } else if ($operation == 'update') {
                            $assistant = $this->personalAssistants->where('pa_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        } else if ($operation == 'delete') {
                            $assistant = $this->personalAssistants->where('pa_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        }
                    break;
                    case 'contacts_economic_updates_countries':
                        $hash = $item['contacts_countries_id'];
                        unset($item['contacts_countries_id']);
                        if ($operation == 'create') {
                            $subscription = $this->economicSubscriptions->firstOrCreate(['contacts_countries_id' => $hash], $item);
                        } else if ($operation == 'update') {
                            $subscription = $this->economicSubscriptions->where('contacts_countries_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        } else if ($operation == 'delete') {
                            $subscription = $this->economicSubscriptions->where('contacts_countries_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        }
                    break;
                    case 'meeting_notes':
                        $hash = $item['meeting_note_id'];
                        unset($item['meeting_note_id']);
                        if ($operation == 'create') {
                            array_push($invites, [
                                'hash' => $hash,
                                'operation' => 'create'
                            ]);
                            $meeting = $this->meetings->firstOrCreate(['meeting_note_id' => $hash], $item);
                        } else if ($operation == 'update') {
                            array_push($invites, [
                                'hash' => $hash,
                                'operation' => 'update'
                            ]);
                            $meeting = $this->meetings->where('meeting_note_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        } else if ($operation == 'delete') {
                            array_push($invites, [
                                'hash' => $hash,
                                'operation' => 'delete'
                            ]);
                            $meeting = $this->meetings->where('meeting_note_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        }
                    break;
                    case 'meeting_note_attendance':
                        $hash = $item['attendance_id'];
                        unset($item['attendance_id']);
                        if ($operation == 'create') {
                            $attendances = $this->meetingsAttendances->firstOrCreate(['attendance_id' => $hash], $item);
                        } else if ($operation == 'update') {
                            $attendances = $this->meetingsAttendances->where('attendance_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        } else if ($operation == 'delete') {
                            $attendances = $this->meetingsAttendances->where('attendance_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        }
                    break;
                    case 'meeting_note_content':
                        $hash = $item['mn_content_id'];
                        unset($item['mn_content_id']);
                        if ($operation == 'create') {
                            $meeting = $this->meetingsNotesContents->firstOrCreate(['mn_content_id' => $hash], $item);
                        } else if ($operation == 'update') {
                            $meeting = $this->meetingsNotesContents->where('mn_content_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        } else if ($operation == 'delete') {
                            $meeting = $this->meetingsNotesContents->where('mn_content_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        }
                    break;
                    case 'company_advisory_potential':
                        $hash = $item['company_id'];
                        unset($item['company_id']);
                        if ($operation == 'create') {
                            $meeting = $this->advisoryPotential->firstOrCreate(['company_id' => $hash], $item);
                        } else if ($operation == 'update') {
                            $meeting = $this->advisoryPotential->where('company_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        } else if ($operation == 'delete') {
                            $meeting = $this->advisoryPotential->where('company_id', $hash)->where('updated_at', '<', $item['updated_at'])->update($item);
                        }
                    break;
                    case 'meeting_notes_action_next_year':
                        $hash = $item['meeting_action_id'];
                        unset($item['meeting_action_id']);
                        if ($operation == 'create') {
                            $action = $this->companiesActionNextYear->firstOrCreate(['meeting_action_id' => $hash], $item);
                        } else if ($operation == 'update') {
                            $action = $this->companiesActionNextYear->where('meeting_action_id', $hash)->where('updated_date', '<', $item['updated_date'])->update($item);
                        } else if ($operation == 'delete') {
                            $action = $this->companiesActionNextYear->where('meeting_action_id', $hash)->where('updated_date', '<', $item['updated_date'])->update($item);
                        }
                    break;
                }
            }

            if (count($invites) > 0) {
                $country = $this->projects
                    ->leftJoin('countries', 'countries.id', '=', 'project.country_id')
                    ->whereIn('project.id', $projects)
                    ->select('countries.timezone AS timezone')
                    ->first();

                foreach ($invites as $invite) {
                    $hash = $invite['hash'];
                    $operation = $invite['operation'];

                    $meeting = $this->meetings
                    ->leftJoin('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('meeting_note_id', $hash)
                    ->select('meeting_notes.*', 'companies.name AS name')
                    ->first();

                    $uid = $meeting->email_uid;
                    if ($uid) {
                        $name = $meeting->name;
                        $date = $meeting->meeting_date;
                        $sequence = $meeting->email_sequence;
                        $delta = $meeting->delta;
                        $attendees = $this->meetingsAttendances->findAttendees([['hash' => $hash]]);
                    
                        $employees = $attendees[0]['employees'];
                        $emails = [];
    
                        foreach ($employees as $employee) {
                            array_push($emails, $employee['email']);
                        }
    
                        $timezone = 'Asia/Manila';
                        if ($country->timezone) {
                            $timezone = $country->timezone;
                        }    
                        
                        if ($operation == 'create') {
                            $this->generateInvitation('create', $uid, $name, $date, $sequence, $emails, $delta, $timezone);
                        } else if ($operation == 'update') {
                            $this->generateInvitation('update', $uid, $name, $date, $sequence, $emails, $delta, $timezone);
                        } else if ($operation == 'delete') {
                            $this->generateInvitation('delete', $uid, $name, $date, $sequence, $emails, $delta, $timezone);
                        }
                    }
                }
            }
            
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Updates were pushed!',
                'error' => false
            ];
        } catch (Exception $e) {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Updates were not pushed!',
                'error' => $e
            ];
        }
        
        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => null,
            'count' => 0
        ];

        return response()->json($response, $code);
    }

    /**
     * Return delta title
     * @param delta
     * 
     * @return [string] title
     */
    public function findDelta($delta) 
    {
        $deltas = [0 => 'PR Meeting', 1 => '1st Pointer', 2 => '2nd Pointer', 3 => '3rd Pointer', 4 => 'Barter Meeting', 5 => 'Follow Up Meeting', 6 => '1st and 2nd Pointer', 8 => 'Analyst Meeting'];
        $title = $deltas[$delta];
        return $title;
    }
}