<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArtworksController extends Controller
{
    /**
     * All artworks for project
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $artworks = $this->artworks
                        ->join('adverts', 'adverts.advert_id', '=', 'artwork.advert_id')
                        ->join('companies', 'companies.company_id', '=', 'adverts.company_id')
                        ->where('companies.country_id', $country->id)
                        ->where('adverts.project_year', $year->text)
                        ->where('artwork.removed', false)
                        ->where('companies.removed', false)
                        ->select('artwork.*')
                        ->distinct('artwork.id')
                        ->get();

        $artworkContracts = $this->artworksContracts
                        ->join('artwork', 'artwork_advert.artwork_id', '=', 'artwork.artwork_id')
                        ->join('adverts', 'adverts.advert_id', '=', 'artwork.advert_id')
                        ->join('companies', 'companies.company_id', '=', 'adverts.company_id')
                        ->where('companies.country_id', $country->id)
                        ->where('adverts.project_year', $year->text)
                        ->where('artwork.removed', false)
                        ->where('companies.removed', false)
                        ->select('artwork_advert.*')
                        ->distinct('artwork_advert.id')
                        ->get();

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Artworks were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => [
                'artworks' => $artworks,
                'artworks_contracts' => $artworkContracts
            ],
            'count' => [
                'artworks' => count($artworks),
                'artworks_contracts' => count($artworkContracts)
            ]
        ];

        return response()->json($response);
    }

    /**
     * Fetch updates for artworks
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $datetime = $input['datetime'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $artworks = $this->artworks
                        ->join('adverts', 'adverts.advert_id', '=', 'artwork.advert_id')
                        ->join('companies', 'companies.company_id', '=', 'adverts.company_id')
                        ->where('companies.country_id', $country->id)
                        ->where('adverts.project_year', $year->text)
                        ->where('artwork.updated_at', '>', $datetime)
                        ->select('artwork.*')
                        ->distinct('artwork.id')
                        ->get();

        $artworkContracts = $this->artworksContracts
                        ->join('artwork', 'artwork_advert.artwork_id', '=', 'artwork.artwork_id')
                        ->join('adverts', 'adverts.advert_id', '=', 'artwork.advert_id')
                        ->join('companies', 'companies.company_id', '=', 'adverts.company_id')
                        ->where('companies.country_id', $country->id)
                        ->where('adverts.project_year', $year->text)
                        ->where('artwork.removed', false)
                        ->where('artwork_advert.updated_at', '>', $datetime)
                        ->where('companies.removed', false)
                        ->select('artwork_advert.*')
                        ->distinct('artwork_advert.id')
                        ->get();


        if(count($artworks) > 0 || count($artworkContracts) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Artworks were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Artworks were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => [
                'artworks' => $artworks,
                'artworks_contracts' => $artworkContracts
            ],
            'count' => [
                'artworks' => count($artworks),
                'artworks_contracts' => count($artworkContracts)
            ]
        ];

        return response()->json($response, $code);
        // $response = [
        //     'status' => [
        //         'type' => 'success',
        //         'code' => 200,
        //         'message' => 'Artworks were found!',
        //         'error' => 'false'
        //     ],
        //     '_links' => [
        //         'base' => $baseUrl,
        //         'self' => $selfUrl
        //     ],
            
        // ];

        // return response()->json($response);
    }
}
