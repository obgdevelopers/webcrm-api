<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdvisoryPotentialController extends Controller
{
    /**
     * Update advisory potential
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function update(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $input = $request->all();
        $user = $input['user'];
        $potential = isset($input['potential']) ? $input['potential'] : 'false';
        $potentialReason = isset($input['potential_reason']) ? $input['potential_reason'] : '';

        if ($potential == 'true') {
            $company = $this->companies->where('company_id', $companyId)->update(['advisory_potential' => 1]);
            // $this->advisoryPotential->where('company_id', $companyId)->update(['advisory_potential' => $potentialReason]);
            $advisory = $this->advisoryPotential->updateOrCreate(['company_id' => $companyId], ['advisory_potential' => $potentialReason, 'last_updated_by' => $user]);
        } else {
            $company = $this->companies->where('company_id', $companyId)->update(['advisory_potential' => 0]);
        }
        
        if($company) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Company was updated successfully!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $company,
            'count' => [
                'result' => count($company),
                'total' => count($company),
                'name' => 'companies'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Fetch updates for potentials
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/updates');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();

        $datetime = $input['datetime'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $companies = $this->companies
                    ->where('companies.country_id', $country->id)
                    ->distinct('id')
                    ->select('companies.company_id AS company_id')
                    ->get();
        
        $ids = [];
        foreach ($companies as $company) {
            array_push($ids, $company->company_id);
        }

        $potentials = $this->advisoryPotential
                    ->where('company_advisory_potential.updated_at', '>', $datetime)
                    ->whereIn('company_advisory_potential.company_id', $ids)
                    ->select('company_id', 'advisory_potential', 'created_at', 'updated_at', 'last_updated_by')
                    ->get();

        if(count($potentials) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Potentials were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => true
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $potentials,
            'count' => [
                'result' => count($potentials),
                'total' => count($potentials),
                'name' => 'potentials'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * All meetings
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        
        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $companies = $this->companies
                    ->where('companies.country_id', $country->id)
                    ->distinct('id')
                    ->select('companies.company_id AS company_id')
                    ->get();
        
        $ids = [];
        foreach ($companies as $company) {
            array_push($ids, $company->company_id);
        }

        $potentials = $this->advisoryPotential
                    ->where('company_advisory_potential.company_id', $ids)
                    ->select('company_id', 'advisory_potential', 'created_at', 'updated_at', 'last_updated_by')
                    ->get();


        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Potentials were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $potentials,
            'count' => count($potentials)
        ];

        return response()->json($response);
    }
}
