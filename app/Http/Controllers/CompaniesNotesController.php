<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompaniesNotesController extends Controller
{
    /**
     * Find company notes for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function list(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $companyNotes = $this->companiesNotes
                ->leftJoin('contacts', 'contacts.contact_id', '=', 'company_notes_tbl.company_notes_contactid')
                ->where('company_notes_tbl.company_notes_companyid', '=', $companyId)
                ->select('company_notes_tbl.company_notes_id AS id', 'company_notes_tbl.company_notes_content AS content', 'company_notes_tbl.company_notes_private AS private', 'company_notes_tbl.company_notes_created AS created', 'company_notes_tbl.company_notes_updated AS modified', 'contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'company_notes_contactid AS creator')
                ->orderBy('company_notes_tbl.company_notes_updated', 'DESC')
                ->get();

        // $companyNotes = $this->companiesNotes
        //         ->leftJoin('tblUsers', 'tblUsers.fldUserId', '=', 'tblCompaniesNotes.tblUsers_fldUserId')
        //         ->leftJoin('tblEmployees', 'tblEmployees.tblUsers_fldUserId', '=', 'tblUsers.fldUserId')
        //         ->where('tblCompaniesNotes.tblCompanies_fldCompanyId', '=', $companyId)
        //         ->where('tblCompaniesNotes.fldCompanyNoteDeleted', false)
        //         ->select('tblCompaniesNotes.fldCompanyNoteId AS id', 'tblCompaniesNotes.fldCompanyNoteContent AS content', 'tblCompaniesNotes.fldCompanyNotePrivate AS private', 'tblCompaniesNotes.fldCompanyNoteCreated AS created', 'tblCompaniesNotes.fldCompanyNoteModified AS modified', 'tblEmployees.fldEmployeeFirstname AS firstname', 'tblEmployees.fldEmployeeLastname AS lastname')
        //         ->get();

        if(count($companyNotes) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Company notes were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Company notes were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $companyNotes,
            'count' => [
                'result' => count($companyNotes),
                'total' => count($companyNotes),
                'name' => 'company_notes'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Add notes for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function add(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $input = $request->all();

        $notes = $input['notes'];
        $privacy = $input['privacy'];
        if($privacy == "true") {
            $privacy = 1;
        } else {
            $privacy = 0;
        }
        $user = $input['user'];

        $note = $this->companiesNotes->create([
            'company_notes_content' => $notes,
            'company_notes_companyid' => $companyId,
            'company_notes_contactid' => $user,
            'company_notes_created' => strtotime(date('Y-m-d H:i:s')),
            'company_notes_updated' => strtotime(date('Y-m-d H:i:s')),
            'company_notes_private' => $privacy
        ]);

        $data = [];
        if($note) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Company note was created successfully!',
                'error' => false
            ];
            $data['id'] = $note->id;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Company note was not created!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'notes'
            ]
        ];

        return response()->json($response, $code);

    }

    /**
     * Update notes for company
     * @param noteId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function update(Request $request, $noteId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $input = $request->all();

        $notes = $input['notes'];
        $privacy = $input['privacy'];
        if($privacy == "true") {
            $privacy = 1;
        } else {
            $privacy = 0;
        }

        $user = $input['user'];

        $note = $this->companiesNotes->find($noteId)->update([
            'company_notes_content' => $notes,
            'company_notes_updated' => strtotime(date('Y-m-d H:i:s')),
            'company_notes_private' => $privacy
        ]);

        $data = [];
        if($note) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Company note was update successfully!',
                'error' => false
            ];
            $data['id'] = $note;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Company note was not updated!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'notes'
            ]
        ];

        return response()->json($response, $code);

    }


    /**
     * Delete notes for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function delete(Request $request, $noteId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $input = $request->all();

        $note = $this->companiesNotes->find($noteId)->delete();

        $data = [];
        if($note) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Company note was deleted successfully!',
                'error' => false
            ];
            $data = $note;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Company note was not deleted!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'notes'
            ]
        ];

        return response()->json($response, $code);

    }
}