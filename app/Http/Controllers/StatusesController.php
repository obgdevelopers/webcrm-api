<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatusesController extends Controller
{
    /**
     * List of statuses by group or individual
     * @param [string] type
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function list(Request $request)
    {
        $baseUrl = url('/api/statuses');
        $selfUrl = url()->full();

        $request->validate([
            'type' => 'required|string'
        ]);

        $parameters = $request->all();
        $type = $parameters['type'];
        $statuses = [];
        switch($type) {
            case 'group':
                $statuses = $this->statuses->findGroupedStatuses();
                break;
            case 'individual':
                $statuses = $this->statuses->where('tblStatuses.fldStatusDeleted', false)->select('tblStatuses.fldStatusId AS id', 'tblStatuses.fldStatusName AS text')->get();
                break;
        }



        if(count($statuses) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Statuses were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Statuses were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $statuses,
            'count' => [
                'result' => count($statuses),
                'total' => count($statuses),
                'name' => 'statuses'
            ]
        ];

        return response()->json($response, $code);
    }
    /**
     * Get Online DB/System Version
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     */
    public function getOnlineVersion(Request $request)
    {
        $baseUrl = url('/api/version');
        $selfUrl = url()->full();
        $headers = $request->headers->all();

        $versions = $this->options->getOnlineDBVer();

        if(count($versions) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Database and System version has been fetched!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Database and System version was not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $versions
        ];

        return response()->json($response, $code);
    }

    /**
     * Get Online DB/System Version
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     */
    public function getTables(Request $request)
    {
        $baseUrl = url('/api/tables');
        $selfUrl = url()->full();
        $headers = $request->headers->all();

        $tables = $this->options->getTables();

        if(count($tables) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Database tables has been fetched!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Database tables was not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $tables
        ];

        return response()->json($response, $code);
    }

     /**
     * Get Online DB/System Schema Version
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     */
    public function getTableSchema(Request $request)
    {
        $baseUrl = url('/api/schema');
        $selfUrl = url()->full();
        $headers = $request->headers->all();
        $table = $request->input('table');
        $tables = $this->options->getTableSchema($table);

        if($tables != null) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Database table Schema has been fetched!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Database table Schema was not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $tables
        ];

        return response()->json($response, $code);
    }
}
