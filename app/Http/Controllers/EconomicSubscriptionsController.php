<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EconomicSubscriptionsController extends Controller
{
    /**
     * View economic subscription for contact
     * @param contactId
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function contacts(Request $request, $contactId)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();
        
        $subscriptions = $this->economicSubscriptions
                        ->leftJoin('countries', 'countries.id', '=', 'contacts_economic_updates_countries.country_id')
                        ->where('contacts_economic_updates_countries.contact_id', $contactId)
                        ->where('contacts_economic_updates_countries.removed', false)
                        ->select('countries.id AS id', 'countries.name AS name', 'contacts_economic_updates_countries.date_subscribed AS subscription_date')
                        ->get();

        if(count($subscriptions) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Economic subscriptions were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Economic subscriptions were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $subscriptions,
            'count' => [
                'result' => count($subscriptions),
                'total' => count($subscriptions),
                'name' => 'subscriptions'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * All assistants for project
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        // $iteration = $input['iteration'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $subscriptions = $this->economicSubscriptions
                    ->join('contacts', 'contacts.contact_id', '=', 'contacts_economic_updates_countries.contact_id')
                    ->join('companies', 'companies.company_id', '=', 'contacts.company_id')
                    ->where('companies.removed', false)
                    ->where('contacts.removed', false)
                    ->where('companies.country_id', $country->id)
                    ->select('contacts_economic_updates_countries.*')
                    ->distinct('contacts_economic_updates_countries.id')
                    ->get();

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Economic subscriptions were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $subscriptions,
            'count' => count($subscriptions)
        ];

        return response()->json($response);
    }

    /**
     * Fetch updates for subscriptions
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/updates');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        $datetime = $input['datetime'];

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $subscriptions = $this->economicSubscriptions
                    ->join('contacts', 'contacts.contact_id', '=', 'contacts_economic_updates_countries.contact_id')
                    ->join('companies', 'companies.company_id', '=', 'contacts.company_id')
                    ->where('contacts_economic_updates_countries.updated_at', '>', $datetime)
                    ->where('companies.country_id', $country->id)
                    ->select('contacts_economic_updates_countries.*')
                    ->distinct('contacts_economic_updates_countries.id')
                    ->get();

        if(count($subscriptions) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Subscriptions were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Subscriptions were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $subscriptions,
            'count' => [
                'result' => count($subscriptions),
                'total' => count($subscriptions),
                'name' => 'subscriptions'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * View economic subscription for contact
     * @param contactId
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function subscribe(Request $request, $contactId)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();

        $input = $request->all();
        $countries = $input['countries'];

        if (count($countries) > 0) {
            $this->contacts->where('contact_id', $contactId)->update(['economic_updates' => true]);
        } else {
            $this->contacts->where('contact_id', $contactId)->update(['economic_updates' => false]);
        }
        
        $this->economicSubscriptions
            ->where('contact_id', $contactId)
            ->whereNotIn('country_id', $countries)
            ->where('removed', false)
            ->update([
                'removed' => 1
            ]);

        foreach ($countries as $country) {
            $this->economicSubscriptions
                ->firstOrCreate(
                    ['contact_id' => $contactId, 'country_id' => $country, 'removed' => 0],
                    ['contacts_countries_id' => $this->generateHash('contacts_economic_updates_countries'), 'date_subscribed' => time()]
                );
        }

        if($countries) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Economic subscriptions were added!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Economic subscriptions were not added!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $countries,
            'count' => [
                'result' => count($countries),
                'total' => count($countries),
                'name' => 'subscriptions'
            ]
        ];

        return response()->json($response, $code);
    }
}