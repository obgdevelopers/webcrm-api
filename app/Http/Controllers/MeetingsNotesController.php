<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MeetingsNotesController extends Controller
{
    /**
     * All meeting notes
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();
        
        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $meetings = $this->meetings
                    ->join('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('companies.country_id', $country->id)
                    ->where('meeting_notes.removed', false)
                    ->where('companies.removed', false)
                    ->select('meeting_notes.meeting_note_id')
                    ->distinct('meeting_notes.id')
                    ->get();     
        
        $ids = [];
        foreach ($meetings as $meeting) {
            array_push($ids, $meeting->meeting_note_id);
        }

        $files = $this->meetingsNotesContents
                    ->whereIn('meeting_note_content.meeting_note_id', $ids)
                    ->where('meeting_note_content.removed', false)
                    ->select('meeting_note_content.*')
                    ->distinct('meeting_note_content.id')
                    // ->take(20)
                    ->get();


        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Files were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $files,
            'count' => count($files)
        ];

        return response()->json($response);
    }

    /**
     * Fetch updates for files
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/updates');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $input = $request->all();

        $datetime = $input['datetime'];
        
        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $meetings = $this->meetings
                    ->join('companies', 'companies.company_id', '=', 'meeting_notes.company_id')
                    ->where('companies.country_id', $country->id)
                    ->select('meeting_notes.meeting_note_id')
                    ->distinct('meeting_notes.id')
                    ->get();     
        
        $ids = [];
        foreach ($meetings as $meeting) {
            array_push($ids, $meeting->meeting_note_id);
        }

        $files = $this->meetingsNotesContents
                    ->whereIn('meeting_note_content.meeting_note_id', $ids)
                    ->where('meeting_note_content.updated_at', '>', $datetime)
                    ->where('meeting_note_content.removed', false)
                    ->select('meeting_note_content.*')
                    ->distinct('meeting_note_content.id')
                    // ->take(20)
                    ->get();


        if(count($files) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Files were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Something went wrong!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $files,
            'count' => [
                'result' => count($files),
                'total' => count($files),
                'name' => 'files'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Update per file
     * @param fileId
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function content(Request $request, $fileId)
    {
        $baseUrl = url('/api/files');
        $selfUrl = url()->full();

        $input = $request->all();

        $content = $input['content'];
        
        $file = $this->meetingsNotesContents
                    ->where('meeting_note_content.mn_content_id', $fileId)
                    ->update(['meeting_note_content.content' => $content]);

        if(count($file) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Content was updated!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Content was not updated!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $file,
            'count' => [
                'result' => count($file),
                'total' => count($file),
                'name' => 'files'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Update files
     * @param meetingId
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function files(Request $request, $meetingId)
    {
        $baseUrl = url('/api/files');
        $selfUrl = url()->full();

        $input = $request->all();

        $files = $input['files'];
        $type = $input['type'];
        $goback = $input['goback'];
        $dontgoback = $input['dontgoback'];

        if ($type == 'draft') {
            $incomplete = false;

            foreach ($files as $file) {
                $hash = $file['hash'];
                $content = $file['content'];
                $actiontobetaken = $file['actiontobetaken'];

                if (!$incomplete && ($content != '' && $content != null)) {
                    $incomplete = true;
                }

                $file = $this->meetingsNotesContents
                    ->where('meeting_note_content.mn_content_id', $hash)
                    ->update(['meeting_note_content.content' => $content]);
                
                if($actiontobetaken) {
                    if($goback || $dontgoback) {
                        $meeting = $this->meetings->where('meeting_note_id', $meetingId)->first();
                        $action = $goback ? $goback : $dontgoback;
                        $actionNote = $this->companiesActionNextYear
                        ->where('company_id', $meeting->company_id)
                        ->where('project_year', $meeting->project_year)
                        ->first();
                        if($actionNote)
                            $actionNote->update(['action'=> $action, 'note'=>$content, 'updated_date'=>date('Y-m-d H:i:s')]);
                        else {
                            $this->companiesActionNextYear
                            ->create([
                                'meeting_action_id' => $this->generateHash('meeting_notes_action_next_year'),
                                'company_id' => $meeting->company_id,
                                'project_year' => $meeting->project_year,
                                'action'=> $action,
                                'note'=>$content,
                                'created_date'=> date('Y-m-d H:i:s')
                            ]);
                        }
                    }
                }

            }

            if ($incomplete) {
                $this->meetings->where('meeting_note_id', $meetingId)->update(['status' => 'incomplete']);
            }
        } else {
            $incomplete = false;
            foreach ($files as $file) {
                $hash = $file['hash'];
                $content = $file['content'];
                $actiontobetaken = $file['actiontobetaken'];

                if (!$incomplete && ($content != '' && $content != null)) {
                    $incomplete = true;
                }

                $file = $this->meetingsNotesContents
                    ->where('meeting_note_content.mn_content_id', $hash)
                    ->update(['meeting_note_content.content' => $content]);
                
                if($actiontobetaken) {
                    if($goback || $dontgoback) {
                        $meeting = $this->meetings->where('meeting_note_id', $meetingId)->first();
                        $action = $goback ? $goback : $dontgoback;
                        $actionNote = $this->companiesActionNextYear
                        ->where('company_id', $meeting->company_id)
                        ->where('project_year', $meeting->project_year)
                        ->first();
                        if($actionNote)
                            $actionNote->update(['action'=> $action, 'note'=>$content, 'updated_date'=>date('Y-m-d H:i:s')]);
                        else{
                            $this->companiesActionNextYear
                            ->create([
                                'meeting_action_id' => $this->generateHash('meeting_notes_action_next_year'),
                                'company_id' => $meeting->company_id,
                                'project_year' => $meeting->project_year,
                                'action'=> $action,
                                'note'=>$content,
                                'created_date'=> date('Y-m-d H:i:s')
                            ]);
                        }
                    }
                }
            }

            $this->meetings->where('meeting_note_id', $meetingId)->where('status', '!=', 'approved')->update(['status' => 'submitted']);

        }
        
        $code = 200;
        $status = [
            'type' => 'success',
            'code' => 200,
            'message' => 'Content was updated!',
            'error' => false
        ];

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => [],
            'count' => [
                'result' => 0,
                'total' => 0,
                'name' => 'files'
            ]
        ];

        return response()->json($response, $code);
    }
}
