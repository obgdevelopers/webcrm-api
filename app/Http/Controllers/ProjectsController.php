<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller
{
    /**
     * Get all open projects
     *
     * @param [string] type
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function all(Request $request)
    {
        $baseUrl = url('/api/filters');
        $selfUrl = url()->full();

        $projects = $this->projects
                    ->leftJoin('countries', 'countries.id', '=', 'project.country_id')
                    ->leftJoin('project_year', 'project_year.id', '=', 'project.project_year_id')
                    ->where('closed', false)
                    ->select('countries.id AS country_id', 'countries.name AS country_text', 'project.project_year_id AS year_id', 'project_year.project_year AS year_text', 'project.created_at AS created', 'project.updated_at AS updated', 'project.status AS status', 'project.updated_by AS creator', 'project.target_sales AS target', 'project.id AS id')
                    ->orderBy('countries.name')
                    ->get();
        
        if(count($projects) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Projects were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Projects does not exist!',
                'error' => 'false'
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $projects,
            'count' => [
                'result' => count($projects),
                'total' => count($projects),
                'name' => 'filters'
            ]
        ];

        return response()->json($response, $code);
    }

}
