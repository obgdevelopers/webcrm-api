<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContractsPartnershipsController extends Controller
{
    /**
     * All partnerships for project
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function raw(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $partnerships = $this->contractsPartnerships
                        ->join('adverts', 'adverts.advert_id', '=', 'advert_partnerships.advert_id')
                        ->join('companies', 'companies.company_id', '=', 'adverts.company_id')
                        ->where('companies.country_id', $country->id)
                        ->where('adverts.project_year', $year->text)
                        ->where('advert_partnerships.advertpartnerships_removed', false)
                        ->where('companies.removed', false)
                        ->select('advert_partnerships.*')
                        ->distinct('advert_partnerships.advertpartnerships_id')
                        ->get();

        $response = [
            'status' => [
                'type' => 'success',
                'code' => 200,
                'message' => 'Partnerships were found!',
                'error' => 'false'
            ],
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $partnerships,
            'count' => count($partnerships)
        ];

        return response()->json($response);
    }

    /**
     * Fetch updates for partnership
     *
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function updates(Request $request)
    {
        $baseUrl = url('/api/all');
        $selfUrl = url()->full();

        $headers = $request->headers->all();
        $projects = $headers['session-project'][0];
        $projects = explode(',', $projects);

        $year = $this->projects->whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = $this->projects->whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $input = $request->all();
        $datetime = $input['datetime'];
        $unix = strtotime($datetime);

        $partnerships = $this->contractsPartnerships
                        ->join('adverts', 'adverts.advert_id', '=', 'advert_partnerships.advert_id')
                        ->join('companies', 'companies.company_id', '=', 'adverts.company_id')
                        ->where('companies.country_id', $country->id)
                        ->where('adverts.project_year', $year->text)
                        ->where('advert_partnerships.advertpartnerships_updated', '>', $unix)
                        ->select('advert_partnerships.*')
                        ->distinct('advert_partnerships.advertpartnerships_id')
                        ->get();

        if(count($partnerships) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Partnerships were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Partnerships were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $partnerships,
            'count' => [
                'result' => count($partnerships),
                'total' => count($partnerships),
                'name' => 'partnerships'
            ]
        ];

        return response()->json($response, $code);
    }
}
