<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RemindersController extends Controller
{
    /**
     * Reminders for companies
     *
     * @param [string] type
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function companies(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $reminders = $this->reminders
                    ->leftJoin('companies', 'companies.company_id', '=', 'reminder_tbl.reminder_companyid')
                    ->leftJoin('contacts', 'contacts.contact_id', '=', 'reminder_tbl.reminder_createdbyid')
                    ->where('reminder_tbl.reminder_active', true)
                    ->where('reminder_tbl.reminder_companyid', $companyId)
                    ->select('reminder_tbl.reminder_notes AS notes', 'reminder_tbl.reminder_category AS category', 'contacts.first_name AS firstname', 'contacts.last_name AS lastname','companies.name AS company_name', 'reminder_tbl.reminder_created AS created', 'reminder_tbl.reminder_date AS date', 'reminder_tbl.reminder_assigneeid AS assignee_id', 'reminder_tbl.reminder_contactid AS client_id')
                    ->first();
        
        if(count($reminders) > 0) {
            $assignee = $this->contacts->where('contact_id', $reminders['assignee_id'])->select('contacts.first_name AS firstname', 'contacts.last_name AS lastname')->first();
            $client = $this->contacts->where('contact_id', $reminders['client_id'])->select('contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'contacts.office_phone AS mobile')->first();
            $reminders['assignee'] = $assignee;
            $reminders['client'] = $client;
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Reminders were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Reminder does not exist!',
                'error' => 'false'
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $reminders,
            'count' => [
                'result' => count($reminders),
                'total' => count($reminders),
                'name' => 'reminders'
            ]
        ];

        return response()->json($response, $code);
    }

    /**
     * Add reminders for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function add(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $input = $request->all();

        $assignee = $input['assignee'];
        $contact = $input['contact'];
        $date = $input['date'];
        $time = $input['time'];
        $category = $input['category'];
        $notes = $input['notes'];
        $user = $input['user'];

        $exist = $this->reminders->where('reminder_companyid', $companyId)->count();

        if  ($exist > 0) {
            $reminder = $this->reminders->where('reminder_companyid', $companyId)->update([
                'reminder_notes' => $notes,
                'reminder_category' => $category,
                'reminder_date' => strtotime($date . ' ' . $time),
                'reminder_createdbyid' => $user,
                'reminder_assigneeid' => $assignee,
                'reminder_contactid' => $contact,
                'reminder_created' => strtotime(date('Y-m-d H:i:s')),
                'reminder_active' => 1
            ]);
        } else {
            $reminder = $this->reminders->create([
                'reminder_notes' => $notes,
                'reminder_category' => $category,
                'reminder_date' => strtotime($date . ' ' . $time),
                'reminder_createdbyid' => $user,
                'reminder_assigneeid' => $assignee,
                'reminder_contactid' => $contact,
                'reminder_companyid' => $companyId,
                'reminder_created' => strtotime(date('Y-m-d H:i:s')),
                'reminder_active' => 1
            ]);
        }

        $data = [];
        if($reminder) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Reminder was created successfully!',
                'error' => false
            ];
            $data['id'] = $reminder;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Reminder was not created!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'reminders'
            ]
        ];

        return response()->json($response, $code);

    }

    /**
     * Update reminders for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function update(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $input = $request->all();

        $assignee = $input['assignee'];
        $contact = $input['contact'];
        $date = $input['date'];
        $time = $input['time'];
        $category = $input['category'];
        $notes = $input['notes'];
        $user = $input['user'];

        $reminder = $this->reminders
                ->where('reminder_companyid', $companyId)
                ->update([
                    'reminder_notes' => $notes,
                    'reminder_category' => $category,
                    'reminder_date' => strtotime($date . ' ' . $time),
                    'reminder_createdbyid' => $user,
                    'reminder_assigneeid' => $assignee,
                    'reminder_contactid' => $contact,
                    'reminder_created' => strtotime(date('Y-m-d H:i:s')),
                    'reminder_active' => 1
                ]);

        $data = [];
        if($reminder) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Reminder was created updated!',
                'error' => false
            ];
            $data['id'] = $reminder['reminder_id'];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Reminder was not updated!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'reminders'
            ]
        ];

        return response()->json($response, $code);

    }

    /**
     * Delete reminders for company
     * @param companyId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function delete(Request $request, $companyId)
    {
        $baseUrl = url('/api/companies');
        $selfUrl = url()->full();

        $input = $request->all();

        $exist = $this->reminders->where('reminder_companyid', $companyId)->count();

        $data = [];
        if ($exist > 0) {
            $reminder = $this->reminders->where('reminder_companyid', $companyId)->update([
                'reminder_active' => 0
            ]);

            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Reminder was created successfully!',
                'error' => false
            ];
            $data['id'] = $reminder;
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Reminder was not created!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $data,
            'count' => [
                'result' => count($data),
                'total' => count($data),
                'name' => 'reminders'
            ]
        ];

        return response()->json($response, $code);

    }
}
