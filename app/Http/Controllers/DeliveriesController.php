<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeliveriesController extends Controller
{
    /**
     * All deliveries for contact
     * @param contactId
     * 
     * @return [json] status
     * @return [json] _links
     * @return [json] data
     * @return [json] count
     */
    public function contacts(Request $request, $contactId)
    {
        $baseUrl = url('/api/contacts');
        $selfUrl = url()->full();

        $deliveries = $this->deliveries
                            ->leftJoin('tblDeliveriesTypes', 'tblDeliveriesTypes.fldDeliveryTypeId', '=', 'tblDeliveries.tblDeliveriesTypes_fldDeliveryTypeId')
                            ->where('tblDeliveries.tblContacts_fldContactId', $contactId)
                            ->where('tblDeliveries.fldDeliveryDeleted', false)
                            ->whereNotNull('tblDeliveries.tblDeliveriesTypes_fldDeliveryTypeId')
                            ->select('tblDeliveriesTypes.fldDeliveryTypeName AS type', 'tblDeliveries.fldDeliveryQuantity AS quantity', 'tblDeliveries.fldDeliverySpecialConditions AS special_conditions', 'tblDeliveries.fldDeliveryOtherCopies AS other_copies')
                            ->get();

        if(count($deliveries) > 0) {
            $code = 200;
            $status = [
                'type' => 'success',
                'code' => 200,
                'message' => 'Deliveries were found!',
                'error' => false
            ];
        } else {
            $code = 404;
            $status = [
                'type' => 'success',
                'code' => 404,
                'message' => 'Deliveries were not found!',
                'error' => false
            ];
        }

        $response = [
            'status' => $status,
            '_links' => [
                'base' => $baseUrl,
                'self' => $selfUrl
            ],
            'data' => $deliveries,
            'count' => [
                'result' => count($deliveries),
                'total' => count($deliveries),
                'name' => 'deliveries'
            ]
        ];

        return response()->json($response, $code);
    }
}