<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $fldCompanyTagId
 * @property string $fldCompanyTagCreated
 * @property string $fldCompanyTagModified
 * @property boolean $fldCompanyTagDeleted
 * @property int $tblCompanies_fldCompanyId
 * @property string $tblCompanies_fldCompanyHashId
 * @property int $tblProjects_fldProjectId
 * @property int $tblTerms_fldTermId
 */
class CompaniesTags extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tblCompaniesTags';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'fldCompanyTagId';

    /**
     * @var array
     */
    protected $fillable = ['fldCompanyTagCreated', 'fldCompanyTagModified', 'fldCompanyTagDeleted', 'tblCompanies_fldCompanyId', 'tblCompanies_fldCompanyHashId', 'tblProjects_fldProjectId', 'tblTerms_fldTermId'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "fldCompanyTagCreated";
    const UPDATED_AT = "fldCompanyTagModified";

}
