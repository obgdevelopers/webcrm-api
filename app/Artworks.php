<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $artwork_id
 * @property string $advert_id
 * @property integer $crm_userid
 * @property string $document_id
 * @property string $filepath
 * @property string $filename
 * @property string $artwork_type
 * @property string $filemime
 * @property boolean $exists_remotely
 * @property boolean $exists_locally
 * @property string $created_by
 * @property integer $created_date
 * @property integer $last_updated_date
 * @property string $last_updated_by
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 */
class Artworks extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'artwork';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['artwork_id', 'advert_id', 'crm_userid', 'document_id', 'filepath', 'filename', 'artwork_type', 'filemime', 'exists_remotely', 'exists_locally', 'created_by', 'created_date', 'last_updated_date', 'last_updated_by', 'removed', 'created_at', 'updated_at'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";
}
