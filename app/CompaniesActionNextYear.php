<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $company_id
 * @property string $meeting_action_id
 * @property integer $project_year
 * @property integer $action
 * @property string $note
 * @property string $created_date
 * @property string $updated_date
 * @property string $updated_by
 */
class CompaniesActionNextYear extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'meeting_notes_action_next_year';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id', 'company_id', 'meeting_action_id', 'project_year', 'action', 'note', 'created_date', 'updated_date', 'updated_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function projectYear()
    {
        return $this->belongsTo('App\ProjectYear');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Companies');
    }

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_date";
    const UPDATED_AT = "updated_date";

    public $timestamps = false;
}
