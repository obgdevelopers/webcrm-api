<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $company_id
 * @property integer $project_id
 * @property int $company_status_id
 * @property string $created_at
 * @property string $company_hash_id
 * @property int $last_company_status_id
 */
class CompaniesProjectsStatuses extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'companies_project_status';

    /**
     * @var array
     */
    protected $fillable = ['company_id', 'project_id', 'company_status_id', 'created_at', 'company_hash_id', 'last_company_status_id'];

}
