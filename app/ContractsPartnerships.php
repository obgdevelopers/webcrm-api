<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $advertpartnerships_id
 * @property string $advertpartnerships_name
 * @property string $advertpartnerships_value
 * @property string $advertpartnerships_details
 * @property boolean $advertpartnerships_removed
 * @property integer $advertpartnerships_created
 * @property integer $advertpartnerships_updated
 * @property string $advertpartnerships_createdby
 * @property string $advertpartnerships_updatedby
 * @property string $advert_id
 * @property string $advertpartnerships_otherdetails
 */
class ContractsPartnerships extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'advert_partnerships';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'advertpartnerships_id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['advertpartnerships_name', 'advertpartnerships_value', 'advertpartnerships_details', 'advertpartnerships_removed', 'advertpartnerships_created', 'advertpartnerships_updated', 'advertpartnerships_createdby', 'advertpartnerships_updatedby', 'advert_id', 'advertpartnerships_otherdetails'];

    /**
     * No timestamps
     */
    public $timestamps = false;
}
