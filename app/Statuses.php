<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $status_group
 */
class Statuses extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'company_status';

    /**
     * @var array
     */
    protected $fillable = ['name', 'status_group'];

    /**
     * Find grouped statuses
     * 
     * @return [object] statuses
     */
    public function findGroupedStatuses()
    {
        $query = $this->select('id AS id', 'name AS text', 'status_group AS group');
        
        $results = $query->get();
        
        $statuses['Pending'] = [];
        $statuses['In Progress'] = [];
        $statuses['Ruled Out'] = [];
        $statuses['Closed - negative'] = [];
        $statuses['Closed - positive'] = [];
       
        foreach ($results as $result) {
            $statuses[$result['group']][] = $result;
        }
                    
        return $statuses;
    }
}
