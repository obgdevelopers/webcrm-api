<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $fldMeetingEmployeeId
 * @property string $fldMeetingEmployeeHashId
 * @property string $fldMeetingEmployeeCreated
 * @property string $fldMeetingEmployeeModified
 * @property boolean $fldMeetingEmployeeDeleted
 * @property int $tblMeetings_fldMeetingId
 * @property string $tblMeetings_fldMeetingHashId
 * @property int $tblEmployees_fldEmployeeId
 * @property string $tblEmployees_fldEmployeeHashId
 */
class MeetingsEmployees extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tblMeetingsEmployees';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'fldMeetingEmployeeId';

    /**
     * @var array
     */
    protected $fillable = ['fldMeetingEmployeeHashId', 'fldMeetingEmployeeCreated', 'fldMeetingEmployeeModified', 'fldMeetingEmployeeDeleted', 'tblMeetings_fldMeetingId', 'tblMeetings_fldMeetingHashId', 'tblEmployees_fldEmployeeId', 'tblEmployees_fldEmployeeHashId'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "fldMeetingEmployeeCreated";
    const UPDATED_AT = "fldMeetingEmployeeModified";

    /**
     * Find employees in meetings
     * 
     * @param  companyId
     * @param  year
     * 
     * @return [object] files
     */
    public function findEmployees($meetings)
    {
        $push = [];
        foreach($meetings as $meeting) {
            $id = $meeting['id'];
            $employees = $this->leftJoin('tblEmployees', 'tblEmployees.fldEmployeeId', '=', 'tblMeetingsEmployees.tblEmployees_fldEmployeeId')
                    ->where('tblEmployees.fldEmployeeId', false)
                    ->where('tblMeetingsEmployees.fldMeetingEmployeeDeleted', false)
                    ->where('tblMeetingsEmployees.tblMeetings_fldMeetingId', $id)
                    ->select('tblEmployees.fldEmployeeFirstname AS firstname', 'tblEmployees.fldEmployeeLastname AS lastname', 'tblEmployees.fldEmployeeJobTitle AS job_title', 'tblEmployees.fldEmployeeOfficeLandline AS landline', 'tblEmployees.fldEmployeeMobile AS mobile', 'tblEmployees.fldEmployeeEmail');

            $meeting['employees'] = $employees->get()->toArray();

            $push[] = $meeting;
        }
        

        $response = $push;

        return $response;
    }
}
