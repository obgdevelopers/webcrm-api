<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $heading_id
 * @property string $heading_name
 * @property boolean $active
 * @property int $fldMeetingNoteHeadingDelta
 * @property string $meeting_delta
 * @property boolean $removed
 */
class MeetingsNotesHeadings extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'meeting_notes_headings';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['heading_id', 'heading_name', 'active', 'meeting_delta', 'removed'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";
}
