<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $fldEmployeeId
 * @property string $fldEmployeeHashId
 * @property string $fldEmployeeSalutation
 * @property string $fldEmployeeFirstname
 * @property string $fldEmployeeMiddlename
 * @property string $fldEmployeeLastname
 * @property string $fldEmployeeJobTitle
 * @property string $fldEmployeeOfficeLandline
 * @property string $fldEmployeeOfficeFax
 * @property string $fldEmployeeMobile
 * @property string $fldEmployeeEmail
 * @property string $fldEmployeeOfficeAddressLine1
 * @property string $fldEmployeeOfficeAddressLine2
 * @property string $fldEmployeeOfficeAddressLine3
 * @property string $fldEmployeeOfficeAddressTownCity
 * @property string $fldEmployeeOfficeAddressRegion
 * @property string $fldEmployeeOfficeAddressPostalCode
 * @property boolean $fldEmployeeLeftCompany
 * @property string $fldEmployeeCreated
 * @property string $fldEmployeeModified
 * @property boolean $fldEmployeeDeleted
 * @property int $tblUsers_fldUserId
 */
class Employees extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tblEmployees';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'fldEmployeeId';

    /**
     * @var array
     */
    protected $fillable = ['fldEmployeeHashId', 'fldEmployeeSalutation', 'fldEmployeeFirstname', 'fldEmployeeMiddlename', 'fldEmployeeLastname', 'fldEmployeeJobTitle', 'fldEmployeeOfficeLandline', 'fldEmployeeOfficeFax', 'fldEmployeeMobile', 'fldEmployeeEmail', 'fldEmployeeOfficeAddressLine1', 'fldEmployeeOfficeAddressLine2', 'fldEmployeeOfficeAddressLine3', 'fldEmployeeOfficeAddressTownCity', 'fldEmployeeOfficeAddressRegion', 'fldEmployeeOfficeAddressPostalCode', 'fldEmployeeLeftCompany', 'fldEmployeeCreated', 'fldEmployeeModified', 'fldEmployeeDeleted', 'tblUsers_fldUserId'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "fldEmployeeCreated";
    const UPDATED_AT = "fldEmployeeModified";

    /**
     * Find users with project access
     * 
     * @param  [string] projects
     * 
     * @return [object] employees
     */
    public function findByProjects($projects)
    {
        $projects = explode(',', $projects);
        $query = $this->leftJoin('tblUsers', 'tblUsers.fldUserId', '=', 'tblEmployees.tblUsers_fldUserId')
                    ->leftJoin('tblProjectsAccesses', 'tblProjectsAccesses.tblUsers_fldUserId', '=', 'tblUsers.fldUserId')
                    ->leftJoin('tblProjects', 'tblProjects.fldProjectId', '=', 'tblProjectsAccesses.tblProjects_fldProjectId')
                    ->where('tblEmployees.fldEmployeeDeleted', false)
                    ->where('tblUsers.fldUserDeleted', false)
                    ->where('tblProjectsAccesses.fldProjectAccessDeleted', false)
                    ->where('tblProjects.fldProjectDeleted', false)
                    ->select('tblEmployees.fldEmployeeId AS id', 'tblEmployees.fldEmployeeFirstname AS firstname', 'tblEmployees.fldEmployeeLastname AS lastname');

                    
        foreach($projects as $project) {
            $query->where('tblProjectsAccesses.tblProjects_fldProjectId', '=', $project);
        }

        $results = $query->get();
        
        return $results;
    }
}


// <?php

// namespace App;

// use Illuminate\Database\Eloquent\Model;

// /**
//  * @property integer $id
//  * @property string $contact_id
//  * @property string $salutation
//  * @property string $first_name
//  * @property string $middle_name
//  * @property string $last_name
//  * @property string $job_title
//  * @property string $office_phone
//  * @property string $office_fax
//  * @property string $mobile_phone
//  * @property string $email
//  * @property string $office_address_line_1
//  * @property string $office_address_line_2
//  * @property string $office_address_line_3
//  * @property string $office_address_town_city
//  * @property string $office_address_region
//  * @property string $office_postal_code
//  * @property string $contact_type
//  * @property string $company_id
//  * @property string $pa_id
//  * @property string $created_by
//  * @property integer $created_date
//  * @property string $last_updated_by
//  * @property integer $last_updated_date
//  * @property boolean $economic_updates
//  * @property boolean $left_company
//  * @property boolean $removed
//  * @property string $created_at
//  * @property string $updated_at
//  * @property string $last_contacted
//  * @property string $last_met
//  */
// class Contacts extends Model
// {
//     /**
//      * The "type" of the auto-incrementing ID.
//      * 
//      * @var string
//      */
//     protected $keyType = 'integer';

//     /**
//      * @var array
//      */
//     protected $fillable = ['contact_id', 'salutation', 'first_name', 'middle_name', 'last_name', 'job_title', 'office_phone', 'office_fax', 'mobile_phone', 'email', 'office_address_line_1', 'office_address_line_2', 'office_address_line_3', 'office_address_town_city', 'office_address_region', 'office_postal_code', 'contact_type', 'company_id', 'pa_id', 'created_by', 'created_date', 'last_updated_by', 'last_updated_date', 'economic_updates', 'left_company', 'removed', 'created_at', 'updated_at', 'last_contacted', 'last_met'];

//     /**
//      * Datestamps for table
//      * 
//      * @var string
//      */
//     const CREATED_AT  = "created_at";
//     const UPDATED_AT = "updated_at";

//     /**
//      * Index page table data
//      * 
//      * @param  [string] where
//      * @param  [string] bind
//      * @param  [int] limit
//      * @param  [int] offset
//      * 
//      * @return [object] table
//      */
//     public function buildTable($parameters, $limit = 50, $offset)
//     {
//         $projects = $parameters['projects'];
//         $projects = explode(',', $projects);
        
//         $query = $this->leftJoin('tblCompanies', 'tblCompanies.fldCompanyId', '=', 'tblContacts.tblCompanies_fldCompanyId')
//                     ->leftJoin('tblProjects', 'tblProjects.tblCountries_fldCountryId', '=', 'tblCompanies.tblCountries_fldCountryId')
//                     ->where('tblContacts.fldContactDeleted', false)
//                     ->take($limit)
//                     ->skip($offset)
//                     ->select('tblContacts.fldContactId AS id', 'tblContacts.fldContactFirstname AS firstname', 'tblContacts.fldContactLastname AS lastname', 'tblContacts.fldContactJobTitle AS title', 'tblCompanies.fldCompanyName as company', 'tblContacts.fldContactOfficeLandline AS landline', 'tblContacts.fldContactMobile AS mobile', 'tblContacts.fldContactEmail AS email');
        
//         foreach($projects as $project) {
//             $query->where('tblProjects.fldProjectId', '=', $project);
//         }

//         $table = $this->buildQuery($parameters, $query);

//         $count = $table->count();

//         $table->take($limit)
//         ->skip($offset);
                    
//         $response = [
//             'table' => $table->get(),
//             'count' => $count
//         ];

//         return $response;
//     }

//     /**
//      * Build a query with parameters/filters
//      * 
//      * @param [array] parameters
//      * @param [object] query
//      * 
//      * @return [object] query
//      */
//     private function buildQuery($parameters, $query) 
//     {
//         if(isset($parameters['first_name'])) {
//             $query->where('tblContacts.fldContactFirstname', 'LIKE', '%'.$parameters['first_name'].'%');
//         }

//         if(isset($parameters['last_name'])) {
//             $query->where('tblContacts.fldContactLastname', 'LIKE', '%'.$parameters['last_name'].'%');
//         }

//         if(isset($parameters['job_title'])) {
//             $query->where('tblContacts.fldContactJobTitle', 'LIKE', '%'.$parameters['job_title'].'%');
//         }

//         if(isset($parameters['company_name'])) {
//             $query->where('tblCompanies.fldCompanyName', 'LIKE', '%'.$parameters['company_name'].'%');
//         }

//         if(isset($parameters['company_name'])) {
//             $query->where('tblCompanies.fldCompanyName', 'LIKE', '%'.$parameters['company_name'].'%');
//         }

//         if(isset($parameters['phone_number'])) {
//             $query->where('tblCompanies.fldContactOfficeLandline', 'LIKE', '%'.$parameters['phone_number'].'%');
//         }

//         if(isset($parameters['mobile_number'])) {
//             $query->where('tblCompanies.fldContactMobile', 'LIKE', '%'.$parameters['mobile_number'].'%');
//         }

//         if(isset($parameters['tags'])) {
//             $query->rightJoin('tblContactsTags', 'tblContactsTags.tblContacts_fldContactId', '=', 'tblContacts.fldContactId');
//             $query->whereIn('tblContactsTags.tblTerms_fldTermId', $parameters['tags']);
//         }

//         if(isset($parameters['contacted_date'])) {
//             $contactedDate = $parameters['contacted_date'];
//             $from = $contactedDate['from'];
//             $to = $contactedDate['to'];

//             if($from != null) {
//                 $query->where('tblContacts.fldContactLastContacted', '>=', $from);
//             }

//             if($to != null) {
//                 $query->where('tblContacts.fldContactLastContacted', '<=', $to);
//             }
//         }

//         if(isset($parameters['met_date'])) {
//             $metDate = $parameters['met_date'];
//             $from = $metDate['from'];
//             $to = $metDate['to'];

//             if($from != null) {
//                 $query->where('tblContacts.fldContactLastMet', '>=', $from);
//             }

//             if($to != null) {
//                 $query->where('tblContacts.fldContactLastMet', '<=', $to);
//             }
//         }

//         return $query;
//     }
// }
