<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $artwork_advert_id
 * @property integer $crm_userid
 * @property string $artwork_id
 * @property string $artwork_supplied_by
 * @property integer $artwork_due_date
 * @property integer $artwork_requested_date
 * @property integer $artwork_received_date
 * @property string $final_placement
 * @property string $left_right
 * @property integer $istanbul_received_date
 * @property integer $istanbul_approved_date
 * @property string $technical_notes
 * @property integer $editorial_approval
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 */
class ArtworksContracts extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'artwork_advert';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['artwork_advert_id', 'crm_userid', 'artwork_id', 'artwork_supplied_by', 'artwork_due_date', 'artwork_requested_date', 'artwork_received_date', 'final_placement', 'left_right', 'istanbul_received_date', 'istanbul_approved_date', 'technical_notes', 'editorial_approval', 'removed', 'created_at', 'updated_at'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";
}
