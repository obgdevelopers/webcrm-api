<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $fldMeetingDeltaId
 * @property string $fldMeetingDeltaTitle
 * @property boolean $fldMeetingDeltaDeleted
 * @property string $fldMeetingDeltaCreated
 * @property string $fldMeetingDeltaModified
 */
class MeetingsDeltas extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tblMeetingsDeltas';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'fldMeetingDeltaId';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['fldMeetingDeltaTitle', 'fldMeetingDeltaDeleted', 'fldMeetingDeltaCreated', 'fldMeetingDeltaModified'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "fldMeetingDeltaCreated";
    const UPDATED_AT = "fldMeetingDeltaModified";

}
