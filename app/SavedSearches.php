<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $parameters
 * @property string $saved_by
 * @property boolean $visible_to_all_users
 * @property int $project_id
 * @property string $entity_type
 */
class SavedSearches extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'saved_search';

    /**
     * @var array
     */
    protected $fillable = ['name', 'parameters', 'saved_by', 'visible_to_all_users', 'project_id', 'entity_type'];

    /**
     * Index page filters data
     * 
     * @param  [string] type

     * @return [object] terms
     */
    public function findByType($parameters, $projects)
    {
        $filters = $this->whereIn('saved_search.project_id', $projects)
                    ->where(function($subquery) use ($parameters) {
                        $subquery->where('saved_search.saved_by', $parameters['user'])
                            ->orWhere('saved_search.visible_to_all_users', true);
                    })
                    ->where('saved_search.entity_type', $parameters['type'])
                    ->select('saved_search.id AS id', 'saved_search.name AS name', 'saved_search.parameters AS parameters', 'saved_search.visible_to_all_users AS public', 'saved_search.saved_by AS user');

        return $filters->get();
    }

    /**
     * No timestamps
     */
    public $timestamps = false;
}
