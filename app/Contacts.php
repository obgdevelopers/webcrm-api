<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Projects;

/**
 * @property integer $id
 * @property string $contact_id
 * @property string $salutation
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $job_title
 * @property string $office_phone
 * @property string $office_fax
 * @property string $mobile_phone
 * @property string $email
 * @property string $office_address_line_1
 * @property string $office_address_line_2
 * @property string $office_address_line_3
 * @property string $office_address_town_city
 * @property string $office_address_region
 * @property string $office_postal_code
 * @property string $contact_type
 * @property string $company_id
 * @property string $pa_id
 * @property string $created_by
 * @property integer $created_date
 * @property string $last_updated_by
 * @property integer $last_updated_date
 * @property boolean $economic_updates
 * @property boolean $left_company
 * @property boolean $removed
 * @property string $created_at
 * @property string $updated_at
 * @property string $last_contacted
 * @property string $last_met
 */
class Contacts extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['contact_id', 'salutation', 'first_name', 'middle_name', 'last_name', 'job_title', 'office_phone', 'office_fax', 'mobile_phone', 'email', 'office_address_line_1', 'office_address_line_2', 'office_address_line_3', 'office_address_town_city', 'office_address_region', 'office_postal_code', 'contact_type', 'company_id', 'pa_id', 'created_by', 'created_date', 'last_updated_by', 'last_updated_date', 'economic_updates', 'left_company', 'removed', 'created_at', 'updated_at', 'last_contacted', 'last_met'];

    /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "created_at";
    const UPDATED_AT = "updated_at";

    /**
     * Find users with project access
     * 
     * @param  [string] projects
     * 
     * @return [object] employees
     */
    public function findByProjects($projects, $title)
    {
        $query = $this->leftJoin('sf_user', 'sf_user.contact_id', '=', 'contacts.contact_id')
                    ->leftJoin('user_year', 'user_year.contact_id', '=', 'contacts.contact_id')
                    ->where('contacts.removed', false)
                    ->where('user_year.removed', false)
                    ->where('sf_user.sf_user_departmentId', 2)
                    ->select('contacts.id AS id', 'contacts.contact_id AS hash', 'contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'sf_user.email', 'sf_user.agenda_email')
                    ->distinct();

        $countries = [];
        $years = [];           
        foreach($projects as $project) {
            array_push($countries, $project['country_id']);
            array_push($years, $project['year']);
            // $query->where('tblProjectsAccesses.tblProjects_fldProjectId', '=', $project);
        }

        if ($title != null) {
            if ($title == 'project manager' || $title == 'country director') {
                $query->whereIn('contacts.job_title', ['project manager', 'country director']);
            } else if ($title == 'editorial manager' || $title == 'editorial manager trainee') {
                $query->whereIn('contacts.job_title', ['editorial manager', 'editorial manager trainee']);
            }
        }

        $query->whereIn('user_year.project_year', $years);
        $query->whereIn('user_year.country_id', $countries);

        $results = $query->get();
        
        return $results;
    }

     /**
     * Index page table data
     * 
     * @param  [string] where
     * @param  [string] bind
     * @param  [int] limit
     * @param  [int] offset
     * 
     * @return [object] table
     */
    public function buildTable($parameters, $limit = 50, $offset, $projects)
    {
        $year = Projects::whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = Projects::whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $query = $this->leftJoin('companies', 'companies.company_id', '=', 'contacts.company_id')
                    ->where('contacts.removed', false)
                    ->where('companies.country_id', $country->id)
                    ->select('contacts.contact_id AS id', 'contacts.contact_id AS hash', 'contacts.first_name AS firstname', 'contacts.last_name AS lastname', 'contacts.job_title AS title', 'companies.name as company', 'companies.company_id AS company_hash', 'contacts.office_phone AS landline', 'contacts.mobile_phone AS mobile', 'contacts.email AS email');
        
        $query->where('companies.country_id', $country->id);

        $table = $this->buildQuery($parameters, $query);

        $count = $table->count();
                    
        $response = [
            'table' => $table->get(),
            'count' => $count
        ];

        return $response;
    }

    /**
     * Build a query with parameters/filters
     * 
     * @param [array] parameters
     * @param [object] query
     * 
     * @return [object] query
     */
    private function buildQuery($parameters, $query) 
    {
        if($this->validate($parameters, 'first_name')) {
            $query->where('contacts.first_name', 'LIKE', '%'.$parameters['first_name'].'%');
        }

        if($this->validate($parameters, 'last_name')) {
            $query->where('contacts.last_name', 'LIKE', '%'.$parameters['last_name'].'%');
        }

        if($this->validate($parameters, 'job_title')) {
            $query->where('contacts.job_title', 'LIKE', '%'.$parameters['job_title'].'%');
        }

        if($this->validate($parameters, 'company_name')) {
            $query->where('companies.name', 'LIKE', '%'.$parameters['company_name'].'%');
        }

        if($this->validate($parameters, 'email_address')) {
            $query->where('contacts.email', 'LIKE', '%'.$parameters['email_address'].'%');
        }

        if($this->validate($parameters, 'landline')) {
            $query->where('contacts.office_phone', 'LIKE', '%'.$parameters['landline'].'%');
        }

        if($this->validate($parameters, 'mobile_number')) {
            $query->where('contacts.mobile_phone', 'LIKE', '%'.$parameters['mobile_number'].'%');
        }

        if($this->validate($parameters, 'tags')) {
            $query->join('entity_project_tag', 'entity_project_tag.entity_id', '=', 'contacts.contact_id')
                    ->where('entity_project_tag.entity_type', 'contact')
                    ->where('entity_project_tag.term_id', $parameters['tags']);
        }

        if($this->validate($parameters, 'contacted_date')) {
            $contactedDate = $parameters['contacted_date'];
            $start = strtotime($contactedDate['start']);
            $end = strtotime($contactedDate['end']);

            if($start != null) {
                $query->where('contacts.last_contacted', '>=', $start);
            }

            if($end != null) {
                $query->where('contacts.last_contacted', '<=', $end);
            }
        }

        if($this->validate($parameters, 'met_date')) {
            $metDate = $parameters['met_date'];
            $start = strtotime($metDate['start']);
            $end = strtotime($metDate['end']);

            if($start != null) {
                $query->where('contacts.last_met', '>=', $start);
            }

            if($end != null) {
                $query->where('contacts.last_met', '<=', $end);
            }
        }

        return $query;
    }

    /**
     * All contacts for the project
     * 
     * @param  [string] where
     * @param  [string] bind
     * @param  [int] limit
     * @param  [int] offset
     * 
     * @return [object] table
     */
    public function buildRaw($projects)
    {
        // $projects = $parameters['projects'];
        // $projects = explode(',', $projects);
        $year = Projects::whereIn('id', $projects)
                    ->select('project_year AS text', 'project_year_id AS id')
                    ->first();

        $country = Projects::whereIn('id', $projects)
                    ->select('country_id AS id')
                    ->first();

        $query = $this->join('companies', 'companies.company_id', '=', 'contacts.company_id')
                    ->where('companies.removed', false)
                    ->where('contacts.removed', false)
                    ->where('companies.country_id', $country->id)
                    ->select('contacts.*')
                    ->distinct('contacts.id');
        // $query = $this->where('companies.removed', false)
        //             ->select('companies.company_id AS id')
        //             ->distinct('id');
                    
        // $query->where('companies.country_id', $country->id);
        $count = $query->count();

        $contacts = $query->get();

        $response = [
            'table' => $contacts,
            'count' => $count
        ];

        return $response;
    }

    /**
     * Check if parameter exists and variable is not empty
     * 
     * @param [array] parameters
     * @param [string] index
     * 
     * @return [bool] exist
     */
    function validate($parameters, $index) {
        if(isset($parameters[$index])) {
            return true;
        }

        return false;
    }
}
