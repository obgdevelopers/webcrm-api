<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $entity_id
 * @property string $entity_type
 * @property int $project_id
 * @property int $term_id
 */
class EntitiesTags extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'entity_project_tag';

    /**
     * @var array
     */
    protected $fillable = ['entity_id', 'entity_type', 'project_id', 'term_id'];

    /**
     * No timestamps
     */
    public $timestamps = false;
}
