<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $fldContactAssistantId
 * @property string $fldContactAssistantCreated
 * @property string $fldContactAssistantModified
 * @property boolean $fldContactAssistantDeleted
 * @property int $tblContacts_fldContactId
 * @property string $tblContacts_fldContactHashId
 * @property int $tblPersonalAssistants_fldPersonalAssistantId
 * @property string $tblPersonalAssistants_fldPersonalAssistanHashId
 */
class ContactsAssistants extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tblContactsAssistants';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'fldContactAssistantId';

    /**
     * @var array
     */
    protected $fillable = ['fldContactAssistantCreated', 'fldContactAssistantModified', 'fldContactAssistantDeleted', 'tblContacts_fldContactId', 'tblContacts_fldContactHashId', 'tblPersonalAssistants_fldPersonalAssistantId', 'tblPersonalAssistants_fldPersonalAssistanHashId'];

     /**
     * Datestamps for table
     * 
     * @var string
     */
    const CREATED_AT  = "fldContactAssistantCreated";
    const UPDATED_AT = "fldContactAssistantModified";
}
