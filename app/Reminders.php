<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $reminder_id
 * @property string $reminder_notes
 * @property string $reminder_category
 * @property string $reminder_date
 * @property string $reminder_createdbyid
 * @property string $reminder_assigneeid
 * @property string $reminder_contactid
 * @property string $reminder_companyid
 * @property string $reminder_created
 * @property boolean $reminder_active
 */
class Reminders extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'reminder_tbl';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'reminder_id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['reminder_notes', 'reminder_category', 'reminder_date', 'reminder_createdbyid', 'reminder_assigneeid', 'reminder_contactid', 'reminder_companyid', 'reminder_created', 'reminder_active'];

    /**
     * No timestamps
     */
    public $timestamps = false;
}
