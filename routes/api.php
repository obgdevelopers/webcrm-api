<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::options('{any}');

Route::get('connection', 'AuthController@connection');

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'contacts',
], function () {
    Route::options('{any}');

    Route::get('', 'ContactsController@list');
    Route::get('table', 'ContactsController@table');
    Route::group([
        'prefix' => '{contactId}',
    ], function () {
        Route::options('{any}');

        Route::get('', 'ContactsController@view');
        Route::put('', 'ContactsController@update');
        Route::delete('', 'ContactsController@delete');

        Route::get('histories', 'CommunicationLogsController@contacts');
        Route::post('histories', 'CommunicationLogsController@addContact');

        Route::get('assistants', 'PersonalAssistantsController@contact');
        Route::post('assistants', 'PersonalAssistantsController@add');

        Route::get('subscriptions', 'EconomicSubscriptionsController@contacts');
        Route::post('subscriptions', 'EconomicSubscriptionsController@subscribe');

        Route::get('deliveries', 'DeliveriesController@contacts');
        Route::get('tags', 'TermsController@contacts');
    });
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'contracts',
], function () {
    Route::options('{any}');

    Route::group([
        'prefix' => '{contactId}',
    ], function () {
        Route::options('{any}');

        Route::get('', 'ContractsController@view');
    });
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'assistants',
], function () {
    Route::options('{any}');

    Route::group([
        'prefix' => '{assistantId}',
    ], function () {
        Route::options('{any}');

        Route::get('', 'PersonalAssistantsController@view');
    });
});

Route::group([
    'middleware' => 'cors',
    'prefix' => 'all',
], function () {
    Route::options('{any}');

    Route::get('companies', 'CompaniesController@raw');
    Route::get('employees', 'EmployeesController@raw');
    Route::get('contacts', 'ContactsController@raw');
    Route::get('histories', 'CommunicationLogsController@raw');
    Route::get('chapters', 'ChaptersController@raw');
    Route::get('contracts', 'ContractsController@raw');
    Route::get('artworks', 'ArtworksController@raw');
    Route::get('partnerships', 'ContractsPartnershipsController@raw');
    Route::get('assistants', 'PersonalAssistantsController@raw');
    Route::get('subscriptions', 'EconomicSubscriptionsController@raw');
    Route::get('meetings', 'MeetingsController@raw');
    Route::get('files', 'MeetingsNotesController@raw');
    Route::get('attendees', 'MeetingsAttendancesController@raw');
    Route::get('potentials', 'AdvisoryPotentialController@raw');
    Route::get('actions', 'CompaniesController@actions');
});

Route::group([
    'middleware' => 'cors',
    'prefix' => 'updates',
], function () {
    Route::options('{any}');

    Route::get('companies', 'CompaniesController@updates');
    // Route::get('employees', 'EmployeesController@updates');
    Route::get('contacts', 'ContactsController@updates');
    Route::get('histories', 'CommunicationLogsController@updates');
    Route::get('chapters', 'ChaptersController@updates');
    Route::get('contracts', 'ContractsController@updates');
    Route::get('artworks', 'ArtworksController@updates');
    Route::get('partnerships', 'ContractsPartnershipsController@updates');
    Route::get('assistants', 'PersonalAssistantsController@updates');
    Route::get('subscriptions', 'EconomicSubscriptionsController@updates');
    Route::get('meetings', 'MeetingsController@updates');
    Route::get('files', 'MeetingsNotesController@updates');
    Route::get('attendees', 'MeetingsAttendancesController@updates');
    Route::get('potentials', 'AdvisoryPotentialController@updates');
    Route::get('actions', 'CompaniesController@actionupdates');
    Route::post('', 'SynchronizerController@push');
});

Route::group([
    'middleware' => 'cors',
    'prefix' => 'companies',
], function () {
    Route::options('{any}');

    Route::post('', 'CompaniesController@add');
    Route::get('', 'CompaniesController@list');

    Route::get('table', 'CompaniesController@table');
    Route::group([
        'prefix' => '{companyId}',
    ], function () {
        Route::options('{any}');

        Route::get('', 'CompaniesController@view');
        Route::put('', 'CompaniesController@update');
        Route::delete('', 'CompaniesController@delete');

        Route::get('tags', 'TermsController@companies');
        Route::get('employee', 'EmployeesController@assignee');
        Route::get('contracts', 'ContractsController@companies');
        Route::get('assistants', 'PersonalAssistantsController@companies');

        Route::put('potentials', 'AdvisoryPotentialController@update');

        Route::group([
            'prefix' => 'contacts',
        ], function () {
            Route::options('{any}');

            Route::get('', 'ContactsController@companies');
            Route::post('', 'ContactsController@add');
        });

        Route::group([
            'prefix' => 'notes',
        ], function () {
            Route::options('{any}');

            Route::get('', 'CompaniesNotesController@list');
            Route::post('', 'CompaniesNotesController@add');
        });

        Route::group([
            'prefix' => 'histories',
        ], function () {
            Route::options('{any}');

            Route::get('', 'CommunicationLogsController@companies');
            Route::post('', 'CommunicationLogsController@add');
        });

        Route::group([
            'prefix' => 'reminders',
        ], function () {
            Route::options('{any}');

            Route::get('', 'RemindersController@companies');
            Route::post('', 'RemindersController@add');
            Route::put('', 'RemindersController@update');
            Route::delete('', 'RemindersController@delete');
        });

        Route::group([
            'prefix' => 'meetings',
        ], function () {
            Route::options('{any}');

            Route::get('', 'MeetingsController@list');
            Route::post('', 'MeetingsController@add');
        });

        Route::get('agendas', 'MeetingsController@agendas');
    });
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'terms',
    // 'middleware' => 'auth:api'
], function () {
    Route::get('', 'TermsController@index');
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'files',
    // 'middleware' => 'auth:api'
], function () {
    Route::options('{any}');
    Route::post('{fileId}', 'MeetingsNotesController@content');
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'meetings',
    // 'middleware' => 'auth:api'
], function () {
    Route::options('{any}');

    Route::get('', 'MeetingsController@calendar');

    Route::group([
        'prefix' => '{meetingId}',
    ], function () {
        Route::options('{any}');

        Route::post('', 'MeetingsNotesController@files');

        Route::patch('', 'MeetingsController@reschedule');
        Route::delete('', 'MeetingsController@cancel');
        Route::get('', 'MeetingsController@view');
        Route::put('', 'MeetingsController@update');
        Route::get('contents', 'MeetingsController@check');
        Route::get('files', 'MeetingsController@files');
    });
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'notes'
], function () {
    Route::options('{any}');

    Route::group([
        'prefix' => '{noteId}',
    ], function () {
        Route::options('{any}');

        Route::put('', 'CompaniesNotesController@update');
        Route::delete('', 'CompaniesNotesController@delete');
    });

});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'filters',
    // 'middleware' => 'auth:api'
], function () {
    Route::options('{any}');

    Route::get('', 'FiltersController@index');
    Route::delete('{filterId}', 'FiltersController@delete');
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'projects',
    // 'middleware' => 'auth:api'
], function () {
    Route::options('{any}');

    Route::get('', 'ProjectsController@all');
    Route::get('statuses', 'CompaniesStatusesController@projects');
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'countries',
    // 'middleware' => 'auth:api'
], function () {
    Route::options('{any}');

    Route::get('', 'CountriesController@all');
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'histories',
    // 'middleware' => 'auth:api'
], function () {
    Route::options('{any}');

    Route::get('{historyId}', 'CommunicationLogsController@view');
    Route::put('{historyId}', 'CommunicationLogsController@update');
    Route::delete('{historyId}', 'CommunicationLogsController@delete');
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'statuses',
    // 'middleware' => 'auth:api'
], function () {
    Route::get('', 'StatusesController@list');
    Route::post('versions', 'StatusesController@getOnlineVersion');
    Route::post('tables', 'StatusesController@getTables');
    Route::post('schema', 'StatusesController@getTableSchema');
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'sectors',
    // 'middleware' => 'auth:api'
], function () {
    Route::get('', 'SectorsController@list');
});

Route::group([
    'middleware' => ['cors'],
    'prefix' => 'employees',
    // 'middleware' => 'auth:api'
], function () {
    Route::options('{any}');

    Route::get('options', 'EmployeesController@options');
    Route::get('', 'EmployeesController@all');
    Route::post('', 'EmployeesController@verify');
    Route::group([
        'prefix' => '{employeeId}',
    ], function () {
        Route::options('{any}');

        Route::post('filters', 'FiltersController@user');
        Route::get('meetings', 'MeetingsController@user');

    });
});
